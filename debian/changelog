xmedcon (0.25.0-gtk3+dfsg-1) unstable; urgency=medium

  * New upstream version 0.25.0-gtk3+dfsg
  * d/patches/*: unfuzz all patches.
  * gcc-15.patch: new: fix build failure with gcc 15. (Closes: #1098154)
  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.7.2.
  * d/patches/*: normalize dep3 header.
  * typos.patch: tackle a new typo in a manual page.
  * d/copyright: update upstream copyright years.

 -- Étienne Mollier <emollier@debian.org>  Wed, 05 Mar 2025 16:05:35 +0100

xmedcon (0.24.0-gtk3+dfsg-2) unstable; urgency=medium

  * Team upload.
  * d/control: xmedcon Breaks+Replaces medcon (<< 0.24)
    Thanks to Andreas Beckmann (Closes: #1077960)
  * d/libmdc3t64.lintian-overrides: match any version in libmdc3 conflict.

 -- Étienne Mollier <emollier@debian.org>  Mon, 05 Aug 2024 08:45:57 +0200

xmedcon (0.24.0-gtk3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.24.0-gtk3+dfsg.
    Fixes: CVE-2024-29421 (Closes: #1077369)
  * d/control: declare compliance to standards version 4.7.0.
  * d/copyright: bump upstream copyright year.
  * d/s/lintian-overrides: flag false positive caused by t64 transition.
  * d/xmedcon.desktop: remove unknown keyword "Medicine".
  * d/*.lintian-overrides: update override regarding t64 transition.
  * d/*.install: move desktop entry to xmedcon package.

 -- Étienne Mollier <emollier@debian.org>  Sun, 28 Jul 2024 22:42:03 +0200

xmedcon (0.23.0-gtk3+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1063277

 -- Steve Langasek <vorlon@debian.org>  Thu, 29 Feb 2024 07:37:28 +0000

xmedcon (0.23.0-gtk3+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Use secure URI in Homepage field.

 -- Andreas Tille <tille@debian.org>  Sun, 05 Feb 2023 19:35:32 +0100

xmedcon (0.22.0-gtk3+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jul 2022 09:58:09 +0200

xmedcon (0.21.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * d/control: build against GTK 3 and GDK pixbuf 2 (Closes: #967827)
  * d/{rules,xmedcon.install}: replace xmedconrc by xmedcon.css, following the
    move to GTK 3.
  * d/patches/:
    - Refresh use_debian_packaged_niftilib.patch
    - Refresh cross.patch
    - Add typos.patch: fix typos caught by lintian
  * d/copyright: update copyright years

 -- Étienne Mollier <emollier@debian.org>  Fri, 11 Feb 2022 21:15:23 +0100

xmedcon (0.16.3+dfsg-1) unstable; urgency=medium

  * Build-Depends: s/libnifti-dev/libnifti2-dev/
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * No quoted placeholder in mailcap entry

 -- Andreas Tille <tille@debian.org>  Mon, 07 Dec 2020 15:51:18 +0100

xmedcon (0.16.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add desktop file (thanks for the patch to thies@jochimsen.de)
    Closes: #924017
  * Remove Debian menu file since there is now a desktop file
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Remove deprecated Encoding key from desktop file debian/xmedcon.desktop.
  * Adjust d-shlibs to new libnifti soname
  * Install appdata
  * Fix dh_missing

 -- Andreas Tille <tille@debian.org>  Wed, 23 Sep 2020 22:06:00 +0200

xmedcon (0.16.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Fri, 25 Jan 2019 18:59:09 +0100

xmedcon (0.15.0+dfsg-3) unstable; urgency=medium

  * Help to enable cross building
    Closes: #905611
  * Standards-Version: 4.1.5

 -- Andreas Tille <tille@debian.org>  Tue, 07 Aug 2018 14:01:36 +0200

xmedcon (0.15.0+dfsg-2) unstable; urgency=medium

  * Add missing conflicts with old package name
    Closes: #901818

 -- Andreas Tille <tille@debian.org>  Tue, 19 Jun 2018 06:31:03 +0200

xmedcon (0.15.0+dfsg-1) unstable; urgency=medium

  * Use d-shlibs
  * Bump library name according to soversion bump
    Closes: #901634
  * Unversioned dev package
  * Repack to remove unneeded files

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jun 2018 16:12:12 +0200

xmedcon (0.15.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Mon, 28 May 2018 17:24:05 +0200

xmedcon (0.14.1-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * debhelper 10
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Sat, 07 Oct 2017 08:30:34 +0200

xmedcon (0.14.1-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Thu, 31 Dec 2015 14:17:58 +0100

xmedcon (0.13.0-2) unstable; urgency=medium

  * Fix typo in package description
    Closes: #589573
  * Verified proposed patch to build with libpng 1.5 and realise that
    the new upstream version
    Closes: #649795
    (Thanks to Nobuhiro Iwamatsu <iwamatsu@nigauri.org> for his patch
     anyway)

 -- Andreas Tille <tille@debian.org>  Tue, 17 Dec 2013 13:37:32 +0100

xmedcon (0.13.0-1) unstable; urgency=low

  * New upstream version
    Closes: #704438
  * debian/control:
     - Add Vcs fields
     - Add Homepage field
     - Team maintenance in Debian Med team
     - cme fix dpkg-control
       Closes: #662563
  * debian/source/format: 3.0 (quilt)
  * debian/watch: version=3
  * debian/libmdc2-dev.install: Drop *.la file
    Closes: #633237
  * debian/patches/man-spelling.patch: Enhance manpages syntax and spelling
  * debian/patches/code-spelling.patch: Fix spelling in library
  * debian/README.Debian: Removed now invalid information
  * debian/README.source: Add basic information about potential legal issues
    of ecat7 format
  * debian/copyright: DEP5 copyright
  * debian/patches/legal-dummies.patch: Restore the patches done by
    Roland Marcus Rutschmann except for gif format which is legal these days
  * debian/patches/avoid_linking_to_unneeded_libs.patch: Avoid useless linking
    against GTK library for command line tool
    Closes: #632163

 -- Andreas Tille <tille@debian.org>  Tue, 23 Jul 2013 08:46:18 +0200

xmedcon (0.10.7-1) unstable; urgency=low

  * New upstream release
  * Bumped Standards-Version to 3.9.1

 -- Roland Marcus Rutschmann <rudi@debian.org>  Thu, 14 Oct 2010 10:32:22 +0000

xmedcon (0.10.5-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Work around bug #519006 by passing -g0 to the compiler on mips and mipsel
    (closes: #590612).

 -- Jakub Wilk <jwilk@debian.org>  Fri, 13 Aug 2010 11:04:27 +0200

xmedcon (0.10.5-2) unstable; urgency=low

  * closes: #589881 (changed builddep to libnifti-dev from libnifti1-dev)
  * Bumped Standards-Version to 3.9.0

 -- Roland Marcus Rutschmann <rudi@debian.org>  Sun, 25 Jul 2010 12:04:16 +0000

xmedcon (0.10.5-1) unstable; urgency=low

  * New upstream release
  * Changed menu section from viewers to science/medicine
  * Bumped Standards-Version to 3.8.2

 -- Roland Marcus Rutschmann <rudi@debian.org>  Mon, 13 Jul 2009 19:58:05 +0000

xmedcon (0.10.4-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Thu, 20 Mar 2008 16:30:00 +0000

xmedcon (0.10.2-1) unstable; urgency=low

  * New upstream release
  * closes: #460373 (changed builddep to libnifti1-dev from libniftiio1-dev)
  * Fixed lintian warning 'description-contains-homepage' and moved homepage
    from Description to Homepage field for the package.
  * Bumped Standards-Version to 3.7.3
  * Removed NEWS file wich is kept up on upstreams web-page
  * moved xmedcon.xpm and Readme.siemens_mosaic to debian directory to keep
    all changes to this directory

 -- Roland Marcus Rutschmann <rudi@debian.org>  Sat, 05 Jan 2008 15:43:57 +0000

xmedcon (0.10.1-1) unstable; urgency=low

  * New upstream release
  * redid the changes from 0.9.1 leaving the source nifti libs configure and Makefile
    from upstream untouched to keep more compatible with upstream. libmdc is still
    linked against debians libnifti1
  * changed build-dependancies to libgtk-2.0-dev

 -- Roland Marcus Rutschmann <rudi@debian.org>  Thu, 29 Nov 2007 16:11:55 +0000

xmedcon (0.9.10-2) unstable; urgency=low

  * closes: #449032 (added dh_icons to rules to adjust to ubuntu source)
  * closes: #450312 (updated debian/watch file to new website)
  * closes: #450589 (patched xmedcon.m4 with quotes for AC_DEFUN to work with
    automake-1.9

 -- Roland Marcus Rutschmann <rudi@debian.org>  Thu, 08 Nov 2007 09:45:06 +0000

xmedcon (0.9.10-1) unstable; urgency=low

  * New upstream release
  * removed nifti libs from source as they are packaged in debian allready
  * patched ./configure and libs/Makefile.in to not fail after removal of libs/nifti

 -- Roland Marcus Rutschmann <rudi@debian.org>  Wed, 26 Sep 2007 10:57:23 +0200

xmedcon (0.9.9.7-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Mon, 24 Sep 2007 11:07:10 +0200

xmedcon (0.9.9.6.2-1) unstable; urgency=low

  * New upstream release
  * fixed dist-clean rule in debian/rules as suggested by lintian
  * changed dependencies to libniftiio1-dev to allow nifti files >2GB
  * changed menu entry from Apps to Applications

 -- Roland Marcus Rutschmann <rudi@debian.org>  Fri, 21 Sep 2007 16:57:35 +0200

xmedcon (0.9.9.6-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Mon, 16 Jul 2007 17:20:16 +0200

xmedcon (0.9.9.5.1-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Fri, 06 Jul 2007 15:55:02 +0200

xmedcon (0.9.9.4-2) unstable; urgency=low

  * Add xmedcon as viewer to /etc/mailcap
  * closes: #407016 (asking for that)

 -- Roland Marcus Rutschmann <rudi@debian.org>  Fri, 19 Jan 2007 21:13:10 +0100

xmedcon (0.9.9.4-1) unstable; urgency=low

  * New upstream release
  * closes: #401529 (Segmentation fault when processing compressed jpeg DICOM)
  * better support for nifti headers in debian

 -- Roland Marcus Rutschmann <rudi@debian.org>  Wed, 13 Dec 2006 14:40:46 +0000

xmedcon (0.9.9.3-3) unstable; urgency=low

  * put libniftiio0-dev in builddep (stupid mistake :-( )
  * closes: #402152 (FTBFS: checking for NIFTI package)

 -- Roland Marcus Rutschmann <rudi@debian.org>  Fri,  8 Dec 2006 16:27:02 +0100

xmedcon (0.9.9.3-2) unstable; urgency=low

  * really include nifti support with change to rules and configure, forgot in v-1 :-(

 -- Roland Marcus Rutschmann <rudi@debian.org>  Tue,  5 Dec 2006 11:44:28 +0100

xmedcon (0.9.9.3-1) unstable; urgency=low

  * New upstream release
  * limited nifti support. Build depends on libniftiio0-dev now
  * bumped to standards 3.7.2

 -- Roland Marcus Rutschmann <rudi@debian.org>  Mon,  2 Oct 2006 13:27:48 +0200

xmedcon (0.9.9.0-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Mon, 19 Dec 2005 21:02:43 +0100

xmedcon (0.9.8.7-1) unstable; urgency=low

  * New upstream release

 --  Roland Marcus Rutschmann <rudi@debian.org>  Mon, 10 Oct 2005 19:34:15 +0000

xmedcon (0.9.8.6-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Wed,  3 Aug 2005 20:08:55 +0200

xmedcon (0.9.8.5-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rudi@debian.org>  Mon, 23 May 2005 11:54:09 +0200

xmedcon (0.9.8.4-1) unstable; urgency=low

  * New upstream release
  * Removed dh_changelog as no changelog is included upstream anymore

 -- Roland Marcus Rutschmann <rudi@debian.org>  Thu, 17 Feb 2005 11:47:33 +0100

xmedcon (0.9.8.1-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Thu, 11 Nov 2004 15:53:37 +0100

xmedcon (0.9.7.3-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Thu, 16 Sep 2004 16:14:14 +0200

xmedcon (0.9.7.2-2) unstable; urgency=low

  * accidentially uploaded woody backported sources in 0.9.7.2-1
  * changed icon path back to /usr/share/pixmaps

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Mon,  9 Aug 2004 15:39:01 +0200

xmedcon (0.9.7.2-1) unstable; urgency=low

  * New upstream release
    closes: #262145 (New upstream release available 0.9.7.2)

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Thu,  5 Aug 2004 20:26:54 +0200

xmedcon (0.9.6-1) unstable; urgency=low

  * New upstream release
    closes: #252687 (New upstream release available 0.9.6)
  * Changed Build-Dependancy (again) to libpng12-dev

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Mon,  7 Jun 2004 09:00:14 +0200

xmedcon (0.9.3-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Tue, 30 Mar 2004 11:56:23 +0200

xmedcon (0.9.1-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Wed, 21 Jan 2004 12:46:38 +0100

xmedcon (0.9.0-1) unstable; urgency=low

  * New upstream release
  * Adapted headers of dummy-versions of m-gif.c and m-ecat*.c in debian's
    orig.tar.gz to headers of version 0.9.0

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Mon,  5 Jan 2004 20:21:50 +0100

xmedcon (0.8.13a-2) unstable; urgency=low

  * Bumped to standards 3.6.1
  * Changed Build-Dependancy to libpng2-dev (see README.Debian) and debhelper to
    version > 4.0.0
  * Removed libc6-dev dependency from libmdc2-dev (redundancy with build-depends)
  * Cleaned up rules a bit (deleting some dh_*  calls, replacing dh_movefile with
    dh_install)

 -- Roland Marcus Rutschmann <Rutschmann@gmx.de>  Mon,  5 Jan 2004 10:29:20 +0100

xmedcon (0.8.13a-1) unstable; urgency=low

  * fixed the orig.tar.gz source (patent related patches see README.Debian)

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Fri, 31 Oct 2003 10:17:40 +0100

xmedcon (0.8.13-1) unstable; urgency=low

  * New upstream release
  * Bumped to standards 3.5.10 (no changes needed)

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Mon, 16 Jun 2003 10:33:23 +0200

xmedcon (0.8.11-1) unstable; urgency=low

  * New upstream release
  * Upstream now uses autoconf-2.57, automake-1.7.4 & libtool-1.5
    closes: #190233

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Mon, 28 Apr 2003 17:54:45 +0200

xmedcon (0.8.9-1) unstable; urgency=low

  * New upstream release
  * changed maintainer's email-address to reflect the debian signed address
  * update standards to 3.5.9
  * reduced icon to 16 colors to satisfy lintian
  * moved libmdc to Section libs
  * moved libmdc-dev to Section libdevel
  * included Homepage in description (devel-ref 6.2.4)
  * renamed libmdc* to libmdc2 to satisfy library so naming convention

 -- Roland Marcus Rutschmann <rutschmann@gmx.net>  Wed, 16 Apr 2003 17:48:00 +0200

xmedcon (0.8.8-1) unstable; urgency=low

  * New upstream release
  * changed copyrights file to include all copyrights
  * included new (V2) Copyrights for ljpeglib by Cornell University

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Thu, 12 Dec 2002 08:58:57 +0100

xmedcon (0.8.7-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Tue, 19 Nov 2002 16:11:59 +0100

xmedcon (0.8.6-2) unstable; urgency=low

  * Removed 2 references to GIF Format in control-file

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Tue, 19 Nov 2002 15:34:07 +0100

xmedcon (0.8.6-1) unstable; urgency=low

  * New upstream release
  * First official Debian package
    closes: #141352
  * incorporate better dummy versions for non-GPL modules (see README.Debian)
  * Standards-Version: 3.5.7 (changed by Andreas Tille <tillle@debian.org>)
  * Minor spelling fixes in README.Debian (changed by Andreas Tille
    <tillle@debian.org>)
  * Added xmedcon.xpm icon acording to the policy of menu package and
    changed menu entry accordingly (changed by Andreas Tille
    <tillle@debian.org>)

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Mon, 18 Nov 2002 23:18:44 +0100

xmedcon (0.8.5-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Wed, 13 Nov 2002 20:37:04 +0100

xmedcon (0.8.4-3) unstable; urgency=low

  * removed license-problematic code from source and binaries

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Wed, 13 Nov 2002 18:16:42 +0100

xmedcon (0.8.4-2) unstable; urgency=low

  * updated build-debends
  * (x)medcon depends on same version libmdc

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Mon, 11 Nov 2002 19:09:08 +0100

xmedcon (0.8.4-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Mon,  4 Nov 2002 20:58:21 +0100

xmedcon (0.8.1-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Wed,  9 Oct 2002 17:43:36 +0200

xmedcon (0.8.0-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Wed, 11 Sep 2002 15:49:13 +0200

xmedcon (0.7.9-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Mon, 10 Jun 2002 14:45:36 +0200

xmedcon (0.7.8-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Tue, 14 May 2002 18:25:11 +0200

xmedcon (0.7.7-1) unstable; urgency=low

  * New upstream release

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Tue, 30 Apr 2002 14:42:40 +0200

xmedcon (0.7.5-1) unstable; urgency=low

  * New upstream release
  * upstream's manpage for xmedcon-config included
  * upstream's icon for xmedcon included in xmedcon.menu

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Wed, 27 Mar 2002 17:10:10 +0100

xmedcon (0.7.4-3) unstable; urgency=low

  * split into 4 packages

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Thu, 21 Mar 2002 18:27:43 +0100

xmedcon (0.7.4-2) unstable; urgency=low

  * added a quick Bugfix from Eric Nolf for m-acr.c
  * added copyright notice for gif and ecat

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Fri,  8 Mar 2002 13:18:52 +0100

xmedcon (0.7.4-1) unstable; urgency=low

  * Initial Release.
  * added NEWS frome http://xmedcon.sourceforge.net/new/

 -- Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>  Mon,  4 Mar 2002 21:42:01 +0100
