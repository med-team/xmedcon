# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# filename: xmedcon.spec.in                                               #
#                                                                         #
# UTILITY text: Medical Image Conversion Utility                          #
#                                                                         #
# purpose     : the RPM package spec template                             #
#                                                                         #
# project     : (X)MedCon by Erik Nolf                                    #
#                                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#
Name: xmedcon
Version: 0.25.0
Release: 1

BuildRoot: %{_tmppath}/%{name}-%{version}-root

Summary: a medical image conversion utility and library
License: (L)GPL
Group: Applications/Graphics

Source: http://prdownloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
URL: http://xmedcon.sourceforge.net
Packager: Erik Nolf

Requires: gtk3
BuildRequires:  gtk3-devel

%description
This project stands for Medical Image Conversion and is released under the
GNU's (L)GPL license. It bundles the C sourcecode, a library, a flexible
command-line utility and a graphical front-end based on the amazing Gtk+
toolkit.

Its main purpose is image conversion while preserving valuable medical
study information. The currently supported formats are: Acr/Nema 2.0,
Analyze (SPM), Concorde/uPET, DICOM 3.0, CTI ECAT 6/7, InterFile 3.3
and PNG or Gif87a/89a towards desktop applications.

%package devel
Summary: static libraries and header files for (X)MedCon development
Group: Development/Libraries
Requires: xmedcon = %{version}

%description devel

The xmedcon-devel package contains the header files and static libraries
necessary for developing programs that make use of the (X)MedCon library
(libmdc).

%prep
%setup -q

%build
%configure
make

%install
rm -rf ${RPM_BUILD_ROOT}
%makeinstall

%clean
rm -rf ${RPM_BUILD_ROOT}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog COPYING COPYING.LIB README REMARKS AUTHORS
%{_libdir}/*so.*
%{_libdir}/*.la
%{_bindir}/*
%{_sysconfdir}/*
%{_mandir}/man1/*
%{_datadir}/appdata/*


%files devel
%doc README COPYING COPYING.LIB
%defattr(-,root,root)
%{_mandir}/man3/*
%{_mandir}/man4/*
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.so
%{_datadir}/aclocal/*

%changelog
* Fri Feb 19 2021 Erik Nolf
- now requires gtk3
* Sat Dec 26 2015 Erik Nolf
- added xmedcon.appdata.xml to %files section
* Fri May 08 2009 Erik Nolf
- removed line with hardcoded sysconfdir value
* Sat Nov 20 2004 Erik Nolf
- multi-lib; bin & devel packages; install in /usr
