# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# filename: acinclude.m4                                                  #
#                                                                         #
# UTILITY text: Medical Image Conversion Utility                          #
#                                                                         #
# purpose     : m4 macro's for configure script                           #
#                                                                         #
# project     : (X)MedCon by Erik Nolf                                    #
#                                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

dnl Define some (X)MedCon macro's

dnl MDC_CHECK_GLIBSUPPORT(GLIBSUPPORT)
dnl check GLIB package
AC_DEFUN([MDC_CHECK_GLIBSUPPORT],[
AC_MSG_CHECKING([for glib support])
if test x$1 = xyes; then
  if test x$ac_cv_prog_glib = xno ; then
    GLIBSUPPORTED=0
    GLIBMDCETC="<unused>"
    mdc_cv_glibsupport=no
  else
    GLIBSUPPORTED=1
    if test x${prefix} != xNONE ; then
      GLIBMDCETC=${prefix}/etc
    else
      GLIBMDCETC=${ac_default_prefix}/etc
    fi
  fi
fi
if test x$mdc_cv_glibsupport = xno; then
  GLIBSUPPORTED=0
  GLIBMDCETC="<unused>"
  echo "no"
else
  echo "yes"
fi
])

dnl MDC_CHECK_GUI(GTKSUPPORTED)
AC_DEFUN([MDC_CHECK_GUI],[
AC_MSG_CHECKING([for GUI support])
if test $1 -eq 1; then
  if test x${prefix} != xNONE ; then
    XMDCETC=${prefix}/etc
  else
    XMDCETC=${ac_default_prefix}/etc
  fi
  mdc_cv_gui=yes
  echo "yes"
else
  XMDCETC="<unused>"
  mdc_cv_gui=no
  echo "no"
fi
])

dnl Get answer to set variable
AC_DEFUN([ReadAnswer],[
read answ 
if test x$answ = xn -o x$answ = xno -o x$answ = xN -o x$answ = xNO;
then
  echo "no" 
else
  echo "yes"
fi
])

