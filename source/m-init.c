/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-init.c                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : initialize library                                       *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : MdcIgnoreSIGFPE() - ignore floating point exception      *
 *                MdcAcceptSIGFPE() - accept floating point exception      *
 *                MdcSetLocale()    -   set POSIX locale and preserve      *
 *                MdcUnsetLocale()  - unset POSIX locale and retore        *
 *                MdcSetEndianness()- set host and write endianness        *
 *                MdcInit()         - library usage initialized            *
 *                MdcFinish()       - library usage finished               *
 *                                                                         *
 * Notes        : no dynamic memory allocations here                       *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-defs.h"

#include <signal.h>
#include <locale.h>
#include <assert.h>
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif

#include "m-init.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

static void (*mdc_old_handler)(int);

static char *mdc_old_locale = NULL;

extern Int8 MDC_HOST_ENDIAN, MDC_WRITE_ENDIAN;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void MdcIgnoreSIGFPE(void)                  /* before Accept! */
{
  mdc_old_handler = signal(SIGFPE, SIG_IGN);
}

void MdcAcceptSIGFPE(void)                  /* after  Ignore! */
{
  signal(SIGFPE, mdc_old_handler);
}

void MdcSetLocale(void)
{
  char *cur_locale;
  static char locale_string[30];

  /* preserve current locale */
  cur_locale = setlocale(LC_ALL,NULL);
  if (cur_locale == NULL) return;

  if (strlen(cur_locale) >= 30) return;

  strcpy(locale_string,cur_locale);
  mdc_old_locale = locale_string;

  /* set POSIX locale */
  setlocale(LC_ALL, "POSIX");

}

void MdcUnsetLocale(void)
{
  if (mdc_old_locale == NULL) return;

  /* restore previous locale */
  setlocale(LC_ALL,mdc_old_locale);

  /* clean up */
  mdc_old_locale = NULL;

}

void MdcSetEndianness(void)
{
  Int8 host_endian=MDC_UNKNOWN_ENDIAN;

  union {
    Uint32 value;
    Uint8  data[sizeof(Uint32)];
  }number;

  number.data[0] = 0x00;
  number.data[1] = 0x01;
  number.data[2] = 0x02;
  number.data[3] = 0x03;

  switch (number.value) {
    case UINT32_C(0x00010203):
        host_endian = MDC_BIG_ENDIAN;
        break;
    case UINT32_C(0x03020100):
        host_endian = MDC_LITTLE_ENDIAN;
        break;
    default: /* unsupported endianness */
        host_endian = MDC_UNKNOWN_ENDIAN;
        assert(host_endian != MDC_UNKNOWN_ENDIAN);
  }

  MDC_HOST_ENDIAN  = host_endian;
  MDC_WRITE_ENDIAN = host_endian;
}

void MdcInit(void)
{
  /* ignore floating point exception */
  MdcIgnoreSIGFPE();

  /* set POSIX locale */
  MdcSetLocale();

  /* set host and write endianness */
  MdcSetEndianness();
}

void MdcFinish(void)
{
  /* accept floating point exception */
  MdcAcceptSIGFPE();

  /* unset POSIX locale */
  MdcUnsetLocale();

}
