/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xmedcon.c                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : main routine                                             *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : main()      - Main XMedCon routine                       *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */


/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/
int main(int argc, char *argv[])
{
    FILEINFO fi;

    my.fi = &fi;

    /* init library */
    MdcInit();

    /* set progress functions */
    MdcProgress  = XMdcProgressBar;
    MDC_PROGRESS = MDC_YES;


    /* some MedCon options initialized */
    XMDC_GUI=MDC_YES;
    MDC_INFO=MDC_NO;
    MDC_VERBOSE=MDC_NO;

    MdcPrintShortInfo();

    /* some XMedCon options initialized */
    sLabelSelection.CurState = MDC_YES;
    sLabelSelection.CurColor = XMDC_LABEL_YELLOW;
    sLabelSelection.CurStyle = XMDC_LABEL_STYLE_ABS;
    sColormapSelection.CurMap = MDC_MAP_GRAY;
    sExtractSelection.input = &mdcextractinput;
    sExtractSelection.input->style = MDC_INPUT_NORM_STYLE;
    sResizeSelection.CurType = XMDC_RESIZE_ORIGINAL;
    sPagesSelection.CurType = XMDC_PAGES_FRAME_BY_FRAME;
    sRenderSelection.Interp = GDK_INTERP_BILINEAR; /* GDK_INTERP_HYPER seems deprecated */
    sGbc.mod.gamma = 256;
    sGbc.mod.brightness = 256;
    sGbc.mod.contrast = 256;
    XMdcSetGbcCorrection(&sGbc.mod);
    sGbc.image = NULL;
    MdcInitRawPrevInput();

    /* preserve POSIX locale: decimal = point */
    gtk_disable_setlocale();

    /* initialize the GTK engine */
    gtk_init(&argc, &argv);

    /* initialize XMedCon global struct */
    my.viewwindow = NULL; XMdcStructsReset();


    /* process possible medcon settings */
    switch (argc) {
      case 1:  /* without arguments */
          break;
      case 2:  /* one     argument  */
          if ( ( strcmp(argv[1],"-h") == 0     )  ||
               ( strcmp(argv[1],"/?") == 0     )  ||
               ( strcmp(argv[1],"--help") == 0 ) ) {
            /* requesting merely some help */
            MdcPrintUsage(argv[0]);

          }else{

            if ( argv[1][0] == '-' ) {
              /* an option, sorry no files starting with - allowed */
              if (MdcHandleArgs(&fi,argc,argv,1) != MDC_OK) {
                MdcPrintUsage(argv[0]);
              }

            }else{
              /* must be an image file */
              mdc_arg_files[0]=argv[1]; mdc_arg_total[MDC_FILES]=1;
            }
          }
          break;
      default: /* several arguments */
          if (MdcHandleArgs(&fi,argc,argv,1) != MDC_OK) {
            MdcPrintUsage(argv[0]);
          }
    }

    /* set the initial palette we want */
    sColormapSelection.Nr = MDC_COLOR_MAP;
    switch (MDC_COLOR_MAP) {
      case MDC_MAP_GRAY:
        sColormapSelection.CurMap = MDC_MAP_GRAY;
        break;
      case MDC_MAP_INVERTED:
        sColormapSelection.CurMap = MDC_MAP_INVERTED;
        break;
      case MDC_MAP_RAINBOW:
        sColormapSelection.CurMap = MDC_MAP_RAINBOW;
        break;
      case MDC_MAP_COMBINED:
        sColormapSelection.CurMap = MDC_MAP_COMBINED;
        break;
      case MDC_MAP_HOTMETAL:
        sColormapSelection.CurMap = MDC_MAP_HOTMETAL;
        break;
      case MDC_MAP_LOADED:
        sColormapSelection.CurMap = MDC_MAP_LOADED;
    }

    XMdcCreateLogConsole();

    XMdcConfigureByCss();

    my.mainwindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    g_signal_connect(my.mainwindow, "destroy",
                       G_CALLBACK(XMdcMedconQuit),
                       "WM destroy");
    gtk_window_set_title(GTK_WINDOW(my.mainwindow), MDC_PRGR);
    gtk_window_set_resizable(GTK_WINDOW(my.mainwindow), TRUE);

    XMdcAttachMainMenu(my.mainwindow);

    gtk_widget_show_all(my.mainwindow);

    /* get some preferred stuff */
    XMdcMakeMyCursors(); /* cursors for over pixmaps images */

    XMdcGetScreenWorkingArea(); /* get screen working area  */

    /* display file on argument */
    if (mdc_arg_total[MDC_FILES] == 1) XMdcDisplayFile(mdc_arg_files[0]);

    /* disable further stdin input */
    MDC_FILE_STDIN = MDC_NO;

    gtk_main();

    /* finish library */
    MdcFinish();

    return(0);
}
