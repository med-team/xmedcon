/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xreslice.c                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : volume reslice routines                                  *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcResliceImages()                 - Reslice the images *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcResliceImages(GtkWidget *widget, guint projection)
{
  char *msg;
  Int8 newproj = (Int8)projection;

  if (XMdcNoFileOpened()) return;

  msg = MdcCheckReslice(my.fi,newproj);
  if (msg != NULL) { XMdcDisplayWarn("Reslice - %s",msg); return; }

  XMdcProgressBar(MDC_PROGRESS_BEGIN,0.,"Reslicing images:");

  XMdcViewerHide();
  XMdcViewerEnableAutoShrink();
  XMdcViewerReset();

  msg = MdcResliceImages(my.fi, newproj);
  if (msg != NULL) XMdcDisplayErr("Reslice - %s",msg);

  XMdcDisplayImages();

  XMdcProgressBar(MDC_PROGRESS_END,0.,NULL);

  XMDC_FILE_TYPE = XMDC_RESLICE;

}

