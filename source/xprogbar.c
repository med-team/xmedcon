/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xprogbar.c                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : progressbar routines                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Note         : This code gets linked in (X)MedCon library with X-support*
 *                                                                         *
 * Functions    : XMdcProgressBar()           - Run progress bar           *
 *                XMdcUpdateDrawing()         - Update queued drawings     *
 *                XMdcUpdateProgressBar()     - Update progressbar         *
 *                XMdcSetProgressBar()        - Set progressbar            *
 *                XMdcIncrProgressBar()       - Increment progressbar      *
 *                XMdcCreateProgressBar()     - Create   progressbar       *
 *                XMdcBeginProgressBar()      - Begin of progressbar       *
 *                XMdcEndProgressBar()        - End   of progressbar       *
 *                XMdcHandleBarLabel()        - Handle its label           *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

static GtkWidget *pbarwindow=NULL;
static GtkWidget *pbar=NULL;
static GtkWidget *pbarlabel=NULL;
static char barstring[26];
static gfloat pvalue = 0.;

Uint8  XMDC_DOBAR = MDC_NO;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcProgressBar(int type, float value, char *label)
{
  switch (type) {
    case MDC_PROGRESS_BEGIN: XMdcBeginProgressBar(label);
        break;
    case MDC_PROGRESS_SET  : XMdcSetProgressBar(value);
        break;
    case MDC_PROGRESS_INCR : XMdcIncrProgressBar(value);
        break;
    case MDC_PROGRESS_END  : XMdcEndProgressBar();
        break;
  }

}

void XMdcUpdateDrawing(void)
{
  while (gtk_events_pending())
       gtk_main_iteration();
}

void XMdcUpdateProgressBar(void)
{
  if (XMDC_DOBAR) {
    if (pvalue > 1.0 ) pvalue = 1.0;
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), pvalue);
  }
  XMdcUpdateDrawing();

}

void XMdcSetProgressBar(float set)
{
  if (XMDC_DOBAR) pvalue = (gfloat)set;

  XMdcUpdateProgressBar();

}

void XMdcIncrProgressBar(float incr)
{
  if (XMDC_DOBAR) pvalue += (gfloat)incr;

  XMdcUpdateProgressBar();
}

void XMdcCreateProgressBar(char *labelstring)
{
     GtkWidget *vbox;

     pvalue = 0.; XMDC_DOBAR = MDC_YES;

     pbarwindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);

     g_signal_connect(pbarwindow, "delete-event",
                         G_CALLBACK(XMdcPreventDelete), NULL);
     g_signal_connect(pbarwindow, "destroy",
                         G_CALLBACK(gtk_widget_destroy),NULL);
     gtk_window_set_title(GTK_WINDOW(pbarwindow),"Progress");
     gtk_window_set_position(GTK_WINDOW(pbarwindow),GTK_WIN_POS_CENTER);
     gtk_container_set_border_width(GTK_CONTAINER(pbarwindow), 0);

     vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
     gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
     gtk_container_add(GTK_CONTAINER(pbarwindow),vbox);
     gtk_widget_show(vbox);

     pbarlabel = gtk_label_new(XMdcHandleBarLabel(labelstring));
     gtk_widget_set_name(pbarlabel,"BarLabel");
     gtk_widget_set_halign(pbarlabel, GTK_ALIGN_START);
     gtk_widget_set_valign(pbarlabel, GTK_ALIGN_CENTER);
     gtk_box_pack_start(GTK_BOX(vbox),pbarlabel,TRUE, TRUE, 0);
     gtk_widget_show(pbarlabel);

     pbar = gtk_progress_bar_new();
     gtk_widget_set_size_request(pbar, 200, 20);
     gtk_box_pack_start(GTK_BOX(vbox), pbar, TRUE, TRUE, 0);
     gtk_widget_show(pbar);

     gtk_widget_show(pbarwindow);

     XMdcUpdateProgressBar();

}

void XMdcBeginProgressBar(char *labelstring)
{
  if (XMDC_DOBAR) {
    pvalue = 0.0;
    gtk_label_set_text(GTK_LABEL(pbarlabel),XMdcHandleBarLabel(NULL));
    XMdcUpdateProgressBar();
    gtk_label_set_text(GTK_LABEL(pbarlabel),XMdcHandleBarLabel(labelstring));
    XMdcUpdateProgressBar();
  }else{
    XMdcCreateProgressBar(labelstring); XMdcMainWidgetsInsensitive();
  }
}

void XMdcEndProgressBar(void)
{
  if (XMDC_DOBAR) {
    gtk_widget_destroy(pbarwindow);
    pbarwindow = NULL;
    pbar = NULL;
    pbarlabel = NULL;
    XMDC_DOBAR = MDC_NO;
    XMdcMainWidgetsResensitive();
  }
}

char *XMdcHandleBarLabel(char *labelstring)
{
  Uint8 i;

  if (labelstring == NULL) {
    /* clean with spaces  */
    for (i=0;i<25;i++) barstring[i]=' ';
    barstring[25]='\0';
  }else{
    /* fill out new label */
    sprintf(barstring,"%-25s",labelstring);
    if (strlen(labelstring) < 25) {
      for (i=strlen(labelstring); i < 25 ; i++) barstring[i]=' ';
    }
  }

  barstring[25]='\0';

  return(barstring);
}
