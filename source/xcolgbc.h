/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xcolgbc.h                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xcolgbc.c header file                                    *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XCOLGBC_H__
#define __XCOLGBC_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/
gboolean XMdcColGbcCorrectDraw(GtkWidget *widget
    ,GdkEventExpose *event, gpointer data);
void XMdcColGbcCorrectUpdate(void);
void XMdcColGbcCorrectMakeIcons(void);
void XMdcColGbcCorrectAddImg(GtkWidget *w);
void XMdcColGbcCorrectModValue(GtkWidget *widget, SliderValueStruct *v);
void XMdcColGbcCorrectResetValue(GtkWidget *widget, SliderValueStruct *v);
void XMdcColGbcCorrectAddOneSlider(GtkWidget *w, int *value, GtkWidget *ic
                                               , SliderValueStruct *sv);
void XMdcColGbcCorrectAddSliders(GtkWidget *w);
void XMdcColGbcCorrectApply(GtkWidget *widget, gpointer data);
void XMdcColGbcCorrectSel(GtkWidget *widget, Uint32 nr);

#endif

