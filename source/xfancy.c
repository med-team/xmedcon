/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xfancy.c                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : fancy routines                                           *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcMakeMyCursors()     - Make my preferred cursors      *
 *                XMdcFreeMyStuff()       - Free my preferred stuff        *
 *                XMdcAbout()             - Hello                          *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcMakeMyCursors(void)
{
  GdkDisplay *display = gdk_display_get_default();

  handcursor = gdk_cursor_new_from_name(display, "pointer");
  zoomcursor = gdk_cursor_new_from_name(display, "zoom-in");

/* eNlf: leave out an error dialog; no need to harras user 
  if (handcursor == NULL || zoomcursor == NULL)
    XMdcDisplayErr("Couldn't get cursor types");
*/
}

void XMdcFreeMyStuff(void)
{
  /* the cursors */
  if (handcursor != NULL) g_object_unref(handcursor);
  if (zoomcursor != NULL) g_object_unref(zoomcursor);
}

void XMdcAbout(GtkWidget *widget, gpointer data)
{
  GtkWidget *window=NULL;
  GtkWidget *box1;
  GtkWidget *label;
  GtkWidget *button;

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g_signal_connect(window, "destroy",
                     G_CALLBACK(gtk_widget_destroy),NULL);
  gtk_window_set_title(GTK_WINDOW(window),"Hello There");

  gtk_container_set_border_width(GTK_CONTAINER(window), 0);

  box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add(GTK_CONTAINER(window),box1);
  gtk_widget_show(box1);

  sprintf(mdcbufr," %s \n\n <a href=\"%s\">%s</a> \n\n",
                  MdcGetLibLongVersion(),
                  XMDCWEB, XMDCWEB);
  strcat(mdcbufr," With special regards to You \n\n" \
                 " Licensed  by  Murphy's Law \n" \
                 " Enjoy it ...");
  label = gtk_label_new(mdcbufr);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  g_signal_connect(label, "activate-link", G_CALLBACK(XMdcHelp), NULL);
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_CENTER);
  gtk_box_pack_start(GTK_BOX(box1),label,TRUE,TRUE,5);
  gtk_widget_show(label);

  button = gtk_button_new_with_label("Bye");
  g_signal_connect_swapped(button, "clicked",
                     G_CALLBACK(gtk_widget_destroy),
                     window);
  gtk_box_pack_start(GTK_BOX(box1),button,TRUE,TRUE,5);
  gtk_widget_show(button);

  XMdcShowWidget(window);

}

