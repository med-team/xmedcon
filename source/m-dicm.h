/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-dicm.h                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-dicm.c header file                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_DICM_H__
#define __M_DICM_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

#define MDC_DICM_SIG        "dicm"

#define MDC_DICM_PIXEL_TYPE BIT16_U

/* maximun length UID string */
#define MDC_UID_MAXSTR          64

/* some specific type UID values */
#define MDC_TYPE_UID_UNKNOWN         0   /*  anything else */
#define MDC_TYPE_UID_MEDIA_INSTANCE  1   /*  0x0002:0x0003 */
#define MDC_TYPE_UID_CREATOR         2   /*  0x0002:0x0014 */
#define MDC_TYPE_UID_SOP_INSTANCE    3   /*  0x0008:0x0018 */
#define MDC_TYPE_UID_STUDY           4   /*  0x0020:0x000D */
#define MDC_TYPE_UID_SERIES          5   /*  0x0020:0x000E */
#define MDC_TYPE_UID_FRAME           6   /*  0x0020:0x0052 */

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckDICM(FILEINFO *fi);
const char *MdcReadDICM(FILEINFO *fi);
const char *MdcWriteDICM(FILEINFO *fi);
int MdcCheckMosaic(FILEINFO *fi, MDC_DICOM_STUFF_T *dicom);

#endif

