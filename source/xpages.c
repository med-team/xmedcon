/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xpages.c                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : image pages routines                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcPagesCbSelected()        - Handle selected page      *
 *                XMdcPagesCbGoTo()            - Go to a specified page    *
 *                XMdcPagesCreateMenu()        - Create pages menu         *
 *                XMdcPagesCbNext()            - Go to next     page       *
 *                XMdcPagesCbPrev()            - Go to previous page       *
 *                XMdcPagesSelCallbackApply()  - Apply new page display    *
 *                XMdcPagesSel()               - Select page display       *
 *                XMdcPagesGetNrImages()       - Get images per page       *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

static GtkWidget *wpages=NULL;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcPagesCbSelected(GtkWidget *widget, gpointer data)
{
  Uint32 pagenr = (Uint32) gtk_combo_box_get_active(GTK_COMBO_BOX(widget));

  if (pagenr != my.curpage) {

    my.curpage = pagenr;
    MdcDebugPrint("one-based selected page = %u",my.curpage + 1);

    XMdcMainWidgetsInsensitive();

    XMdcRemovePreviousColorMap();
    XMdcRemovePreviousImages();
    XMdcBuildColorMap();
    XMdcBuildCurrentImages();

    my.prevpage = my.curpage;

    XMdcMainWidgetsResensitive();
  }
}

gboolean XMdcPagesCbGoTo(GtkWidget *spinner, gpointer data)
{
  GtkSpinButton *spin = GTK_SPIN_BUTTON(spinner);
  Uint32 page, newpage;

  page = (Uint32) gtk_spin_button_get_value_as_int(spin); newpage = page - 1;

  if ( page == 0 || page > my.number_of_pages ) {

    XMdcDisplayWarn("Need a number between [1 , %u]",my.number_of_pages);

  }else{

    gtk_combo_box_set_active(GTK_COMBO_BOX(my.pagemenu), (gint)newpage);

  }

  return(TRUE);

}

GtkWidget *XMdcPagesCreateMenu(void)
{
  GtkWidget *menu;
  Uint32 i;

  menu = gtk_combo_box_text_new();

  for(i=0; i<my.number_of_pages; i++) {

    sprintf(mdcbufr,"Page: %4u/%u",i+1,my.number_of_pages);
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(menu), mdcbufr);

  }

  g_signal_connect(menu, "changed",
                 G_CALLBACK(XMdcPagesCbSelected),
                 NULL);

  gtk_combo_box_set_active(GTK_COMBO_BOX(menu), (gint) my.curpage);

  return(menu);

}

void XMdcPagesCbNext(GtkWidget *widget, gpointer data)
{

  Uint32 nextpage = my.curpage + 1;

  if (nextpage < my.number_of_pages) {
    gtk_combo_box_set_active(GTK_COMBO_BOX(my.pagemenu), nextpage);
  }
}

void XMdcPagesCbPrev(GtkWidget *widget, gpointer data)
{
  Uint32 prevpage = my.curpage;

  if (my.curpage > 0) {
    prevpage = my.curpage - 1;
    gtk_combo_box_set_active(GTK_COMBO_BOX(my.pagemenu), prevpage);
  }
}

void XMdcPagesSelCallbackApply(GtkWidget *widget, gpointer data)
{
  Int8 type=XMDC_PAGES_FRAME_BY_FRAME;

  MdcDebugPrint("pages layout: ");
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sPagesSelection.FrameByFrame)))         {
    MdcDebugPrint("\tper frame"); type = XMDC_PAGES_FRAME_BY_FRAME;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sPagesSelection.SliceBySlice)))   {
    MdcDebugPrint("\tper slice"); type = XMDC_PAGES_SLICE_BY_SLICE;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sPagesSelection.ScreenFull)))     {
    MdcDebugPrint("\tscreen full"); type = XMDC_PAGES_SCREEN_FULL;
  }

  if (type != sPagesSelection.CurType) {
    sPagesSelection.CurType = type;
    if (XMDC_FILE_OPEN == MDC_YES) {
      /* prevent useless reprocessing */
      if (my.fi->number == 1) return;

      XMdcProgressBar(MDC_PROGRESS_BEGIN,0.,"Redisplay images:");
      XMdcViewerHide();
      XMdcViewerEnableAutoShrink();
      XMdcViewerReset();
      XMdcDisplayImages();
      XMdcProgressBar(MDC_PROGRESS_END,0.,NULL);
    }
  }
}

void XMdcPagesSel(void)
{

  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *separator;
  GSList *group;

  if (wpages == NULL) {

    wpages = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    g_signal_connect(wpages, "destroy",
                       G_CALLBACK(XMdcMedconQuit),NULL);
    g_signal_connect(wpages, "delete_event",
                       G_CALLBACK(XMdcHandlerToHide),NULL);

    gtk_window_set_title(GTK_WINDOW(wpages),"Pages Selection");

    gtk_container_set_border_width (GTK_CONTAINER (wpages), 0);

    box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add (GTK_CONTAINER (wpages), box1);
    gtk_widget_show(box1);

    /* create upper box - Initial Resize */
    box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER(box2), 5);
    gtk_widget_show(box2);

    box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_pack_start(GTK_BOX(box2), box3, TRUE, TRUE, 0);
    gtk_widget_show(box3);

    frame = gtk_frame_new("Display Pages");
    gtk_box_pack_start(GTK_BOX (box3), frame, TRUE, TRUE, 0);
    gtk_widget_show(frame);

    box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(frame), box4);
    gtk_container_set_border_width(GTK_CONTAINER(box4), 5);
    gtk_widget_show(box4);

    button = gtk_radio_button_new_with_label(NULL, "frame by frame (volume)");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sPagesSelection.CurType == XMDC_PAGES_FRAME_BY_FRAME)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sPagesSelection.FrameByFrame = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "slice by slice (image)");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sPagesSelection.CurType == XMDC_PAGES_SLICE_BY_SLICE)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sPagesSelection.SliceBySlice = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "whole screen filled");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sPagesSelection.CurType == XMDC_PAGES_SCREEN_FULL)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sPagesSelection.ScreenFull = button;

    /* create horizontal separator */
    separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start (GTK_BOX (box1), separator, FALSE, FALSE, 0);
    gtk_widget_show (separator);

    /* create bottom button box */
    box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(box1), box2, TRUE, TRUE, 2);
    gtk_widget_show(box2);

    button = gtk_button_new_with_label("Apply");
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
    g_signal_connect_swapped(button, "clicked",
                       G_CALLBACK(gtk_widget_hide), wpages);
    g_signal_connect(button, "clicked",
                       G_CALLBACK(XMdcPagesSelCallbackApply), NULL);
    gtk_widget_show(button);

    button = gtk_button_new_with_label ("Cancel");
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
    g_signal_connect_swapped(button, "clicked",
        G_CALLBACK(gtk_widget_hide), wpages);
    gtk_widget_show(button);

  }else{
    /* set buttons to appropriate state */
    GtkWidget *b1, *b2, *b3;

    gtk_widget_hide(wpages);

    b1 = sPagesSelection.FrameByFrame;
    b2 = sPagesSelection.SliceBySlice;
    b3 = sPagesSelection.ScreenFull;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3),FALSE);
    switch (sPagesSelection.CurType) {
      case XMDC_PAGES_FRAME_BY_FRAME :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1),TRUE); break;
      case XMDC_PAGES_SLICE_BY_SLICE :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2),TRUE); break;
      case XMDC_PAGES_SCREEN_FULL    :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3),TRUE); break;
    }

  }

  XMdcShowWidget(wpages);
}

Uint32 XMdcPagesGetNrImages(void)
{
  Uint32 nr=0;

  switch (sPagesSelection.CurType) {
    case XMDC_PAGES_FRAME_BY_FRAME: nr = my.fi->dim[3];
        break;
    case XMDC_PAGES_SLICE_BY_SLICE: nr = 1;
        break;
    case XMDC_PAGES_SCREEN_FULL   : nr = my.fi->number;
        break;
  }

  return(nr);
}
