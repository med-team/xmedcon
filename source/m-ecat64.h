/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-ecat64.h                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-ecat64.c header file                                   *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_ECAT64_H__
#define __M_ECAT64_H__

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-matrix.h"

/****************************************************************************                              D E F I N E S
****************************************************************************/

#define MDC_ECAT6_MAX_PLANES  1024
#define MDC_ECAT6_MAX_FRAMES   512
#define MDC_ECAT6_MAX_GATES     64
#define MDC_ECAT6_MAX_BEDS      16

#define MDC_ECAT6_MAX_DIMS     256

#define MDC_ECAT6_SYST_TYPE    951

#define MDC_ECAT6_SCAN_FILE     1
#define MDC_ECAT6_IMAGE_FILE    2
#define MDC_ECAT6_ATTN_FILE     3
#define MDC_ECAT6_NORM_FILE     4

#define MDC_ECAT6_ACQTYPE_UNKNOWN            0
#define MDC_ECAT6_ACQTYPE_BLANK              1
#define MDC_ECAT6_ACQTYPE_TRANSMISSION       2
#define MDC_ECAT6_ACQTYPE_STATIC_EMISSION    3
#define MDC_ECAT6_ACQTYPE_DYNAMIC_EMISSION   4
#define MDC_ECAT6_ACQTYPE_GATED_EMISSION     5
#define MDC_ECAT6_ACQTYPE_TRANSMISSION_RECT  6
#define MDC_ECAT6_ACQTYPE_EMISSION_RECT      7
#define MDC_ECAT6_ACQTYPE_WHOLE_BODY_TRANSM  8
#define MDC_ECAT6_ACQTYPE_WHOLE_BODY_STATIC  9

#define MDC_MAX_ECATDATATYPES        8
#define MDC_MAX_ECATDATATYPES_SIZE  11
#define MDC_MAX_ECATFILETYPES        6
#define MDC_MAX_ECATFILETYPES_SIZE  14
#define MDC_MAX_ECATACQTYPES        10
#define MDC_MAX_ECATACQTYPES_SIZE   25
#define MDC_MAX_ECATFLTRTYPES        8
#define MDC_MAX_ECATFLTRTYPES_SIZE   8
#define MDC_MAX_ECATQUANTTYPES      12
#define MDC_MAX_ECATQUANTTYPES_SIZE 25

#define MDC_MAX_ECATSYSTEMTYPES      6

#define MDC_ECAT6_RECON_METHOD "ACS reconstruction with new AP"

#define MDC_ECAT6_SORT_ANATOMICAL 1
#define MDC_ECAT6_SORT_BYFRAME    2


/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckECAT6(FILEINFO *fi);
const char *MdcReadECAT6(FILEINFO *fi);
const char *MdcWriteECAT6(FILEINFO *fi);
float MdcGetSliceLocation(FILEINFO *fi, Int32 img);
int MdcGetFilterCode(char *string);
void MdcFillMainHeader(FILEINFO *fi, Mdc_Main_header *mh);
void MdcFillImageSubHeader(FILEINFO *fi, Mdc_Image_subheader *ish,int type,Int32 img,Int32 matnum,Uint32 NEWSIZE);
void MdcPrintEcatInfoDB(Mdc_Main_header *mh);

#endif

