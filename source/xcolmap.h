/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xcolmap.h                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xcolmap.c header file                                    *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XCOLMAP_H__
#define __XCOLMAP_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcRemovePreviousColorMap(void);
void XMdcApplyNewColorMap(int map);
gboolean XMdcColorMapCallbackClicked(GtkWidget *widget, GdkEventButton *button, gpointer data);
void XMdcColorMapSelCallbackApply(GtkWidget *widget, gpointer data);
gboolean XMdcColorMapSel(void);
gboolean XMdcColorMapCallbackDraw(GtkWidget *widget, GdkEventExpose *event, gpointer data);
void XMdcApplyMapPlace(int place);
void XMdcMapPlaceSelCallbackApply(GtkWidget *widget, gpointer data);
gboolean XMdcMapPlaceSel(void);
void XMdcBuildColorMap(void);
int XMdcLoadLUT(char *lutname);
gboolean XMdcChangeLUT(GtkWidget *spinner, gpointer data);
gboolean XMdcMapNotAllowed(void);

#endif

