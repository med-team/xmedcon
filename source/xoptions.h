/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xoptions.h                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xoptions.c header file                                   *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XOPTIONS_H__
#define __XOPTIONS_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcOptionsMedconCallbackApply(GtkWidget *widget, gpointer data);
void XMdcOptionsMedconAddTabPixels(GtkWidget *notebook);
void XMdcOptionsMedconAddTabFiles(GtkWidget *notebook);
void XMdcOptionsMedconAddTabSlices(GtkWidget *notebook);
void XMdcOptionsMedconAddTabFormats(GtkWidget *notebook);
void XMdcOptionsMedconAddTabMosaic(GtkWidget *notebook);
void XMdcOptionsMedconSel(GtkWidget *widget, gpointer data);
void XMdcOptionsRenderSel(GtkWidget *widget, gpointer data);
void XMdcOptionsResizeSel(GtkWidget *widget, gpointer data);
void XMdcOptionsColorMapSel(GtkWidget *widget, gpointer data);
void XMdcOptionsLabelSel(GtkWidget *widget, gpointer data);
void XMdcOptionsPagesSel(GtkWidget *widget, gpointer data);
void XMdcSensitiveBitsUsed12(GtkWidget *widget, gpointer data);
void XMdcUnsensitiveBitsUsed12(GtkWidget *widget, gpointer data);
void XMdcToggleSensitivityMosaic(GtkWidget *widget, gpointer data);
void XMdcToggleSensitivityForced(GtkWidget *widget, gpointer data);
void XMdcInitMosaicFrame(void);
void XMdcToggleSensitivityCine(GtkWidget *widget, gpointer data);
void XMdcInitCineButtons(void);
void XMdcToggleSensitivityColor(GtkWidget *widget, gpointer data);
void XMdcInitColorButtons(void);
#endif

