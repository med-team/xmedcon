/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xutils.h                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xutils.c header file                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XUTILS_H__
#define __XUTILS_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcMedconQuit(GtkWidget *widget, gpointer data);
void XMdcMainWidgetsInsensitive(void);
void XMdcMainWidgetsResensitive(void);
void XMdcConfigureByCss(void);
void XMdcAskYesNo(GCallback YesFunc, GCallback NoFunc, char *question);
void XMdcRunDialog(GtkWidget *dialog);
void XMdcShowWidget(GtkWidget *widget);
void XMdcSetGbcCorrection(ColorModifier *mod);
Uint8 *XMdcBuildRgbImage(Uint8 *img8, Int16 type, Uint32 pixels, Uint8 *vgbc);
GdkPixbuf *XMdcBuildGdkPixbuf(Uint8 *img8, Uint32 w, Uint32 h, Int16 type, Uint8 *vgbc);
GdkPixbuf *XMdcBuildGdkPixbufFI(FILEINFO *fi,Uint32 i,Uint8 *vgbc);
gboolean XMdcPreventDelete(GtkWidget *widget, GdkEvent *event, gpointer data);
gboolean XMdcHandlerToHide(GtkWidget *widget, GdkEvent *event, gpointer data);
void XMdcFreeRGB(guchar *pixdata, gpointer data);
void XMdcToggleVisibility(GtkWidget *widget);
void XMdcSetImageScales(void);
Uint32 XMdcScaleW(Uint32 width);
Uint32 XMdcScaleH(Uint32 height);
void XMdcGetScreenWorkingArea(void);

#endif

