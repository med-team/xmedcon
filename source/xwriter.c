/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xwriter.c                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : file writer                                              *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcWriteFile()       - Write file routine               *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int XMdcWriteFile(int format_to_save)
{
  char *msg;

  switch (MDC_FILE_SPLIT) {
        case MDC_SPLIT_PER_SLICE:
            msg = MdcSplitSlices(my.fi,format_to_save,(int)write_counter);
            if (msg != NULL) {
              XMdcDisplayErr("File Split - %s",msg);
              return(MDC_NO);
            }
            break;
        case MDC_SPLIT_PER_FRAME:
            msg = MdcSplitFrames(my.fi,format_to_save,(int)write_counter);
            if (msg != NULL) {
              XMdcDisplayErr("File Split - %s",msg);
              return(MDC_NO);
            }
            break;
        default:
            if (MdcWriteFile(my.fi,format_to_save,(int)write_counter,NULL)
                != MDC_OK) {
              XMdcDisplayErr("Failure writing file");
              write_counter-=1;
              return(MDC_NO);
            }
  }

  XMdcDisplayMesg("File successfully written");

  return(MDC_YES);

}
