/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xfiles.c                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : file routines                                            *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcDisplayFile()        - Read file and display images  *
 *                XMdcRereadFile()         - Reread the file               *
 *                XMdcCloseFile()          - Close file (free memory)      *
 *                XMdcNoFileOpened()       - Give message if no file open  *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcDisplayFile(char *fname)
{
  XMdcViewerHide();
  XMdcMainWidgetsInsensitive();

  XMdcFileReset();
  if (XMdcReadFile(fname) == MDC_OK) {
    XMDC_FILE_OPEN = MDC_YES;
    XMdcViewerEnableAutoShrink();
    XMdcDisplayImages();
  }
  XMdcProgressBar(MDC_PROGRESS_END,0.,NULL);
  XMdcMainWidgetsResensitive();
}

void XMdcRereadFile(GtkWidget *widget, gpointer data)
{
  MdcMergePath(my.fi->ipath,my.fi->idir,my.fi->ifname);
  strcpy(xmdcstr,my.fi->ipath);
  MdcAddCompressionExt(my.fi->compression, xmdcstr);

  XMdcDisplayFile(xmdcstr);

}

void XMdcCloseFile(GtkWidget *widget, gpointer data)
{
  XMdcViewerHide();
  XMdcFileReset();
}

int XMdcNoFileOpened(void)
{
  if (XMDC_FILE_OPEN == MDC_NO) {
    XMdcDisplayWarn("No file opened");
    return(MDC_YES);
  }

  return(MDC_NO);
}
