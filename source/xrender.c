/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xrender.c                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : rendering type handling                                  *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcApplyNewRendering()         - Apply new rendering    *
 *                XMdcRenderingSelCallbackApply() - Rendering Apply        *
 *                XMdcRenderingSel()              - Rendering selection    *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

static GtkWidget *wrender=NULL;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcApplyNewRendering(void)
{
  gtk_widget_set_sensitive(my.viewwindow,FALSE);

  XMdcRemovePreviousColorMap();
  XMdcRemovePreviousImages();

  XMdcBuildColorMap();
  XMdcBuildCurrentImages();

  gtk_widget_set_sensitive(my.viewwindow,TRUE);
}

void XMdcRenderingSelCallbackApply(GtkWidget *widget, gpointer data)
{
  GdkInterpType interp = sRenderSelection.Interp;

  MdcDebugPrint("interpolation type: ");

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sRenderSelection.InterpNearest)))        {
    interp = GDK_INTERP_NEAREST;  MdcDebugPrint("\tnearest");
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sRenderSelection.InterpTiles)))    {
    interp = GDK_INTERP_TILES;    MdcDebugPrint("\ttiles");
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sRenderSelection.InterpBilinear))) {
    interp = GDK_INTERP_BILINEAR; MdcDebugPrint("\tbilinear");
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sRenderSelection.InterpHyper)))    {
    interp = GDK_INTERP_HYPER;    MdcDebugPrint("\thyper");
  }

  if (sRenderSelection.Interp != interp) {
    sRenderSelection.Interp = interp;
    if (XMDC_FILE_OPEN == MDC_YES) XMdcApplyNewRendering();
  }

}


void XMdcRenderingSel(void)
{
  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *separator;
  GSList *group;

  if (wrender == NULL) {
    wrender = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    g_signal_connect(wrender, "destroy",
                       G_CALLBACK(XMdcMedconQuit),NULL);
    g_signal_connect(wrender, "delete_event",
                       G_CALLBACK(XMdcHandlerToHide),NULL);

    gtk_window_set_title(GTK_WINDOW(wrender),"Render Selection");

    gtk_container_set_border_width (GTK_CONTAINER (wrender), 0);

    box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add (GTK_CONTAINER (wrender), box1);
    gtk_widget_show(box1);

    box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER(box2), 5);
    gtk_widget_show(box2);

    box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_pack_start(GTK_BOX(box2), box3, TRUE, TRUE, 0);
    gtk_widget_show(box3);

    /* create frame Interpolation Type */
    frame = gtk_frame_new("Interpolation Type");
    gtk_box_pack_start(GTK_BOX(box3), frame, TRUE, TRUE, 0);
    gtk_widget_show(frame);

    box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(frame), box4);
    gtk_container_set_border_width(GTK_CONTAINER(box4), 10);
    gtk_widget_show(box4);

    button =
    gtk_radio_button_new_with_label(NULL,"Nearest neighbour sampling");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sRenderSelection.Interp == GDK_INTERP_NEAREST)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sRenderSelection.InterpNearest = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button =
    gtk_radio_button_new_with_label(group,"Tiles as mix nearest and bilinear");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sRenderSelection.Interp == GDK_INTERP_TILES)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sRenderSelection.InterpTiles = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button =
    gtk_radio_button_new_with_label(group,"Bilinear interpolation");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sRenderSelection.Interp == GDK_INTERP_BILINEAR)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sRenderSelection.InterpBilinear = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button =
    gtk_radio_button_new_with_label(group,"Hyperbolic-filter interpolation");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sRenderSelection.Interp == GDK_INTERP_HYPER)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sRenderSelection.InterpHyper = button;

    /* create horizontal separator */
    separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start (GTK_BOX (box1), separator, FALSE, FALSE, 0);
    gtk_widget_show (separator);

    /* create bottom button box */
    box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(box1), box2, TRUE, TRUE, 2);
    gtk_widget_show(box2);

    button = gtk_button_new_with_label("Apply");
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
    g_signal_connect_swapped(button, "clicked",
                       G_CALLBACK(gtk_widget_hide), wrender);
    g_signal_connect(button, "clicked",
                       G_CALLBACK(XMdcRenderingSelCallbackApply), NULL);
    gtk_widget_show(button);

    button = gtk_button_new_with_label ("Cancel");
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
    g_signal_connect_swapped(button, "clicked",
        G_CALLBACK(gtk_widget_hide), wrender);
    gtk_widget_show(button);

  }else{
    /* set buttons to appropriate state */
    GtkWidget *b1, *b2, *b3, *b4;

    gtk_widget_hide(wrender);

    b1 = sRenderSelection.InterpNearest;
    b2 = sRenderSelection.InterpTiles;
    b3 = sRenderSelection.InterpBilinear;
    b4 = sRenderSelection.InterpHyper;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4),FALSE);
    switch (sRenderSelection.Interp) {
      case GDK_INTERP_NEAREST :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1),TRUE); break;
      case GDK_INTERP_TILES   :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2),TRUE); break;
      case GDK_INTERP_BILINEAR:
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3),TRUE); break;
      case GDK_INTERP_HYPER   :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4),TRUE); break;
    }


  }

  XMdcShowWidget(wrender);

}

