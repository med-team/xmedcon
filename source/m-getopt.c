/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-getopt.c                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : routines for handling the command-line options           *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : MdcPrintGlobalOptions() - Print only global option usage *
 *                MdcPrintLocalOptions()  - Print only local  option usage *
 *                MdcPrintShortInfo()     - Print info to get more help    *
 *                MdcPrintUsage()         - Print usage of medcon options  *
 *                MdcHandleArgs()         - Handle the arguments           *
 *                MdcApplyReadOptions()   - Apply read options             *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#include <ctype.h>
#ifdef LIBMDC_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "medcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

/* options useful for derived programs */
void MdcPrintGlobalOptions(void)
{

  if (XMDC_GUI == MDC_NO) {

  MdcPrntScrn("\
\n  -c, --convert            give list of conversion <format> strings:\n"
);

  MdcPrntScrn("\n");
  MdcPrntScrn("\t\tascii = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_ASCII],
                                        FrmtExt[MDC_FRMT_ASCII]);
  MdcPrntScrn("\t\tbin   = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_RAW],
                                        FrmtExt[MDC_FRMT_RAW]);
#if MDC_INCLUDE_ACR
  MdcPrntScrn("\t\tacr   = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_ACR],
                                        FrmtExt[MDC_FRMT_ACR]);
#endif
#if MDC_INCLUDE_ANLZ
  MdcPrntScrn("\t\tanlz  = %s (.%s)+(.img)\n",
                                        FrmtString[MDC_FRMT_ANLZ],
                                        FrmtExt[MDC_FRMT_ANLZ]);
#endif
#if MDC_INCLUDE_CONC
  MdcPrntScrn("\t\tconc  = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_CONC],
                                        FrmtExt[MDC_FRMT_CONC]);
#endif
#if MDC_INCLUDE_DICM
  MdcPrntScrn("\t\tdicom = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_DICM],
                                        FrmtExt[MDC_FRMT_DICM]);
#endif
#if MDC_INCLUDE_ECAT
  MdcPrntScrn("\t\tecat6 = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_ECAT6],
                                        FrmtExt[MDC_FRMT_ECAT6]);
  #if MDC_INCLUDE_TPC
  MdcPrntScrn("\t\tecat7 = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_ECAT7],
                                        FrmtExt[MDC_FRMT_ECAT7]);
  #endif
#endif
#if MDC_INCLUDE_GIF
  MdcPrntScrn("\t\tgif   = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_GIF],
                                        FrmtExt[MDC_FRMT_GIF]);
#endif
#if MDC_INCLUDE_INTF
  MdcPrntScrn("\t\tintf  = %s (.%s)+(.i33)\n",
                                        FrmtString[MDC_FRMT_INTF],
                                        FrmtExt[MDC_FRMT_INTF]);
#endif
#if MDC_INCLUDE_INW
  MdcPrntScrn("\t\tinw   = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_INW],
                                        FrmtExt[MDC_FRMT_INW]);
#endif
#if MDC_INCLUDE_NIFTI
  MdcPrntScrn("\t\tnifti = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_NIFTI],
                                        FrmtExt[MDC_FRMT_NIFTI]);
#endif
#if MDC_INCLUDE_PNG
  MdcPrntScrn("\t\tpng   = %s (.%s)\n",
                                        FrmtString[MDC_FRMT_PNG],
                                        FrmtExt[MDC_FRMT_PNG]);
#endif
  }

  MdcPrntScrn("\n\
Pixels: [-n] [-nf] [-qs|-qc|-q] [-b8|-b16[.12]] [-big|little]\n\
        [-si=<slope>:<intercept>] [-cw=<centre>:<width>]\n\
\n  -n, --negatives           enable negative pixels\
\n  -nf, --norm-over-frames   normalize values over each frames\
\n  -q, --quantitation        quantitation using all factors (-qc)\
\n  -qs, --quantification     quantification (use one scale factor )\
\n  -qc, --calibration        calibration    (use two scale factors)");
  MdcPrntScrn("\
\n  -b8, --unsigned-char      write unsigned char pixels  (8  bits)\
\n  -b16, --signed-short      write signed short integers (16 bits)\
\n  -b16.12                   write unsigned shorts, only 12 bits used\
\n  -big, --big-endian        write files in big    endian\
\n  -little, --little-endian  write files in little endian\
\n  -si                       force slope/intercept rescaling\
\n  -cw                       force specified contrast remapping\
\n");
  MdcPrntScrn("\n\
Fallback Read Format: [-fb-none|-fb-anlz|-fb-conc|-fb-ecat|fb-dicom]\n\
\n  -fb-none, --without-fallback  fallback disabled");
  MdcPrntScrn("\
\n  -fb-anlz, --fallback-analyze  ");
#if MDC_INCLUDE_ANLZ
  MdcPrntScrn("fallback on Analyze (SPM)");
#else
  MdcPrntScrn("(*unused*)");
#endif
  MdcPrntScrn("\
\n  -fb-conc, --fallback-concorde ");
#if MDC_INCLUDE_CONC
  MdcPrntScrn("fallback on Concorde uPET");
#else
  MdcPrntScrn("(*unused*)");
#endif
  MdcPrntScrn("\
\n  -fb-ecat, --fallback-ecat     ");
#if MDC_INCLUDE_ECAT
  MdcPrntScrn("fallback on ECAT 6.4");
#else
  MdcPrntScrn("(*unused*)");
#endif
  MdcPrntScrn("\
\n  -fb-dicom, --fallback-dicom   ");
#if MDC_INCLUDE_DICM
  MdcPrntScrn("fallback on DICOM 3.0");
#else
  MdcPrntScrn("(*unused*)");
#endif
  MdcPrntScrn("\n\n\
Slices Transform: [-fh -fv] [-rs -cs -cu] [-sqr | -sqr2] [-crop=<X>:<Y>:<W>:<H>]\n\t\t[-pad | -padtl | -padbr]\
\n  -fh, --flip-horizontal        flip images horizontally (along x-axis)\
\n  -fv, --flip-vertical          flip images vertically   (along y-axis)\
\n  -sqr, --make-square           make square images (largest dimension)\
\n  -sqr2, --make-square-two      make square images (nearest power)");
  MdcPrntScrn("\
\n  -crop, --crop-images          crop image dimensions\
\n  -rs, --reverse-slices         reverse slices sequence\
\n  -cs, --cine-sorting           apply cine sorting\
\n  -cu, --cine-undo              undo  cine sorting\
\n  -pad, --pad-around            padding symmetrical around image\
\n  -padtl, --pad-top-left        padding before first row and column\
\n  -padbr, --pad-bottom-right    padding after last row and column (default)\
\n");
  MdcPrntScrn("\n\
Color Remap: [-24 | -8 [-g -dith -mh|-mr|-mi|-mc|-lut <file>]]\n\
\n  -24, --true-color             color mode of 24 bits RGB triplets\
\n  -8, --indexed-color           color mode of  8 bits indexed colormap\
\n  -dith, --dither-color         apply dithering on color reduction\n\
\n  -g, --make-gray               remap images to grayscale");
  MdcPrntScrn("\
\n  -mh, --map-hotmetal           load colormap hotmetal\
\n  -mr, --map-rainbow            load colormap rainbow\
\n  -mi, --map-inverted           load colormap gray inverted\
\n  -mc, --map-combined           load colormap combined (gray/rainbow)\
\n  -lut, --load-lut              load specified LUT colormap\
\n");
  MdcPrntScrn("\n\
Extras: [-alias -noprefix -preacq -preser -uin]\
\n        [[-splits | -splitf] | [-stacks | -stackf]]\n\
\n  -alias, --alias-naming        output name based  on patient/study id's\
\n  -noprefix, --without-prefix   output name without default prefix\
\n  -preacq, --prefix-acquisition use acquisition number as filename prefix\
\n  -preser, --prefix-series      use series      number as filename prefix\
\n  -uin, --use-institution-name  override default name of institution");
  MdcPrntScrn("\n\
\n  -split3d, --split-slices      split single image slices in separate files\
\n  -split4d, --split-frames      split volume time frames  in separate files\
\n  -stack3d, --stack-slices      stack single image slices into one 3D file\
\n  -stack4d, --stack-frames      stack volume time  frames into one 4D file\
\n");
#if MDC_INCLUDE_ECAT
  MdcPrntScrn("\n\
Format Ecat/Matrix: [-byframe]\n\
\n  -byframe, --sort-by-frame     sort ECAT images by frame (not anatomical)\
\n");
  /*MdcWaitForEnter(0);*/
#endif

#if MDC_INCLUDE_ANLZ
  MdcPrntScrn("\n\
Format Analyze: [-spm -optspm]\n\
\n  -spm, --analyze-spm           use analyze format for SPM software\
\n");
  if (XMDC_GUI == MDC_NO) {
  MdcPrntScrn("\
\n  -optspm, --options-spm        ask for SPM related options\
\n");
  }
#endif
#if (MDC_INCLUDE_DICM || MDC_INCLUDE_ACR)
  MdcPrntScrn("\n\
Format DICOM:\n\
\n a) general: [-cw=<centre>:<width>]\n\
             [-contrast] [-gap] [-implicit -nometa | -weu]\n\
\n  -contrast, --enable-contrast  enable support for contrast window changes\
\n  -gap, --spacing-true-gap      slice spacing is true gap or overlap");
  MdcPrntScrn("\
\n  -implicit, --write-implicit   output file as implicit little endian\
\n  -nometa, --write-without-meta output file without (part 10) meta header\
\n  -weu, --write-encap-uncomp    output file encapsulated uncompressed\
\n  -cw                           force contrast window centre/width");
  MdcPrntScrn("\n\
\n b) mosaic: [-mosaic | -fmosaic=<W>x<H>x<N> [-interl] [-mfixv]]\n\
\n  -mosaic, --enable-mosaic      enable mosaic by \"detected\" stamps layout\
\n  -fmosaic, --force-mosaic      force  mosaic by predefined stamps layout\
\n  -mfixv, --mosaic-fix-voxel    rescale voxel sizes by mosaic factor\
\n  -interl, --mosaic-interlaced  consider mosaic stamp slices as interlaced\
\n");
#endif
#if MDC_INCLUDE_GIF
  MdcPrntScrn("\n\
\nFormat Gif89a: [-optgif]\n");
  if (XMDC_GUI == MDC_NO) {
  MdcPrntScrn("\
\n  -optgif, --options-gif        ask for GIF related options\
\n");
  }else{
  MdcPrntScrn("\
\n <none>\
\n");
  }
#endif
#if MDC_INCLUDE_INTF
  MdcPrntScrn("\
\nFormat InterFile: [-skip1 -nopath -one]\n\
\n  -skip1, --skip-preview-slice  skip first preview slice\
\n  -nopath, --ignore-path        ignore path in \'name of data file\' key\
\n  -one, --single-file           write header and image to same file\
\n");
#endif

  if (XMDC_GUI == MDC_YES) MdcPrntScrn("\n");

}

/* options for MedCon in particular */
void MdcPrintLocalOptions(void)
{
  MdcPrntScrn("\n\n\
Patient/Slice/Study: [-anon|-ident] [-vifi]\n\
\n  -anon, --anonymous            make patient/study anonymous\
\n  -ident, --identify            ask for patient/study information\
\n  -vifi, --edit-fileinfo        edit internal entries (images/slice/origent)\
\n");
  MdcPrntScrn("\n\
Reslicing: [-tra|-sag|-cor]\n\
\n  -tra, --tranverse             reslice images transverse\
\n  -sag, --sagittal              reslice images sagittal\
\n  -cor, --coronal               reslice images coronal\
\n");
  MdcPrntScrn("\n\
Debug/Mode: [-d -v -db -hackacr -ean]\n\
\n  -d, --debug                   give debug information (printout FI)\
\n  -s, --silent                  force silent mode, suppress all messages\
\n  -v, --verbose                 run in verbose mode\
\n  -db, --database               print database info\
\n  -ean, --echo-alias-name       echo alias name on screen\
\n");
#if MDC_INCLUDE_ACR
  MdcPrntScrn("\
\n  -hackacr, --hack-acrtags      try to locate and interpret acr tags in file\
\n");
#endif
  MdcPrntScrn("\n");
}

void MdcPrintShortInfo(void)
{
  if (XMDC_GUI == MDC_YES) {
    MdcPrntScrn("\nGUI X Window System");
  }else{
    MdcPrntScrn("\nCLI");
  }
    MdcPrntScrn(" Medical Image Conversion Utility\n");
    MdcPrntScrn("(X)MedCon %s\n",MdcGetLibShortVersion());
    MdcPrntScrn("Copyright (C) 1997-2025 by Erik Nolf\n\n");

  if (XMDC_GUI == MDC_YES) {
    MdcPrntScrn("Try \'xmedcon --help\' for more information.\n\n");
  }else{
    MdcPrntScrn("Try \'medcon --help\' for more information.\n\n");
#ifdef _WIN32
    fflush(NULL); MdcWaitForEnter(-1);
#endif
  }
}

void MdcPrintUsage(char *pgrname)
{

  /* allow usage info to stdout */
  /* |more only catches stdout  */
  MDC_FILE_STDOUT = MDC_NO;

  if (pgrname == NULL) {

  /* usage for MedCon in particular */
  MdcPrintShortInfo();
  MdcPrntScrn("\nUsage:\n\n");
  MdcPrntScrn("  medcon [options] -f <files> ...\n");
  MdcPrntScrn("\n");
  MdcPrntScrn("Flags:\n\n");
  MdcPrntScrn("  -f, --file, --files  file or list of files to handle\n");
  MdcPrntScrn("\n");
  MdcPrntScrn("General: [-i -e -r -w] [-p -pa|-c <format> ...] ");
  MdcPrntScrn("[-o <basename>]\n");
  MdcPrntScrn("\n");
  MdcPrntScrn("  -e, --extract            extract images from file\n");
  MdcPrntScrn("  -i, --interactive        read raw files after user input\n");
  MdcPrntScrn("  -o, --output-name        output name set from command-line\n");
  MdcPrntScrn("  -p, --print-values       print values of specified pixels\n");
  MdcPrntScrn("  -pa, --print-all-values  print all values without asking\n");
  MdcPrntScrn("  -r, --rename-file        rename file after user input\n");
  MdcPrntScrn("  -w, --overwrite-files    always overwrite files\n");
  MdcPrintGlobalOptions();
  MdcPrintLocalOptions();

  }else{

  /* usage for any derived program */
  MdcPrntScrn("\nUsage:\n\n");
  MdcPrntScrn("  %s [options] -f <file> ...\n",pgrname);
  if (XMDC_GUI == MDC_NO) {
  MdcPrntScrn("\n");
  MdcPrntScrn("Options: General: [-c <format> ...] [-o <basename>]\n");
  }

  MdcPrintGlobalOptions();

  }

  exit(0);

}

int MdcHandleArgs(FILEINFO *fi, int argc, char *argv[], int MAXFILES)
{
  int a,ARG=-1, DO_STDIN=MDC_NO;
  char **files = mdc_arg_files;
  int   *convs = mdc_arg_convs;
  int   *total = mdc_arg_total;
  char  *pset = NULL;

  MdcExtractInputStruct *input = &mdcextractinput;

  /* limit number of files */
  if (MAXFILES > MDC_MAX_FILES || MAXFILES <= 0) MAXFILES = MDC_MAX_FILES;

  fi->map = MDC_MAP_GRAY;      /* default color map */

  if ( argc == 1 ) return(MDC_BAD_CODE);


  /* initialize some stuff */
  files[0]=NULL;
  memset(convs,0,sizeof(int)*MDC_MAX_FRMTS);
  memset(total,0,sizeof(int)*2);
  mdcbufr[0]='\0';

  for (a=1; a<argc; a++) {

     if ( (strcasecmp(argv[a],"-h") == 0) ||
          (strcasecmp(argv[a],"--help") == 0) ) return(MDC_BAD_CODE);

     if ( (strcasecmp(argv[a],"-c") == 0) ||
          (strcasecmp(argv[a],"--convert") == 0) ) { /* begin list of convs */
       if (XMDC_GUI == MDC_NO) {
         MDC_INFO=MDC_NO; MDC_CONVERT=MDC_YES; ARG=MDC_ARG_CONV;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-i") == 0) ||
                (strcasecmp(argv[a],"--interactive") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_INTERACTIVE=MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-e") == 0) ||
                (strcasecmp(argv[a],"--extract") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_EXTRACT=MDC_YES; ARG = MDC_ARG_EXTRACT;
         input->INTERACTIVE = MDC_YES; input->list[0]='\0';
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-db") == 0) ||
                (strcasecmp(argv[a],"--database") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_INFO_DB = MDC_YES; MDC_INFO = MDC_NO;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-alias") == 0) ||
                (strcasecmp(argv[a],"--alias-naming") == 0) ) {
       MDC_ALIAS_NAME = MDC_YES;
       continue;
     }else if ( (strcasecmp(argv[a],"-ean") == 0) ||
                (strcasecmp(argv[a],"--echo-alias-name") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_ECHO_ALIAS = MDC_YES;
         MDC_INFO = MDC_NO;
         if (MDC_BLOCK_MESSAGES == MDC_NO) MDC_BLOCK_MESSAGES = MDC_LEVEL_WARN;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-noprefix") == 0) ||
                (strcasecmp(argv[a],"--without-prefix") == 0) ) {
       MDC_PREFIX_DISABLED = MDC_YES;
       continue;
     }else if ( (strcasecmp(argv[a],"-preacq") == 0) ||
                (strcasecmp(argv[a],"--prefix-acquisition") == 0) ) {
       MDC_PREFIX_ACQ = MDC_YES;
       continue;
     }else if ( (strcasecmp(argv[a],"-preser") == 0) ||
                (strcasecmp(argv[a],"--prefix-series") == 0) ) {
       MDC_PREFIX_SER = MDC_YES;
       continue;
     }else if ( (strcasecmp(argv[a],"-uin") == 0) ||
                (strcasecmp(argv[a],"--use-institution-name") == 0) ) {
       a+=1;
       if (a < argc) MdcStringCopy(MDC_INSTITUTION,argv[a],strlen(argv[a]));
       continue;
     }else if ( (strcasecmp(argv[a],"-splits") == 0) ||
                (strcasecmp(argv[a],"-split3d") == 0) ||
                (strcasecmp(argv[a],"-split3D") == 0) ||
                (strcasecmp(argv[a],"--split-slices") == 0) ) {
       MDC_FILE_SPLIT = MDC_SPLIT_PER_SLICE;
       continue;
     }else if ( (strcasecmp(argv[a],"-splitf") == 0) ||
                (strcasecmp(argv[a],"-split4d") == 0) ||
                (strcasecmp(argv[a],"-split4D") == 0) ||
                (strcasecmp(argv[a],"--split-frames") == 0) ) {
       MDC_FILE_SPLIT = MDC_SPLIT_PER_FRAME;
       continue;
     }else if ( (strcasecmp(argv[a],"-stacks") == 0)  ||
                (strcasecmp(argv[a],"-stack3d") == 0) ||
                (strcasecmp(argv[a],"-stack3D") == 0) ||
                (strcasecmp(argv[a],"--stack-slices") == 0) ) {
       MDC_FILE_STACK = MDC_STACK_SLICES;
       continue;
     }else if ( (strcasecmp(argv[a],"-stackf") == 0) ||
                (strcasecmp(argv[a],"-stack4d") == 0) ||
                (strcasecmp(argv[a],"-stack4D") == 0) ||
                (strcasecmp(argv[a],"--stack-frames") == 0) ) {
       MDC_FILE_STACK = MDC_STACK_FRAMES;
       continue;
     }else if ( (strcasecmp(argv[a],"-anon") == 0) ||
                (strcasecmp(argv[a],"--anonymous") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_PATIENT_ANON = MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-ident") == 0) ||
                (strcasecmp(argv[a],"--identify") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_PATIENT_IDENT = MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-o") == 0) ||
                (strcasecmp(argv[a],"--output-name") == 0) ) {
       a+=1;
       if ((XMDC_GUI == MDC_NO) && (a < argc)) mdcbasename = argv[a];
       continue;
     }else if ( (strcasecmp(argv[a],"-p") == 0) ||
                (strcasecmp(argv[a],"--print-values") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_PIXELS=MDC_YES;   MDC_INFO=MDC_NO;
         MDC_NEGATIVE=MDC_YES; MDC_CALIBRATE=MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-pa") == 0) ||
                (strcasecmp(argv[a],"--print-all-values") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_PIXELS=MDC_YES;   MDC_INFO=MDC_NO;
         MDC_NEGATIVE=MDC_YES; MDC_CALIBRATE=MDC_YES;
         MDC_PIXELS_PRINT_ALL=MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-r") == 0) ||
                (strcasecmp(argv[a],"--rename-file") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_RENAME = MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-w") == 0) ||
                (strcasecmp(argv[a],"--overwrite-files") == 0) ) {
       if (XMDC_GUI == MDC_NO) {
         MDC_FILE_OVERWRITE = MDC_YES;
       }
       continue;
     }else if ( (strcasecmp(argv[a],"-f") == 0) ||
                (strcasecmp(argv[a],"--file") == 0) ||
                (strcasecmp(argv[a],"--files") == 0)) {/* begin list of files */
       ARG=MDC_ARG_FILE; continue;
     }else if ( (strcasecmp(argv[a],"-n") == 0) ||
                (strcasecmp(argv[a],"--negatives") == 0) ) {
       MDC_NEGATIVE=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-q") == 0) ||
                (strcasecmp(argv[a],"--quantitation") == 0)) {
       MDC_CALIBRATE=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-qs") == 0) ||
                (strcasecmp(argv[a],"--quantification") == 0) ) {
       MDC_QUANTIFY=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-qc") == 0) ||
                (strcasecmp(argv[a],"--calibration") == 0) ) {
       MDC_CALIBRATE=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-24") == 0) ||
                (strcasecmp(argv[a],"--true-color") == 0) ) {
       MDC_COLOR_MODE = MDC_COLOR_RGB; continue;
     }else if ( (strcasecmp(argv[a],"-8") == 0) ||
                (strcasecmp(argv[a],"--indexed-color") == 0) ) {
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-mr") == 0) ||
                (strcasecmp(argv[a],"--map-rainbow") == 0) ) {
       MDC_COLOR_MAP = MDC_MAP_RAINBOW;  MDC_MAKE_GRAY = MDC_YES;
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-mc") == 0) ||
                (strcasecmp(argv[a],"--map-combined") == 0) ) {
       MDC_COLOR_MAP = MDC_MAP_COMBINED; MDC_MAKE_GRAY = MDC_YES;
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-mh") == 0) ||
                (strcasecmp(argv[a],"--map-hotmetal") == 0) ) {
       MDC_COLOR_MAP = MDC_MAP_HOTMETAL; MDC_MAKE_GRAY = MDC_YES;
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-mi") == 0) ||
                (strcasecmp(argv[a],"--map-inverted") == 0) ) {
       MDC_COLOR_MAP = MDC_MAP_INVERTED;     MDC_MAKE_GRAY = MDC_YES;
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-g") == 0) ||
                (strcasecmp(argv[a],"--make-gray") == 0) ) {
       MDC_COLOR_MAP = MDC_MAP_GRAY;     MDC_MAKE_GRAY = MDC_YES;
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-dith") == 0) ||
                (strcasecmp(argv[a],"--dither-color") == 0) ) {
       MDC_DITHER_COLOR = MDC_YES;
       MDC_COLOR_MODE = MDC_COLOR_INDEXED; continue;
     }else if ( (strcasecmp(argv[a],"-lut") == 0) ||
                (strcasecmp(argv[a],"--load-lut") == 0) ) {
       a+=1;
       if ((a < argc) && (MdcLoadLUT(argv[a]) == MDC_YES)) {
         MDC_COLOR_MAP = MDC_MAP_LOADED; MDC_MAKE_GRAY = MDC_YES;
         MDC_COLOR_MODE = MDC_COLOR_INDEXED;
       }
       continue;
#if MDC_INCLUDE_GIF
     }else if ( (strcasecmp(argv[a],"-optgif") == 0) ||
                (strcasecmp(argv[a],"--options-gif") == 0) ) {
       MDC_GIF_OPTIONS = MDC_YES; continue;
#endif
#if MDC_INCLUDE_ACR
     }else if ( (strcasecmp(argv[a],"-hackacr") == 0) ||
                (strcasecmp(argv[a],"--hack-acrtags") == 0) ) {
       MDC_HACK_ACR = MDC_YES; continue;
#endif
#if MDC_INCLUDE_ANLZ
     }else if ( (strcasecmp(argv[a],"-spm") == 0) ||
                (strcasecmp(argv[a],"--analyze-spm") == 0) ) {
       MDC_ANLZ_SPM = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-optspm") == 0) ||
                (strcasecmp(argv[a],"--options-spm") == 0) ) {
       MDC_ANLZ_OPTIONS = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-fb-anlz") == 0) ||
                (strcasecmp(argv[a],"--fallback-analyze") == 0) ) {
       MDC_FALLBACK_FRMT = MDC_FRMT_ANLZ; continue;
#endif
#if MDC_INCLUDE_CONC
     }else if ( (strcasecmp(argv[a],"-fb-conc") == 0) ||
                (strcasecmp(argv[a],"--fallback-concorde") == 0) ) {
       MDC_FALLBACK_FRMT = MDC_FRMT_CONC; continue;
#endif
#if MDC_INCLUDE_ECAT
     }else if ( (strcasecmp(argv[a],"-fb-ecat") == 0) ||
                (strcasecmp(argv[a],"--fallback-ecat") == 0) ) {
       MDC_FALLBACK_FRMT = MDC_FRMT_ECAT6; continue;
     }else if ( (strcasecmp(argv[a],"-byframe") == 0) ||
                (strcasecmp(argv[a],"--sort-by-frame") == 0) ) {
       MDC_ECAT6_SORT = MDC_BYFRAME; continue;
#endif
#if MDC_INCLUDE_DICM
     }else if ( (strcasecmp(argv[a],"-fb-dicom") == 0) ||
                (strcasecmp(argv[a],"--fallback-dicom") == 0) ) {
       MDC_FALLBACK_FRMT = MDC_FRMT_DICM; continue;
     }else if ( strstr(argv[a],"-si=") != NULL ) {
       MDC_FORCE_RESCALE = MDC_YES; MDC_CALIBRATE = MDC_YES;
       sscanf(argv[a],"-si=%f:%f",&mdc_si_slope,&mdc_si_intercept);
       continue;
     }else if ( strstr(argv[a],"-cw=") != NULL ) {
       MDC_FORCE_CONTRAST = MDC_YES; MDC_CONTRAST_REMAP = MDC_YES;
       sscanf(argv[a],"-cw=%f:%f",&mdc_cw_centre,&mdc_cw_width);
       continue;
     }else if ( (strcasecmp(argv[a],"-mosaic") == 0) ||
                (strcasecmp(argv[a],"--enable-mosaic") == 0) ) {
       MDC_DICOM_MOSAIC_ENABLED = MDC_YES;
       continue;
     }else if ((pset=strstr(argv[a],"-fmosaic=")) != NULL) {
       MDC_DICOM_MOSAIC_ENABLED = MDC_YES; MDC_DICOM_MOSAIC_FORCED = MDC_YES;
       MdcLowStr(pset);
       sscanf(pset,"-fmosaic=%ux%ux%u",&mdc_mosaic_width
                                      ,&mdc_mosaic_height
                                      ,&mdc_mosaic_number);
       continue;
     }else if ((pset=strstr(argv[a],"--force-mosaic=")) != NULL) {
       MDC_DICOM_MOSAIC_ENABLED = MDC_YES; MDC_DICOM_MOSAIC_FORCED = MDC_YES;
       MdcLowStr(pset);
       sscanf(pset,"--force-mosaic=%ux%ux%u",&mdc_mosaic_width
                                            ,&mdc_mosaic_height
                                            ,&mdc_mosaic_number);
       continue;
     }else if ( (strcasecmp(argv[a],"-mfixv") == 0) ||
                (strcasecmp(argv[a],"--mosaic-fix-voxel") == 0) ) {
       MDC_DICOM_MOSAIC_FIX_VOXEL = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-interl") == 0) ||
                (strcasecmp(argv[a],"--mosaic-interlaced") == 0) ) {
       MDC_DICOM_MOSAIC_DO_INTERL = MDC_YES; mdc_mosaic_interlaced = MDC_YES;
       continue;
     }else if ( (strcasecmp(argv[a],"-gap") == 0) ||
                (strcasecmp(argv[a],"--spacing-true-gap") == 0) ) {
       MDC_TRUE_GAP = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-contrast") == 0) ||
                (strcasecmp(argv[a],"--enable-contrast") == 0) ) {
       MDC_CONTRAST_REMAP = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-implicit") == 0) ||
                (strcasecmp(argv[a],"--write-implicit") == 0) ) {
       MDC_DICOM_WRITE_IMPLICIT = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-nometa") == 0) ||
                (strcasecmp(argv[a],"--write-without-meta") == 0) ) {
       MDC_DICOM_WRITE_NOMETA = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-weu") == 0) ||
                (strcasecmp(argv[a],"--write-encap-uncomp") == 0) ) {
       MDC_DICOM_WRITE_ENCAP_UNCOMP = MDC_YES; continue;
#endif
     }else if ( (strcasecmp(argv[a],"-fb-none") == 0) ||
                (strcasecmp(argv[a],"--without-fallback") == 0) ) {
       MDC_FALLBACK_FRMT = MDC_FRMT_NONE; continue;
     }else if ( (strcasecmp(argv[a],"-d") == 0) ||
                (strcasecmp(argv[a],"--debug") == 0) ) {
       MDC_DEBUG = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-nf") == 0) ||
                (strcasecmp(argv[a],"--norm-over-frames") == 0) ) {
       MDC_NORM_OVER_FRAMES = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-v") == 0) ||
                (strcasecmp(argv[a],"--verbose") == 0) ) {
       MDC_VERBOSE  = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-b8") == 0) ||
                (strcasecmp(argv[a],"--unsigned-char") == 0) ) {
       MDC_FORCE_INT = BIT8_U;  continue;
     }else if ( (strcasecmp(argv[a],"-b16") == 0) ||
                (strcasecmp(argv[a],"--signed-short") == 0) ) {
       MDC_FORCE_INT = BIT16_S; MDC_INT16_BITS_USED=16; continue;
     }else if ( strcasecmp(argv[a],"-b16.12") == 0 ) {
       MDC_FORCE_INT = BIT16_S; MDC_INT16_BITS_USED=12; continue;
     }else if ( strcasecmp(argv[a],"-debuging") == 0 ) { /* undocumented */
       MDC_MY_DEBUG = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-vifi") == 0) ||
                (strcasecmp(argv[a],"--edit-fileinfo") == 0) ) {
       MDC_EDIT_FI = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-big") == 0) ||
                (strcasecmp(argv[a],"--big-endian") == 0) ) {
       MDC_WRITE_ENDIAN=MDC_BIG_ENDIAN; continue;
     }else if ( (strcasecmp(argv[a],"-little") == 0) ||
                (strcasecmp(argv[a],"--little-endian") == 0) ) {
       MDC_WRITE_ENDIAN=MDC_LITTLE_ENDIAN; continue;
     }else if ( (strcasecmp(argv[a],"-skip1") == 0) ||
                (strcasecmp(argv[a],"--skip-preview-slice") == 0) ) {
       MDC_SKIP_PREVIEW=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-nopath") == 0) ||
                (strcasecmp(argv[a],"--ignore-path") == 0) ) {
       MDC_IGNORE_PATH=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-one") == 0) ||
                (strcasecmp(argv[a],"--single-file") == 0) ) {
       MDC_SINGLE_FILE=MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-tra") == 0) ||
                (strcasecmp(argv[a],"--transverse") == 0) ) {
       MDC_RESLICE = MDC_TRANSAXIAL;  continue;
     }else if ( (strcasecmp(argv[a],"-sag") == 0) ||
                (strcasecmp(argv[a],"--sagittal") == 0) ) {
       MDC_RESLICE = MDC_SAGITTAL;    continue;
     }else if ( (strcasecmp(argv[a],"-cor") == 0) ||
                (strcasecmp(argv[a],"--coronal") == 0) ) {
       MDC_RESLICE = MDC_CORONAL;     continue;
     }else if ( (strcasecmp(argv[a],"-fh") == 0) ||
                (strcasecmp(argv[a],"--flip-horizontal") == 0) ) {
       MDC_FLIP_HORIZONTAL = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-fv") == 0) ||
                (strcasecmp(argv[a],"--flip-vertical") == 0) ) {
       MDC_FLIP_VERTICAL   = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-rs") == 0) ||
                (strcasecmp(argv[a],"--reverse-slices") == 0) ) {
       MDC_SORT_REVERSE    = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-cs") == 0) ||
                (strcasecmp(argv[a],"--cine-sorting") == 0) ) {
       MDC_SORT_CINE_APPLY = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-cu") == 0) ||
                (strcasecmp(argv[a],"--cine-undo") == 0) ) {
       MDC_SORT_CINE_UNDO   = MDC_YES; continue;
     }else if ( (strcasecmp(argv[a],"-sqr") == 0) ||
                (strcasecmp(argv[a],"--make-square") == 0) ) {
       MDC_MAKE_SQUARE = MDC_TRANSF_SQR1; continue;
     }else if ( (strcasecmp(argv[a],"-sqr2") == 0) ||
                (strcasecmp(argv[a],"--make-square-two") == 0) ) {
       MDC_MAKE_SQUARE = MDC_TRANSF_SQR2; continue;
     }else if ( (strcasecmp(argv[a],"-pad") == 0) ||
                (strcasecmp(argv[a],"--pad-around") == 0) ) {
       MDC_PADDING_MODE = MDC_PAD_AROUND; continue;
     }else if ( (strcasecmp(argv[a],"-padtl") == 0) ||
                (strcasecmp(argv[a],"--pad-top-left") == 0) ) {
       MDC_PADDING_MODE = MDC_PAD_TOP_LEFT; continue;
     }else if ( (strcasecmp(argv[a],"-padbr") == 0) ||
                (strcasecmp(argv[a],"--pad-bottom-right") == 0) ) {
       MDC_PADDING_MODE = MDC_PAD_BOTTOM_RIGHT; continue;
     }else if ((pset=strstr(argv[a],"-crop=")) != NULL) {
       MDC_CROP_IMAGES = MDC_YES;
       MdcLowStr(pset);
       sscanf(pset,"-crop=%u:%u:%u:%u",&mdc_crop_xoffset
                                      ,&mdc_crop_yoffset
                                      ,&mdc_crop_width
                                      ,&mdc_crop_height);
       continue;
     }else if ((pset=strstr(argv[a],"--crop-images=")) != NULL) {
       MDC_CROP_IMAGES = MDC_YES;
       MdcLowStr(pset);
       sscanf(pset,"--crop-images=%u:%u:%u:%u",&mdc_crop_xoffset
                                              ,&mdc_crop_yoffset
                                              ,&mdc_crop_width
                                              ,&mdc_crop_height);
       continue;
     }else if ( (strcasecmp(argv[a],"-s") == 0) ||
                (strcasecmp(argv[a],"--silent") == 0) ) {
       MDC_BLOCK_MESSAGES = MDC_LEVEL_ALL; continue;
     }

     if ( ARG == -1 ) return(MDC_BAD_CODE);              /* no '-c' or '-f' */

     switch (ARG) {

       case MDC_ARG_CONV:
         if ( strcasecmp(argv[a],"ascii")== 0) {
             convs[MDC_FRMT_ASCII]+=1; total[MDC_CONVS]+=1;
         }else
         if ( strcasecmp(argv[a],"bin")  == 0) {
             convs[MDC_FRMT_RAW]+=1; total[MDC_CONVS]+=1;
         }else
#if MDC_INCLUDE_ACR
         if ( strcasecmp(argv[a],"acr")  == 0 ) {
             convs[MDC_FRMT_ACR]+=1; total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_GIF
         if ( strcasecmp(argv[a],"gif")  == 0 ) {
             convs[MDC_FRMT_GIF]+=1; total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_INW
         if ( strcasecmp(argv[a],"inw")  == 0 ) {
             convs[MDC_FRMT_INW]+=1; total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_CONC
         if ( strcasecmp(argv[a],"conc") == 0 ) {
             convs[MDC_FRMT_CONC]+=1; total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_ECAT
         if ( strcasecmp(argv[a],"ecat") == 0 ||
              strcasecmp(argv[a],"ecat6") == 0 ) {
             convs[MDC_FRMT_ECAT6]+=1; total[MDC_CONVS]+=1;
         }else
  #if MDC_INCLUDE_TPC
         if ( strcasecmp(argv[a],"ecat7") == 0 ) {
             convs[MDC_FRMT_ECAT7]+=1; total[MDC_CONVS]+=1;
         }else
  #endif
#endif
#if MDC_INCLUDE_INTF
         if ( strcasecmp(argv[a],"intf") == 0 ) {
             convs[MDC_FRMT_INTF]+=1; total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_ANLZ
         if ( strcasecmp(argv[a],"anlz") == 0 ) {
             convs[MDC_FRMT_ANLZ]+=1; total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_DICM
         if ( strcasecmp(argv[a],"dicom") == 0) {
             convs[MDC_FRMT_DICM]+=1; total[MDC_CONVS]+=1;
         }else
#if MDC_INCLUDE_PNG
         if ( strcasecmp(argv[a],"png") == 0) {
             convs[MDC_FRMT_PNG]+=1;  total[MDC_CONVS]+=1;
         }else
#endif
#if MDC_INCLUDE_NIFTI
         if ( strcasecmp(argv[a],"nifti") == 0) {
             convs[MDC_FRMT_NIFTI]+=1;  total[MDC_CONVS]+=1;
         }else
#endif
#endif
         if ( strcasecmp(argv[a],"-") == 0) {
           MDC_FILE_STDOUT = MDC_YES;
         }else

         MdcPrntErr(MDC_BAD_CODE,"Unsupported conversion \"%s\"",argv[a]);

         break;

       case MDC_ARG_FILE:

         if (MDC_FILE_STDIN == MDC_NO) {
           /* input from files */

           if ( total[MDC_FILES] == MAXFILES ) {
             MdcPrntErr(MDC_OVER_FLOW,"Too many files specified (max=%d)"
                                     ,MAXFILES);
           }else{
             files[total[MDC_FILES]]=argv[a]; total[MDC_FILES]+=1;
           }
         }else{
           /* input from stdin */

           /* format specification of stdin file so no MdcCheckFrmt() needed */
           /* otherwise a seek is needed, thus eleminating pipes support     */
#if MDC_INCLUDE_ACR
           if ( strcasecmp(argv[a],"acr")  == 0 ) {
             MdcPrntErr(MDC_NO_CODE,"ACR is unsupported in pipes (seek)");
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_ACR;
           }else
#endif
#if MDC_INCLUDE_GIF
           if ( strcasecmp(argv[a],"gif")  == 0 ) {
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_GIF;
           }else
#endif
#if MDC_INCLUDE_INW
           if ( strcasecmp(argv[a],"inw")  == 0 ) {
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_INW;
           }else
#endif
#if MDC_INCLUDE_CONC
           if ( strcasecmp(argv[a],"conc") == 0) {
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_CONC;
           }else
#endif
#if MDC_INCLUDE_ECAT
           if ( strcasecmp(argv[a],"ecat") == 0 ||
                strcasecmp(argv[a],"ecat6") == 0 ) {
             MdcPrntErr(MDC_NO_CODE,"ECAT6 is unsupported in pipes (seek)");
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_ECAT6;
           }else
  #if MDC_INCLUDE_TPC
           if ( strcasecmp(argv[a],"ecat7") == 0) {
             MdcPrntErr(MDC_NO_CODE,"ECAT7 is unsupported in pipes");
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_ECAT7;
           }else
  #endif
#endif
#if MDC_INCLUDE_INTF
           if ( strcasecmp(argv[a],"intf") == 0) {
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_INTF;
           }else
#endif
#if MDC_INCLUDE_ANLZ
           if ( strcasecmp(argv[a],"anlz") == 0) {
             MdcPrntErr(MDC_NO_CODE,"ANLZ is unsupported in pipes (seek)");
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_ANLZ;
           }else
#endif
#if MDC_INCLUDE_DICM
           if ( strcasecmp(argv[a],"dicom") == 0) {
             MdcPrntErr(MDC_NO_CODE,"DICOM is unsupported in pipes (seek)");
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_DICM;
           }else
#endif
#if MDC_INCLUDE_PNG
           if ( strcasecmp(argv[a],"png") == 0) {
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_PNG;
           }else
#endif
#if MDC_INCLUDE_NIFTI
           if ( strcasecmp(argv[a],"nifti") == 0) {
             DO_STDIN = MDC_YES; MDC_FRMT_INPUT = MDC_FRMT_NIFTI;
           }else
#endif

             { DO_STDIN = MDC_NO;  MDC_FRMT_INPUT = MDC_FRMT_NONE; }

         }

         if (strcmp(argv[a],"-") == 0) DO_STDIN = MDC_YES;


         if (DO_STDIN == MDC_YES) {
             files[0]=argv[a]; total[MDC_FILES] = 1;
             DO_STDIN = MDC_NO; MDC_FILE_STDIN = MDC_YES;
         }

         break;

       case MDC_ARG_EXTRACT:

         if (isdigit((int)argv[a][0])) {
           if (strlen(input->list) + strlen(argv[a]) < MDC_MAX_LIST) {
             strcat(input->list,argv[a]); strcat(input->list," ");
           }else{
             MdcPrntErr(MDC_NO_CODE,"Extraction list too big for buffer");
           }
           input->INTERACTIVE = MDC_NO;
         }else{
           ARG = MDC_ARG_FILE;
         }

         break;
     }

  }

  /* options shown but not available for GUI front-end */
  if (XMDC_GUI == MDC_YES) {
    if (MDC_FILE_STACK != MDC_NO) {
      MdcPrntWarn("Options '-stack'   not available to GUI front-end");
    }
    if ((MDC_GIF_OPTIONS == MDC_YES) || (MDC_ANLZ_OPTIONS == MDC_YES)) {
      MdcPrntWarn("Options '-opt***'  not available to GUI front-end");
    }
  }

  /* split / stack mutually exclusive */
  if ((MDC_FILE_STACK != MDC_NO) && (MDC_FILE_SPLIT != MDC_NO)) {
    MdcPrntErr(MDC_NO_CODE,"Options '-stack' and '-split' mutually exclusive");
  }

  /* stack only in true color */
  if ((MDC_FILE_STACK != MDC_NO) && (MDC_COLOR_MODE == MDC_COLOR_INDEXED)) {
    MdcPrntErr(MDC_NO_CODE,"Stacking only supported for true color or gray");
  }

  /* print database info; legacy option, just used within ECAT */
  if (MDC_INFO_DB == MDC_YES) {
    if (total[MDC_FILES] > 1)
      MdcPrntErr(MDC_NO_CODE,"Option '-db' only useful one file at a time");
    if (MDC_CONVERT == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Option '-db' useless with '-c' conversion");
  }

  /* echo alias name; single option allowed */
  if (MDC_ECHO_ALIAS == MDC_YES) {
    if (total[MDC_FILES] > 1)
      MdcPrntErr(MDC_NO_CODE,"Option '-ean' only useful one file at a time");
    if (MDC_CONVERT == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Option '-ean' useless with '-c' conversion");
  }

  /* useless sorting selections */
  if ((MDC_SORT_CINE_APPLY == MDC_YES) && (MDC_SORT_CINE_UNDO == MDC_YES)) {
    MDC_SORT_CINE_APPLY = MDC_NO; MDC_SORT_CINE_UNDO = MDC_NO;
  }

  /* in case of alias filenaming, disable other options */
  if (MDC_ALIAS_NAME == MDC_YES) {
    MDC_RENAME = MDC_NO; mdcbasename = NULL;
  }

  /* need an image file, except for XMedCon GUI */
  if ( (total[MDC_FILES] == 0) && (XMDC_GUI == MDC_NO) )
    MdcPrntErr(MDC_NO_CODE,"No files specified");

  /* checks related to file writing to stdout */
  if (MDC_FILE_STDOUT == MDC_YES) {
    if (MDC_VERBOSE == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Option '-v' disallowed with stdout file writing");

    if ((total[MDC_FILES] > 1) && (MDC_FILE_STACK == MDC_NO))
      MdcPrntErr(MDC_NO_CODE,"Only one file supported when writing to stdout");

    if (total[MDC_CONVS] > 1)
      MdcPrntErr(MDC_NO_CODE,"Only one format allowed when writing to stdout");

    if (MDC_INTERACTIVE == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Interactive read impossible with stdout");

    if (MDC_SINGLE_FILE == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Single file output unsupported with stdout");

  }
  /* checks related to file input from stdin */
  if (MDC_FILE_STDIN == MDC_YES) {

    if (MDC_HACK_ACR == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Hack ACR from stdin not allowed");

    if (MDC_INTERACTIVE == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Interactive read impossible from stdin");
  }

  /* quantification troubles */
  if (MDC_INFO == MDC_YES) {
    if (MDC_QUANTIFY == MDC_YES) {
      MDC_NEGATIVE=MDC_YES; MDC_CALIBRATE=MDC_NO;
    }else{
      MDC_NEGATIVE=MDC_YES; MDC_CALIBRATE=MDC_YES;
    }
  }

  if ((MDC_ANLZ_SPM == MDC_YES) && (MDC_QUANTIFY == MDC_NO)
                                && (MDC_CALIBRATE == MDC_NO) )
    MdcPrntWarn("For SPM scaling you should select a quantification option");

  if (MDC_INTERACTIVE == MDC_YES) {
    MdcPrntWarn("Enabling negative pixels & disabling quantification");
    MDC_NEGATIVE  = MDC_YES;
    MDC_QUANTIFY  = MDC_NO;
    MDC_CALIBRATE = MDC_NO;
  }

  if ( (MDC_PIXELS == MDC_YES) && (MDC_QUANTIFY == MDC_YES) )
    MDC_CALIBRATE = MDC_NO;

  /* conversion related issues */
  if ((MDC_CONVERT  == MDC_YES) || (XMDC_GUI == MDC_YES)) {
    /* with output file */
    if (XMDC_GUI == MDC_NO) {
      if ( total[MDC_CONVS] == 0 )
        MdcPrntErr(MDC_NO_CODE,"No conversion formats specified");
      if (MDC_PIXELS   == MDC_YES)
        MdcPrntErr(MDC_NO_CODE,"Options '-c' & '-p(a)' are mutually exclusive");
    }
  }else{
    /* without output file */
    if (MDC_RENAME == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Option '-r' requires '-c' conversion specified");
    if (MDC_EXTRACT == MDC_YES)
      MdcPrntErr(MDC_NO_CODE,"Option '-e' requires '-c' conversion specified");
  }

  /* study identification */
  if ( (MDC_PATIENT_ANON == MDC_YES) && (MDC_PATIENT_IDENT == MDC_YES) )
    MdcPrntErr(MDC_NO_CODE,"Options '-anon' & '-ident' are mutually exclusive");

  /* the must do FINAL quantification/calibration check */
  if ( (MDC_QUANTIFY == MDC_YES) && (MDC_CALIBRATE == MDC_YES) )
    MdcPrntErr(MDC_NO_CODE,"Options '-qs' & '-qc' are mutually exclusive");

  /* some checks on mosaic settings */
  if ( (MDC_DICOM_MOSAIC_DO_INTERL == MDC_YES) &&
       (MDC_DICOM_MOSAIC_FORCED == MDC_NO) )
    MdcPrntWarn("Option '-interl' requires mosaic forced");

  /* color */
  if (MDC_COLOR_MODE == MDC_COLOR_RGB) {
    if (MDC_MAKE_GRAY == MDC_YES)
      MdcPrntWarn("Option -24 overrides -g option");
    if (MDC_DITHER_COLOR == MDC_YES)
      MdcPrntWarn("Option -24 overrides -dith option");
  }

  /* giving an overview of settings */
  if (MDC_VERBOSE ) {

    if (MDC_WRITE_ENDIAN == MDC_LITTLE_ENDIAN)
      MdcPrntMesg("Writing in little endian as default");
    else
      MdcPrntMesg("Writing in big endian as default");

    switch (MDC_FALLBACK_FRMT) {
      case MDC_FRMT_ANLZ:
         MdcPrntMesg("Read fallback format Analyze (SPM)"); break;
      case MDC_FRMT_DICM:
         MdcPrntMesg("Read fallback format DICOM 3.0");     break;
      case MDC_FRMT_CONC:
         MdcPrntMesg("Read fallback format Concorde/uPET"); break;
      case MDC_FRMT_ECAT6:
         MdcPrntMesg("Read fallback format ECAT 6.4");      break;
    }

    /* flags */

    if (MDC_FILE_OVERWRITE == MDC_YES)
    MdcPrntMesg("Files overwrite    is ON");
    if (MDC_FILE_STDIN  == MDC_YES)
    MdcPrntMesg("Input  from stdin  is ON");
    if (MDC_FILE_STDOUT == MDC_YES)
    MdcPrntMesg("Output to  stdout  is ON");
    if (MDC_QUANTIFY == MDC_YES)
    MdcPrntMesg("Quantification     is ON  (ECAT units=[counts/second/pixel])");
    if (MDC_CALIBRATE == MDC_YES)
    MdcPrntMesg("Calibration        is ON  (ECAT units=[uCi/ml])");
    if (MDC_NEGATIVE  == MDC_YES)
    MdcPrntMesg("Negative pixels    is ON");
    if (MDC_CONTRAST_REMAP == MDC_YES)
    MdcPrntMesg("Contrast remapping is ON");
    if (MDC_ANLZ_SPM  == MDC_YES)
    MdcPrntMesg("Analyze for SPM    is ON");
    if (MDC_DICOM_MOSAIC_ENABLED == MDC_YES)
    MdcPrntMesg("Mosaic support     is ON");
    if (MDC_DICOM_MOSAIC_FORCED == MDC_YES)
    MdcPrntMesg("Mosaic forced      is ON");
    if (MDC_DICOM_MOSAIC_FIX_VOXEL == MDC_YES)
    MdcPrntMesg("Mosaic fix voxel   is ON");
    if (MDC_TRUE_GAP == MDC_YES)
    MdcPrntMesg("True gap/overlap   is ON");

    if (MDC_DICOM_WRITE_ENCAP_UNCOMP == MDC_YES) {

    MdcPrntMesg("Dicom encap uncomp is ON");
    }else{

    if (MDC_DICOM_WRITE_IMPLICIT == MDC_YES)
    MdcPrntMesg("Dicom implicit     is ON");
    if (MDC_DICOM_WRITE_NOMETA == MDC_YES)
    MdcPrntMesg("Dicom no meta      is ON");

    }
    if (MDC_NORM_OVER_FRAMES == MDC_YES)
    MdcPrntMesg("Norm over frames   is ON");
    if (MDC_SKIP_PREVIEW == MDC_YES)
    MdcPrntMesg("Skip preview slice is ON");
    if (MDC_IGNORE_PATH == MDC_YES)
    MdcPrntMesg("Ignore path fname  is ON");
    if (MDC_ALIAS_NAME == MDC_YES)
    MdcPrntMesg("Alias file name    is ON");
    if (MDC_PREFIX_DISABLED == MDC_YES)
    MdcPrntMesg("Disable prefix     is ON");
    if (MDC_FLIP_HORIZONTAL == MDC_YES)
    MdcPrntMesg("Flip horizontal    is ON");
    if (MDC_FLIP_VERTICAL   == MDC_YES)
    MdcPrntMesg("Flip vertical      is ON");
    if (MDC_SORT_REVERSE    == MDC_YES)
    MdcPrntMesg("Sort reverse       is ON");
    if (MDC_SORT_CINE_APPLY       == MDC_YES)
    MdcPrntMesg("Sort cine apply    is ON");
    if (MDC_SORT_CINE_UNDO   == MDC_YES)
    MdcPrntMesg("Sort cine undo     is ON");
    if (MDC_MAKE_SQUARE == MDC_TRANSF_SQR1)
    MdcPrntMesg("Make square images is ON");
    if (MDC_MAKE_SQUARE == MDC_TRANSF_SQR2)
    MdcPrntMesg("Make square pwr2   is ON");
    if (MDC_FORCE_CONTRAST == MDC_YES)
    MdcPrntMesg("DICOM contrast     is ON");
    if (MDC_FORCE_RESCALE == MDC_YES)
    MdcPrntMesg("DICOM rescale      is ON");
    if (MDC_COLOR_MODE == MDC_COLOR_RGB)
    MdcPrntMesg("Color 24 bits RGB  is ON");
    if (MDC_COLOR_MODE == MDC_COLOR_INDEXED)
    MdcPrntMesg("Color  8 bits map  is ON");
    if (MDC_DITHER_COLOR == MDC_YES)
    MdcPrntMesg("Color dithering    is ON");
    if (MDC_MAKE_GRAY == MDC_YES)
    MdcPrntMesg("Color to gray      is ON");


    if (MDC_FORCE_INT != MDC_NO) {
      switch (MDC_FORCE_INT) {
        case BIT8_U :
            MdcPrntMesg("Writing Uint8 pixs is ON  (quantified values lost!)");
            break;
        case BIT16_S:
            MdcPrntMesg("Writing Int16 pixs is ON  (quantified values lost!)");
            break;
        default     :
            MdcPrntMesg("Writing Int16 pixs is ON  (quantified values lost!)");
      }

      if (MDC_INT16_BITS_USED == 12)
            MdcPrntMesg("Using only 12 bits is ON");

    }
  }

  return(MDC_OK);

}

char *MdcApplyReadOptions(FILEINFO *fi)
{
  char *msg=NULL;

  /* anonymize patient information */
  if (MDC_PATIENT_ANON) MdcMakePatAnonymous(fi);

  /* ask for patient information */
  if (MDC_PATIENT_IDENT) MdcGivePatInformation(fi);

  /* edit the FILEINFO structure */
  if (MDC_EDIT_FI) {
    if ((msg = MdcEditFI(fi)) != NULL) return(msg);
  }

  /* print FILEINFO structure */
  if (MDC_DEBUG)  MdcPrintFI(fi);

  /* print out pixel requested values */
  if (MDC_PIXELS) MdcDisplayPixels(fi);

  /* extract some images */
  if (MDC_EXTRACT) {
    if ((msg = MdcExtractImages(fi)) != NULL) return(msg);
  }

  /* reslice images as specified */
  if (MDC_RESLICE != MDC_NO) {
    if ((msg = MdcResliceImages(fi, MDC_RESLICE)) != NULL) return(msg);
  }

  /* rename base filename */
  if (MDC_RENAME) MdcRenameFile(fi->ifname);

  return(NULL);

}
