/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xreset.c                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : reset file/structures routines                           *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcStructsReset()   - Reset the global structures       *
 *                XMdcViewerReset()    - Reset the viewer window           *
 *                XMdcFileReset()      - Reset and close current file      *
 *                XMdcColorMapReset()  - Reset to a new colormap           *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/
void XMdcStructsReset(void)
{
  my.viewbox = NULL;
  my.cmap = NULL;
  my.curpage = 0; my.prevpage = 0;
  my.scale_width = 1.; my.scale_height = 1.;
  my.RESIZE = MDC_YES;
}

void XMdcViewerReset(void)
{
  Uint32 i;

  if (my.viewwindow != NULL) {
    MdcDebugPrint("Removing viewer box & images ...");
    gtk_widget_destroy(my.viewbox);
    gtk_widget_unrealize(my.viewwindow);
    g_object_unref(my.imcmap);

    for (i=0; i<my.real_images_on_page; i++) g_object_unref(my.im[i]);

    MdcFree(my.im);
    MdcFree(my.pagenumber);
    MdcFree(my.imagenumber);
    MdcFree(my.realnumber);
    MdcFree(my.image);
#ifdef MDC_USE_SIGNAL_BLOCKER
    MdcFree(my.sblkr);
#endif
    XMdcStructsReset();
  }
}

void XMdcFileReset(void)
{
  if (XMDC_FILE_OPEN == MDC_YES) {
    MdcDebugPrint("Cleaning up previous image ...");
    XMDC_FILE_OPEN = MDC_NO; /* prevent second time after killing Viewer */
    XMDC_FILE_TYPE = XMDC_NORMAL;
    MdcCleanUpFI(my.fi);
    XMdcViewerReset();
  }
}

void XMdcColorMapReset(int map)
{
  MdcGetColorMap(map,my.fi->palette);
  my.fi->map = map;
}

