/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-ecat72.h                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-ecat72.c header file                                   *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_ECAT72_H__
#define __M_ECAT72_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

#define MDC_ECAT7_SIG   "MATRIX7"

#define MDC_ECAT7_MAX_MATRICES          5000

#define MDC_ECAT7_MAX_PLANES            1024
#define MDC_ECAT7_MAX_FRAMES             512
#define MDC_ECAT7_MAX_GATES               32
#define MDC_ECAT7_MAX_BEDS                32

#define MDC_ECAT7_FILE_TYPE_UNKNOWN        0
#define MDC_ECAT7_FILE_TYPE_SINOGRAM       1
#define MDC_ECAT7_FILE_TYPE_IMAGE16        2
#define MDC_ECAT7_FILE_TYPE_ATTNCORR       3
#define MDC_ECAT7_FILE_TYPE_NORM           4
#define MDC_ECAT7_FILE_TYPE_POLARMAP       5
#define MDC_ECAT7_FILE_TYPE_VOLUME8        6
#define MDC_ECAT7_FILE_TYPE_VOLUME16       7
#define MDC_ECAT7_FILE_TYPE_PROJECTION8    8
#define MDC_ECAT7_FILE_TYPE_PROJECTION16   9
#define MDC_ECAT7_FILE_TYPE_IMAGE8         10
#define MDC_ECAT7_FILE_TYPE_3DSINO16       11
#define MDC_ECAT7_FILE_TYPE_3DSINO8        12
#define MDC_ECAT7_FILE_TYPE_3DNORM         13
#define MDC_ECAT7_FILE_TYPE_3DSINOFLT      14

#define MDC_ECAT7_SOURCE_TYPE_NONE         1
#define MDC_ECAT7_SOURCE_TYPE_RRING        2
#define MDC_ECAT7_SOURCE_TYPE_RING         3
#define MDC_ECAT7_SOURCE_TYPE_ROD          4
#define MDC_ECAT7_SOURCE_TYPE_RROD         5

#define MDC_ECAT7_FEETFIRST_PRONE          0
#define MDC_ECAT7_HEADFIRST_PRONE          1
#define MDC_ECAT7_FEETFIRST_SUPINE         2
#define MDC_ECAT7_HEADFIRST_SUPINE         3
#define MDC_ECAT7_FEETFIRST_RIGHT          4
#define MDC_ECAT7_HEADFIRST_RIGHT          5
#define MDC_ECAT7_FEETFIRST_LEFT           6
#define MDC_ECAT7_HEADFIRST_LEFT           7

#define MDC_ECAT7_SCAN_UNKNOWN              0
#define MDC_ECAT7_SCAN_BLANK                1
#define MDC_ECAT7_SCAN_TRANSMISSION         2
#define MDC_ECAT7_SCAN_STATIC_EMISSION      3
#define MDC_ECAT7_SCAN_DYNAMIC_EMISSION     4
#define MDC_ECAT7_SCAN_GATED_EMISSION       5
#define MDC_ECAT7_SCAN_TRANS_RECTILINEAR    6
#define MDC_ECAT7_SCAN_EMISSION_RECTILINEAR 7

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckECAT7(FILEINFO *fi);
void MdcEcatPrintMainHdr(Mdc_Main_header7 *mh);
void MdcEcatPrintImgSubHdr(Mdc_Image_subheader7 *ish, int nr);
void MdcEcatPrintAttnSubHdr(Mdc_Attn_subheader7 *ash, int nr);
void MdcEcatPrintScanSubHdr(Mdc_Scan_subheader7 *ssh);
void MdcEcatPrintNormSubHdr(Mdc_Norm_subheader7 *nsh);
const char *MdcReadECAT7(FILEINFO *fi);
const char *MdcWriteECAT7(FILEINFO *fi);

#endif

