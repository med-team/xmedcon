/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-xtract.h                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-xtract.c header file                                   *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_XTRACT_H__
#define __M_XTRACT_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

typedef struct MdcExtractInputStruct_t {
    char list[MDC_MAX_LIST+1];
    int INTERACTIVE;
    Int32  style;
    Uint32 *inrs;
    Uint32 num_p, num_f, num_g, num_b;
}MdcExtractInputStruct;


extern MdcExtractInputStruct mdcextractinput;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/
int MdcGetImagesToExtract(FILEINFO *fi, MdcExtractInputStruct *input);
char *MdcExtractImages(FILEINFO *fi);

#endif

