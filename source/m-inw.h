/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-inw.h                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-inw.c header file                                      *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_INW_H__
#define __M_INW_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

#define MDC_INW_VERS_HIGH       1           /* INW version 1.0              */
#define MDC_INW_VERS_LOW        0

#define MDC_INW_SIG     (int)(0x789abcde)   /* for identification of header */

/* scanner type */
#define EcatII          2
#define EcatIV          4

/* reconstruction methods */
#define reconFBP        'i'
#define reconMaxLikFV   'm'
#define reconMaxLik     'l'
#define reconMaxPos     'e'
#define reconALL        "imle"
#define reconTEXT       "Filtered Backprojection",\
                        "Maximum likelihood  (Frank Vermeulen)",\
                        "Maximum likelihood (Tom De Backer)",\
                        "Maximum a Posteriori"

/* image headers */
typedef struct Head_start_t {

        Int32 mark;           /* should be HEADER_MARK                */
        Int16 version;        /* high*256 + low                       */
        Int16 size_header;    /* whole header (in bytes)              */
        Int16 size_start;     /* sizeof(Head_start_t)                 */
        Int16 size_gen;       /* sizeof(Head_gen_t)                   */
        Int16 size_spec;      /* sizeof(Head_spec_t)                  */
        char  reserved[10];

} MDC_INW_HEAD_START;                 /* current size: 24                     */

#define MDC_INW_HEAD_START_SIZE 24

typedef struct Head_gen_t {

        Int16 no;             /* number of planes                     */
        Int16 sizeX;          /* number of columns                    */
        Int16 sizeY;          /* number of rows                       */
        Int16 pixel_type;     /* sizeof(pixel)                        */
                              /* for compatibility only 2 is allowed  */
        Int16 init_trans;     /* initial translation (mm)             */
        Int16 dummy1;         /* for alignment reasons only           */

        /* Note: We take the positive axis into the gantry !
                 This means, if the patient lies with his head into
                 the gantry, the head has higher translation offset
                 than his feet                                        */

        char  day[12];        /* day of first scan eg. 04-AUG-89      */
        Int32 time;           /* seconds after midnight               */
                              /* first scan or time activity measured */
        float decay_cst;      /* NOT half_life ! (discards log(2))    */
                              /* decay_cst = half_life / log(2)       */
        float pixel_size;     /* sampling distance (mm)               */
        float max;            /* scaled maximum of all images         */
        float min;
        Int16 scanner;        /* EcatII, EcatIV                       */
        char  reconstruction; /* reconFBP, reconMaxLik,...            */
        char  recon_version;  /* reconstruction version (0-99)        */
        char  reserved[24];

} MDC_INW_HEAD_GEN;                   /* current size: 72                     */

#define MDC_INW_HEAD_GEN_SIZE 72

typedef struct Head_spec_t {

        Int32 time;           /* time relative to gen.time (secs)     */
        float cal_cst;        /* abs_activ(uCU/ml) = cal_cst*pix_val  */
                              /* cal_cst = calibr_cst * decay_comp    */
                              /* decay_comp = exp(time/decay_cst)     */
        Int32 max;            /* maximum in plane                     */
        Int32 min;            /* minimum in plane                     */
        Int16 trans;          /* translation relative to gen.trans mm */
        char  reserved[6];

} MDC_INW_HEAD_SPEC;                  /* current size: 24                     */

#define MDC_INW_HEAD_SPEC_SIZE 24

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckINW(FILEINFO *fi);
char *MdcReadINW(FILEINFO *fi);
int MdcWriteHeadStart(FILEINFO *fi);
int MdcWriteHeadGen(FILEINFO *fi);
int MdcSkipHeadSpecs(FILEINFO *fi);
int MdcWriteHeadSpecs(FILEINFO *fi);
char *MdcWriteINW(FILEINFO *fi);

#endif

