/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xoptions.c                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : handle (X)MedCon options routines                        *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcOptionsMedconCallbackApply() - Options Apply callback*
 *                XMdcOptionsMedconAddTabPixels()  - Add Pixels tab        *
 *                XMdcOptionsMedconAddTabFiles()   - Add Files tab         *
 *                XMdcOptionsMedconAddTabSlices()  - Add Slices tab        *
 *                XMdcOptionsMedconAddTabFormats() - Add Formats tab       *
 *                XMdcOptionsMedconAddTabMosaic()  - Add Mosaic tab        *
 *                XMdcOptionsMedconSel()           - Options Medcon select *
 *                XMdcOptionsRenderSel()           - Options Render select *
 *                XMdcOptionsResizeSel()           - Options Resize select *
 *                XMdcOptionsColorMapSel()         - Options Map    select *
 *                XMdcOptionsLabelSel()            - Options Label  select *
 *                XMdcOptionsPagesSel()            - Options Pages  select *
 *                XMdcSensitiveBitsUsed12()        - Sensitive   button    *
 *                XMdcUnsensitveBitsUsed12()       - Unsensitive button    *
 *                XMdcToggleSensitivityMosaic()    - Toggle mosaic         *
 *                XMdcToggleSensitivityForced()    - Toggle mosaic forced  *
 *                XMdcInitMosaicFrame()            - Init mosaic frame     *
 *                XMdcToggleSensitivityCine()      - Toggle cine           *
 *                XMdcInitCineButtons()            - Init cine buttons     *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                             D E F I N E S
****************************************************************************/

static GtkWidget *woption=NULL;

static GtkWidget *wmosaic=NULL;
static GtkWidget *wforced=NULL;

static GtkWidget *wlabel=NULL;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/
void XMdcOptionsMedconCallbackApply(GtkWidget *widget, gpointer data)
{
  GtkSpinButton *spin;

  /* Pixel Sign */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixPositives))) {
    MDC_NEGATIVE=MDC_NO;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixNegatives))) {
    MDC_NEGATIVE=MDC_YES;
  }

  /* Pixel Value */
  MDC_QUANTIFY = MDC_NO; MDC_CALIBRATE = MDC_NO;
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixNoQuant))) {
    MDC_QUANTIFY = MDC_NO; MDC_CALIBRATE = MDC_NO;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixQuantify))) {
    MDC_QUANTIFY=MDC_YES;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixCalibrate))) {
    MDC_CALIBRATE=MDC_YES;
  }

  /* Pixel Types */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixTypeNONE))) {
    MDC_FORCE_INT=MDC_NO;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixTypeBIT8_U))) {
    MDC_FORCE_INT=BIT8_U;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PixTypeBIT16_S))) {
    MDC_FORCE_INT=BIT16_S;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.BitsUsed12))) {
    MDC_INT16_BITS_USED = 12;
  }else{
    MDC_INT16_BITS_USED = 16;
  }

  if (MDC_FORCE_INT != MDC_NO) {
    if (MDC_QUANTIFY || MDC_CALIBRATE)
      XMdcDisplayWarn("Quantified values could get lost (integer pixel write)");
  }

  /* File Endian Type */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FileTypeLITTLE))) {
    MDC_WRITE_ENDIAN = MDC_LITTLE_ENDIAN;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FileTypeBIG))) {
    MDC_WRITE_ENDIAN = MDC_BIG_ENDIAN;
  }

  /* Flipping - Sorting*/
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FlipHoriz))) {
    MDC_FLIP_HORIZONTAL = MDC_YES;
  }else{
    MDC_FLIP_HORIZONTAL = MDC_NO;
  }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FlipVert))) {
    MDC_FLIP_VERTICAL = MDC_YES;
  }else{
    MDC_FLIP_VERTICAL = MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SortReverse))) {
    MDC_SORT_REVERSE = MDC_YES;
  }else{
    MDC_SORT_REVERSE = MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SortCine))) {
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SortCineApply))) {
      MDC_SORT_CINE_APPLY = MDC_YES;
    }else{
      MDC_SORT_CINE_APPLY = MDC_NO;
    }
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SortCineUndo))) {
      MDC_SORT_CINE_UNDO = MDC_YES;
    }else{
      MDC_SORT_CINE_UNDO = MDC_NO;
    }
  }else{
    MDC_SORT_CINE_APPLY = MDC_NO; MDC_SORT_CINE_UNDO = MDC_NO;
  }

  /* Matrix */
  /* Image Flipping */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.MakeSqrNo))) {
    MDC_MAKE_SQUARE = MDC_NO;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.MakeSqr1))) {
    MDC_MAKE_SQUARE = MDC_TRANSF_SQR1;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.MakeSqr2))) {
    MDC_MAKE_SQUARE = MDC_TRANSF_SQR2;
  }

  /* Normalization */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.NormOverFrames))) {
    MDC_NORM_OVER_FRAMES=MDC_YES;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.NormOverAll))) {
    MDC_NORM_OVER_FRAMES=MDC_NO;
  }

  /* Fallback Read Format */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FallbackNONE))) {
    MDC_FALLBACK_FRMT=MDC_FRMT_NONE;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FallbackANLZ))) {
    MDC_FALLBACK_FRMT=MDC_FRMT_ANLZ;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FallbackDICM))) {
    MDC_FALLBACK_FRMT=MDC_FRMT_DICM;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FallbackCONC))) {
    MDC_FALLBACK_FRMT=MDC_FRMT_CONC;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.FallbackECAT))) {
    MDC_FALLBACK_FRMT=MDC_FRMT_ECAT6;
  }

  /* Split Output*/
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SplitNone))) {
    MDC_FILE_SPLIT=MDC_SPLIT_NONE;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SplitFrames))) {
    MDC_FILE_SPLIT=MDC_SPLIT_PER_FRAME;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.SplitSlices))) {
    MDC_FILE_SPLIT=MDC_SPLIT_PER_SLICE;
  }

  /* Color */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.ColorModeIndexed))) {
    MDC_COLOR_MODE = MDC_COLOR_INDEXED;
  }else{
    MDC_COLOR_MODE = MDC_COLOR_RGB;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.ColorMakeGray))) {
    MDC_MAKE_GRAY=MDC_YES;
  }else{
    MDC_MAKE_GRAY=MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.ColorDither))) {
    MDC_DITHER_COLOR=MDC_YES;
  }else{
    MDC_DITHER_COLOR=MDC_NO;
  }

  /* Padding */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PadAround))) {
    MDC_PADDING_MODE = MDC_PAD_AROUND;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PadTopLeft))) {
    MDC_PADDING_MODE = MDC_PAD_TOP_LEFT;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.PadBottomRight))) {
    MDC_PADDING_MODE = MDC_PAD_BOTTOM_RIGHT;
  }

  /* File Names */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.NameAlias))) {
    MDC_ALIAS_NAME=MDC_YES;
  }else{
    MDC_ALIAS_NAME=MDC_NO;
  }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.NameNoPrefix))) {
    MDC_PREFIX_DISABLED=MDC_YES;
  }else{
    MDC_PREFIX_DISABLED=MDC_NO;
  }

  /* DICOM */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmContrast))) {
    MDC_CONTRAST_REMAP=MDC_YES;
  }else{
    MDC_CONTRAST_REMAP=MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmTrueGap))) {
    MDC_TRUE_GAP=MDC_YES;
  }else{
    MDC_TRUE_GAP=MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmWriteImplicit))) {
    MDC_DICOM_WRITE_IMPLICIT = MDC_YES;
  }else{
    MDC_DICOM_WRITE_IMPLICIT = MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmWriteNoMeta))) {
    MDC_DICOM_WRITE_NOMETA = MDC_YES;
  }else{
    MDC_DICOM_WRITE_NOMETA = MDC_NO;
  }

  /* Siemens - MOSAIC */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmMosaicEnabled))) {
    MDC_DICOM_MOSAIC_ENABLED = MDC_YES;
  }else{
    MDC_DICOM_MOSAIC_ENABLED = MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmMosaicForced))) {
    MDC_DICOM_MOSAIC_FORCED = MDC_YES;
  }else{
    MDC_DICOM_MOSAIC_FORCED = MDC_NO;
  }

  spin = GTK_SPIN_BUTTON(sOptionsMedCon.DicmMosaicWidth);
  mdc_mosaic_width = (Uint32)gtk_spin_button_get_value_as_int(spin);

  spin = GTK_SPIN_BUTTON(sOptionsMedCon.DicmMosaicHeight);
  mdc_mosaic_height= (Uint32)gtk_spin_button_get_value_as_int(spin);

  spin = GTK_SPIN_BUTTON(sOptionsMedCon.DicmMosaicNumber);
  mdc_mosaic_number= (Uint32)gtk_spin_button_get_value_as_int(spin);

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmMosaicDoInterl))) {
    MDC_DICOM_MOSAIC_DO_INTERL=MDC_YES;
  }else{
    MDC_DICOM_MOSAIC_DO_INTERL=MDC_NO;
  }
  mdc_mosaic_interlaced = MDC_DICOM_MOSAIC_DO_INTERL;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.DicmMosaicFixVoxel))) {
    MDC_DICOM_MOSAIC_FIX_VOXEL=MDC_YES;
  }else{
    MDC_DICOM_MOSAIC_FIX_VOXEL=MDC_NO;
  }

  XMdcInitMosaicFrame();

  /* Analyze (SPM) */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.AnlzSPM))) {
    MDC_ANLZ_SPM=MDC_YES;
    if ((MDC_CALIBRATE == MDC_NO) && (MDC_QUANTIFY == MDC_NO))
      XMdcDisplayWarn("For SPM scaling you should select\n a pixels value quantitation");
  }else{
    MDC_ANLZ_SPM=MDC_NO;
  }

  /* InterFile */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.IntfSkip1))) {
    MDC_SKIP_PREVIEW=MDC_YES;
  }else{
    MDC_SKIP_PREVIEW=MDC_NO;
  }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.IntfNoPath))) {
    MDC_IGNORE_PATH=MDC_YES;
  }else{
    MDC_IGNORE_PATH=MDC_NO;
  }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.IntfSingleFile))) {
    MDC_SINGLE_FILE=MDC_YES;
  }else{
    MDC_SINGLE_FILE=MDC_NO;
  }

  /* ECAT 6 */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.EcatSortAnatom))) {
    MDC_ECAT6_SORT = MDC_ANATOMICAL;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sOptionsMedCon.EcatSortByFrame))) {
    MDC_ECAT6_SORT = MDC_BYFRAME;
  }

  if ((XMDC_FILE_OPEN == MDC_YES) && (XMDC_FILE_TYPE == XMDC_NORMAL))
    XMdcAskYesNo(G_CALLBACK(XMdcRereadFile),G_CALLBACK(NULL)
                                                ,"Reread current file?");

}

void XMdcOptionsMedconAddTabPixels(GtkWidget *notebook)
{
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *grid;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *tablabel;
  GSList *group;

  /* tab page Pixels */
  box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
  gtk_widget_show(box2);

  tablabel = gtk_label_new("Pixels");
  gtk_widget_show(tablabel);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

  grid = gtk_grid_new();
  gtk_container_add(GTK_CONTAINER(box2), grid);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
  gtk_widget_set_hexpand(grid, TRUE);
  gtk_widget_show(grid);

  /* Pixel Value */
  frame = gtk_frame_new("Value");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,0,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,
                               "[rw]  without quantitation");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_QUANTIFY == MDC_NO && MDC_CALIBRATE == MDC_NO)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PixNoQuant = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,
                               "[rw]  quantified              (floats)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_QUANTIFY == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PixQuantify = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,
                               "[rw]  quantified & calibrated (floats)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_CALIBRATE == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PixCalibrate = button;

  /* Pixel Sign */
  frame = gtk_frame_new("Sign");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,"[rw]  positives only");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_NEGATIVE == MDC_NO)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PixPositives = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[rw]  positives & negatives");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_NEGATIVE == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PixNegatives = button;

  /* Pixel Types   */
  frame = gtk_frame_new("Types");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,0,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,"[w]  writing default pixels");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FORCE_INT == MDC_NO)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  g_signal_connect(button, "toggled",
                     G_CALLBACK(XMdcUnsensitiveBitsUsed12),NULL);
  gtk_widget_show(button);
  sOptionsMedCon.PixTypeNONE = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[w]  writing  Uint8  pixels");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FORCE_INT == BIT8_U)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  g_signal_connect(button, "toggled",
                     G_CALLBACK(XMdcUnsensitiveBitsUsed12),NULL);
  gtk_widget_show(button);
  sOptionsMedCon.PixTypeBIT8_U = button;

  box4 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
  gtk_container_add(GTK_CONTAINER(box3),box4);
  gtk_container_set_border_width(GTK_CONTAINER(box4),0);
  gtk_widget_show(box4);

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[w]  writing  Int16  pixels");
  gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
  if (MDC_FORCE_INT == BIT16_S)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  g_signal_connect(button, "toggled",
                     G_CALLBACK(XMdcSensitiveBitsUsed12),NULL);
  gtk_widget_show(button);
  sOptionsMedCon.PixTypeBIT16_S = button;

  button = gtk_check_button_new_with_label("12 bits used");
  gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
  if (MDC_INT16_BITS_USED == 12)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.BitsUsed12 = button;

  if (MDC_FORCE_INT == BIT16_S) {
    XMdcSensitiveBitsUsed12(NULL,NULL);
  }else{
    XMdcUnsensitiveBitsUsed12(NULL,NULL);
  }

  /* Normalization */
  frame = gtk_frame_new("Normalization");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL, "[r]  over images in frame");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_NORM_OVER_FRAMES == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.NormOverFrames = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[r]  over all images");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_NORM_OVER_FRAMES == MDC_NO)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.NormOverAll = button;
}

void XMdcOptionsMedconAddTabFiles(GtkWidget *notebook)
{
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *grid;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *tablabel;
  GSList *group;

  /* tab page Files */
  box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
  gtk_widget_show(box2);

  tablabel = gtk_label_new("Files");
  gtk_widget_show(tablabel);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

  grid = gtk_grid_new();
  gtk_container_add(GTK_CONTAINER(box2), grid);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
  gtk_widget_set_hexpand(grid, TRUE);
  gtk_widget_show(grid);

  /* File Endian Type */
  frame = gtk_frame_new("Endian Type");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,0,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,
                               "[w]  writing LITTLE endian (MSB last)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_WRITE_ENDIAN == MDC_LITTLE_ENDIAN)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FileTypeLITTLE = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,
                               "[w]  writing  BIG   endian (MSB first)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_WRITE_ENDIAN == MDC_BIG_ENDIAN)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FileTypeBIG = button;

  /* File Names */
  frame = gtk_frame_new("File Names");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
/*  gtk_table_attach_defaults(GTK_TABLE(table),frame,1,2,0,1);*/
  gtk_grid_attach(GTK_GRID(grid),frame,0,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_check_button_new_with_label("[w]  alias with patient/study id");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_ALIAS_NAME == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.NameAlias = button;

  button = gtk_check_button_new_with_label("[w]  disable numbered prefix");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_PREFIX_DISABLED == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.NameNoPrefix = button;

  /* Fallback Read Format */
  frame = gtk_frame_new("Fallback Read Format");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,"[r]  without fallback");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FALLBACK_FRMT == MDC_FRMT_NONE)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FallbackNONE = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[r]  Analyze (SPM)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FALLBACK_FRMT == MDC_FRMT_ANLZ)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FallbackANLZ = button;
#if ! MDC_INCLUDE_ANLZ
  gtk_widget_set_sensitive(button,FALSE);
#endif

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[r]  DICOM 3.0");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FALLBACK_FRMT == MDC_FRMT_DICM)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FallbackDICM = button;
#if ! MDC_INCLUDE_DICM
  gtk_widget_set_sensitive(button,FALSE);
#endif

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[r]  Concorde/uPET");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FALLBACK_FRMT == MDC_FRMT_CONC)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FallbackCONC = button;
#if ! MDC_INCLUDE_CONC
  gtk_widget_set_sensitive(button,FALSE);
#endif

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"[r]  ECAT 6.4");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FALLBACK_FRMT == MDC_FRMT_ECAT6)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FallbackECAT = button;
#if ! MDC_INCLUDE_ECAT
  gtk_widget_set_sensitive(button,FALSE);
#endif

  /* Split Output */
  frame = gtk_frame_new("Split Output");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL
           ,"[w]  none, keep volume in one file");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FILE_SPLIT == MDC_SPLIT_NONE)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.SplitNone = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group
           ,"[w]  split over each frame group");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FILE_SPLIT == MDC_SPLIT_PER_FRAME)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.SplitFrames = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button =gtk_radio_button_new_with_label(group
          ,"[w]  split over each image slice");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FILE_SPLIT == MDC_SPLIT_PER_SLICE)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.SplitSlices = button;

}

void XMdcOptionsMedconAddTabSlices(GtkWidget *notebook)
{
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *grid;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *tablabel;
  GSList *group;

  /* tab page Slices */
  box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
  gtk_widget_show(box2);

  tablabel = gtk_label_new("Slices");
  gtk_widget_show(tablabel);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

  grid = gtk_grid_new();
  gtk_container_add(GTK_CONTAINER(box2), grid);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
  gtk_widget_set_hexpand(grid, TRUE);
  gtk_widget_show(grid);

  /* Image Flipping*/
  frame = gtk_frame_new("Flip - Sorting");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
/*  gtk_table_attach_defaults(GTK_TABLE(table),frame,0,1,0,1);*/
  gtk_grid_attach(GTK_GRID(grid),frame,0,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_check_button_new_with_label("[r]  flip horizontal");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FLIP_HORIZONTAL == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FlipHoriz = button;

  button =gtk_check_button_new_with_label("[r]  flip vertical");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_FLIP_VERTICAL == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.FlipVert = button;

  button =gtk_check_button_new_with_label("[r]  reverse slices");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_SORT_REVERSE == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.SortReverse = button;

  box4 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
  gtk_container_add(GTK_CONTAINER(box3),box4);
  gtk_container_set_border_width(GTK_CONTAINER(box4),0);
  gtk_widget_show(box4);

  button = gtk_check_button_new_with_label("[r]  cine sorting:");
  gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
  g_signal_connect(button, "toggled",
                     G_CALLBACK(XMdcToggleSensitivityCine),NULL);
  gtk_widget_show(button);
  sOptionsMedCon.SortCine = button;

  button = gtk_radio_button_new_with_label(NULL,"apply");
  gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
  gtk_widget_show(button);
  sOptionsMedCon.SortCineApply = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,"undo");
  gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
  gtk_widget_show(button);
  sOptionsMedCon.SortCineUndo = button;

  XMdcInitCineButtons();

  /* Matrix Transform */
  frame = gtk_frame_new("Matrix");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,"[r]  keep dimensions");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_MAKE_SQUARE == MDC_NO)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.MakeSqrNo = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,
                               "[r]  make square (largest dimension)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_MAKE_SQUARE == MDC_TRANSF_SQR1)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.MakeSqr1 = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,
                               "[r]  make square (nearest power of two)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_MAKE_SQUARE == MDC_TRANSF_SQR2)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.MakeSqr2 = button;

  /* Color */
  frame = gtk_frame_new("Color");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,0,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_check_button_new_with_label("[r]  simply remap to gray scale");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_MAKE_GRAY == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.ColorMakeGray = button;

  button =
  gtk_check_button_new_with_label("[r]  force indexed colormap (8-bit)");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_COLOR_MODE == MDC_COLOR_INDEXED)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.ColorModeIndexed = button;

  button = gtk_check_button_new_with_label("[r]  reduce color by dithering");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_DITHER_COLOR == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.ColorDither = button;

  /* Padding */
  frame = gtk_frame_new("Padding");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button =
  gtk_radio_button_new_with_label(NULL,"[rw]  symmetrical around image");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_PADDING_MODE == MDC_PAD_AROUND)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PadAround = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button =
  gtk_radio_button_new_with_label(group,"[rw]  before first row and column");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_PADDING_MODE == MDC_PAD_TOP_LEFT)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PadTopLeft = button;

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button =
  gtk_radio_button_new_with_label(group,"[rw]  after last row and column");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_PADDING_MODE == MDC_PAD_BOTTOM_RIGHT)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.PadBottomRight = button;

}

void XMdcOptionsMedconAddTabFormats(GtkWidget *notebook)
{
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *grid;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *tablabel;
  GSList *group;

  /* tab page Formats */
  box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
  gtk_widget_show(box2);

  tablabel = gtk_label_new("Formats");
  gtk_widget_show(tablabel);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

  grid = gtk_grid_new();
  gtk_container_add(GTK_CONTAINER(box2), grid);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
  gtk_widget_set_hexpand(grid, TRUE);
  gtk_widget_show(grid);

  /* Analyze (SPM) */
  frame = gtk_frame_new("Analyze (SPM)");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button =gtk_check_button_new_with_label("[rw]  SPM version with scale & offset");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_ANLZ_SPM == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.AnlzSPM = button;
#if ! MDC_INCLUDE_ANLZ
  gtk_widget_set_sensitive(button,FALSE);
#endif

  /* DICOM 3.0 */
  frame = gtk_frame_new("Dicom - Acr/Nema");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
/*  gtk_table_attach_defaults(GTK_TABLE(table),frame,1,2,0,1);*/
  gtk_grid_attach(GTK_GRID(grid),frame,0,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_check_button_new_with_label("[r]  enable contrast remapping");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_CONTRAST_REMAP == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmContrast = button;
#if (! MDC_INCLUDE_DICM) && (! MDC_INCLUDE_ACR)
  gtk_widget_set_sensitive(button,FALSE);
#endif

  button = gtk_check_button_new_with_label("[r]  spacing is true gap/overlap");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_TRUE_GAP == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmTrueGap = button;
#if (MDC_INCLUDE_DICM || MDC_INCLUDE_ACR)
  gtk_widget_set_sensitive(button,TRUE);
#else
  gtk_widget_set_sensitive(button,FALSE);
#endif

  button = gtk_check_button_new_with_label("[w]  write implicit VR little");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_DICOM_WRITE_IMPLICIT == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmWriteImplicit = button;
#if (MDC_INCLUDE_DICM || MDC_INCLUDE_ACR)
  if (MDC_DICOM_WRITE_ENCAP_UNCOMP == MDC_YES) {
    gtk_widget_set_sensitive(button,FALSE);
  }else{
    gtk_widget_set_sensitive(button,TRUE);
  }
#else
  gtk_widget_set_sensitive(button,FALSE);
#endif

  button = gtk_check_button_new_with_label("[w]  write without meta header");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_DICOM_WRITE_NOMETA == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmWriteNoMeta = button;
#if (MDC_INCLUDE_DICM || MDC_INCLUDE_ACR)
  if (MDC_DICOM_WRITE_ENCAP_UNCOMP == MDC_YES) {
    gtk_widget_set_sensitive(button,FALSE);
  }else{
    gtk_widget_set_sensitive(button,TRUE);
  }
#else
  gtk_widget_set_sensitive(button,FALSE);
#endif

  /* ECAT 6.4 */
  frame = gtk_frame_new("Ecat 6");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,1,1,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_radio_button_new_with_label(NULL,
                               "[r]  planes sort order anatomical");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_ECAT6_SORT == MDC_ANATOMICAL)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.EcatSortAnatom = button;
#if ! MDC_INCLUDE_ECAT
  gtk_widget_set_sensitive(button,FALSE);
#endif

  group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
  button = gtk_radio_button_new_with_label(group,
                               "[r]  planes sort order by frame");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_ECAT6_SORT == MDC_BYFRAME)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.EcatSortByFrame = button;
#if ! MDC_INCLUDE_ECAT
  gtk_widget_set_sensitive(button,FALSE);
#endif

  /* InterFile */
  frame = gtk_frame_new("InterFile");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,0,0,1,1);
  gtk_widget_show(frame);

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_check_button_new_with_label("[r]  skip first preview slice");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_SKIP_PREVIEW == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.IntfSkip1 = button;
#if ! MDC_INCLUDE_INTF
  gtk_widget_set_sensitive(button,FALSE);
#endif

  button = gtk_check_button_new_with_label("[r]  ignore path in \'name of data file\' key");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_IGNORE_PATH == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.IntfNoPath = button;
#if ! MDC_INCLUDE_INTF
  gtk_widget_set_sensitive(button,FALSE);
#endif

  button = gtk_check_button_new_with_label("[w]  write into a single file");
  gtk_box_pack_start(GTK_BOX(box3),button,TRUE,TRUE,0);
  if (MDC_SINGLE_FILE == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.IntfSingleFile = button;
#if ! MDC_INCLUDE_INTF
  gtk_widget_set_sensitive(button,FALSE);
#endif
}

void XMdcOptionsMedconAddTabMosaic(GtkWidget *notebook)
{
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *grid;
  GtkWidget *grid2;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *label, *tablabel;
  GtkWidget *spinner;
  GtkAdjustment *adj;

  /* tab page Mosaic */
  box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
  gtk_widget_show(box2);

  tablabel = gtk_label_new("Mosaic");
  gtk_widget_show(tablabel);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

  grid = gtk_grid_new();
  gtk_container_add(GTK_CONTAINER(box2), grid);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
  gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
  gtk_widget_set_hexpand(grid, TRUE);
  gtk_widget_show(grid);

  /* Siemens - MOSAIC */

  label = gtk_label_new("\
* * * * * * * * * * * *\n\n\
see also DICOM options\n\n\
in the tab Formats\n\n\
* * * * * * * * * * * *");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_CENTER);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid),label,1,0,1,1);
  gtk_widget_show(label);

  frame = gtk_frame_new("Siemens - Mosaic");
  gtk_container_set_border_width(GTK_CONTAINER(frame),5);
  gtk_grid_attach(GTK_GRID(grid),frame,0,0,1,1);
  gtk_widget_show(frame);
#if (! MDC_INCLUDE_DICM) && (! MDC_INCLUDE_ACR)
  gtk_widget_set_sensitive(frame,FALSE);
#endif

  box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_container_add(GTK_CONTAINER(frame),box3);
  gtk_container_set_border_width(GTK_CONTAINER(box3),5);
  gtk_widget_show(box3);

  button = gtk_check_button_new_with_label("[r]  enable mosaic support");
  gtk_box_pack_start(GTK_BOX(box3),button,FALSE,FALSE,0);
  g_signal_connect(button, "toggled",
                     G_CALLBACK(XMdcToggleSensitivityMosaic),NULL);
  if (MDC_DICOM_MOSAIC_ENABLED == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmMosaicEnabled = button;

  box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
  gtk_box_pack_start(GTK_BOX(box3),box4,FALSE,FALSE,5);
  gtk_widget_show(box4);
  wmosaic = box4;

  button = gtk_check_button_new_with_label("[r]  force specified stamps layout");
  gtk_box_pack_start(GTK_BOX(box4),button,FALSE,FALSE,5);
  g_signal_connect(button, "toggled",
                     G_CALLBACK(XMdcToggleSensitivityForced),NULL);
  if (MDC_DICOM_MOSAIC_FORCED == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmMosaicForced = button;

  grid2 = gtk_grid_new();
  gtk_box_pack_start(GTK_BOX(box4),grid2,TRUE,TRUE,5);
  gtk_grid_set_row_spacing(GTK_GRID(grid2), 5);
  gtk_grid_set_column_spacing(GTK_GRID(grid2), 5);
  gtk_grid_set_row_homogeneous(GTK_GRID(grid2), TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid2), TRUE);
  gtk_widget_show(grid2);
  wforced = grid2;

  label = gtk_label_new("width (X)");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,0,0,1,1);
  gtk_widget_show(label);

  adj=(GtkAdjustment *)gtk_adjustment_new(0., 0., 9999., 1., 4., 0.);
  spinner = gtk_spin_button_new(adj, 0.0, 0);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_spin_button_set_snap_to_ticks (GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_grid_attach(GTK_GRID(grid2),spinner,1,0,1,1);
  gtk_widget_show(spinner);
  sOptionsMedCon.DicmMosaicWidth=spinner;
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinner),(gfloat)mdc_mosaic_width);

  label = gtk_label_new("[pixels]");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,2,0,1,1);
  gtk_widget_show(label);

  label = gtk_label_new("height (Y)");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,0,1,1,1);
  gtk_widget_show(label);

  adj=(GtkAdjustment *)gtk_adjustment_new(0., 0., 9999., 1., 4., 0.);
  spinner = gtk_spin_button_new(adj, 0.0, 0);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_spin_button_set_snap_to_ticks (GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_grid_attach(GTK_GRID(grid2),spinner,1,1,1,1);
  gtk_widget_show(spinner);
  sOptionsMedCon.DicmMosaicHeight=spinner;
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinner),(gfloat)mdc_mosaic_height);

  label = gtk_label_new("[pixels]");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,2,1,1,1);
  gtk_widget_show(label);

  label = gtk_label_new("number (Z)");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,0,2,1,1);
  gtk_widget_show(label);

  adj=(GtkAdjustment *)gtk_adjustment_new(0., 0., 9999., 1., 4., 0.);
  spinner = gtk_spin_button_new(adj, 0.0, 0);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_spin_button_set_snap_to_ticks (GTK_SPIN_BUTTON(spinner), TRUE);
  gtk_grid_attach(GTK_GRID(grid2),spinner,1,2,1,1);
  gtk_widget_show(spinner);
  sOptionsMedCon.DicmMosaicNumber=spinner;
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinner),(gfloat)mdc_mosaic_number);

  label = gtk_label_new("[slices]");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,2,2,1,1);
  gtk_widget_show(label);

  label = gtk_label_new("interlaced slices");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,0,3,1,1);
  gtk_widget_show(label);

  button = gtk_check_button_new();
  gtk_grid_attach(GTK_GRID(grid2),button,1,3,1,1);
  if (MDC_DICOM_MOSAIC_DO_INTERL == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmMosaicDoInterl = button;

  label = gtk_label_new("fix voxel sizes");
  gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
  gtk_widget_set_name(label, "FixedLabel");
  gtk_grid_attach(GTK_GRID(grid2),label,0,4,1,1);
  gtk_widget_show(label);

  button = gtk_check_button_new();
  gtk_grid_attach(GTK_GRID(grid2),button,1,4,1,1);
  if (MDC_DICOM_MOSAIC_FIX_VOXEL == MDC_YES)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
  gtk_widget_show(button);
  sOptionsMedCon.DicmMosaicFixVoxel = button;

  XMdcInitMosaicFrame();

}

void XMdcOptionsMedconSel(GtkWidget *widget, gpointer data)
{
   GtkWidget *box1;
   GtkWidget *box2;
   GtkWidget *button;
   GtkWidget *separator;
   GtkWidget *notebook;

   if (woption == NULL) {

     woption = gtk_window_new(GTK_WINDOW_TOPLEVEL);

     g_signal_connect(woption, "destroy",
                        G_CALLBACK(XMdcMedconQuit),NULL);
     g_signal_connect(woption, "delete_event",
                        G_CALLBACK(XMdcHandlerToHide), NULL);

     gtk_window_set_title(GTK_WINDOW(woption),"MedCon Options");
     gtk_container_set_border_width(GTK_CONTAINER(woption),0);

     box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
     gtk_widget_show(box1);
     gtk_container_add(GTK_CONTAINER(woption),box1);

     wlabel = gtk_label_new(
            " \n** Reread file for applying [r] or [rw] changes **\n ");
     gtk_widget_set_name(wlabel, "WarningLabel");
     gtk_widget_set_halign(wlabel, GTK_ALIGN_CENTER);
     gtk_widget_set_valign(wlabel, GTK_ALIGN_CENTER);
     gtk_box_pack_start(GTK_BOX(box1),wlabel,TRUE,TRUE,0);
     if (XMDC_FILE_OPEN == MDC_YES) {
       gtk_widget_show(wlabel);
     }else {
       gtk_widget_hide(wlabel);
     }

     notebook = gtk_notebook_new();
     gtk_container_add(GTK_CONTAINER(box1),notebook);
     gtk_container_set_border_width(GTK_CONTAINER(notebook), 10);
     gtk_widget_show(notebook);

     XMdcOptionsMedconAddTabPixels(notebook);

     XMdcOptionsMedconAddTabFiles(notebook);

     XMdcOptionsMedconAddTabSlices(notebook);

     XMdcOptionsMedconAddTabFormats(notebook);

     XMdcOptionsMedconAddTabMosaic(notebook);

     /* separator */
     separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
     gtk_box_pack_start(GTK_BOX(box1),separator,FALSE,FALSE,0);
     gtk_widget_show(separator);

     /* create bottom button box */
     box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
     gtk_box_pack_start(GTK_BOX(box1),box2,TRUE,TRUE,2);
     gtk_widget_show(box2);

     button = gtk_button_new_with_label("Apply");
     gtk_box_pack_start(GTK_BOX(box2),button,TRUE,TRUE,2);
     g_signal_connect_object(button, "clicked",
                        G_CALLBACK(gtk_widget_hide), woption, G_CONNECT_SWAPPED);
     g_signal_connect(button, "clicked",
                        G_CALLBACK(XMdcOptionsMedconCallbackApply), NULL);
     gtk_widget_show(button);

     button = gtk_button_new_with_label("Cancel");
     gtk_box_pack_start(GTK_BOX(box2),button,TRUE,TRUE,2);
     g_signal_connect_object(button, "clicked",
         G_CALLBACK(gtk_widget_hide), woption, G_CONNECT_SWAPPED);
     gtk_widget_show(button);

  }else{
     /* set buttons to appropriate state */
     GtkWidget *b1, *b2, *b3, *b4, *e1;

     /* show or hide warn label */
     if (XMDC_FILE_OPEN == MDC_YES) {
       gtk_widget_show(wlabel);
     }else {
       gtk_widget_hide(wlabel);
     }

     gtk_widget_hide(woption);

     b1 = sOptionsMedCon.PixNoQuant;
     b2 = sOptionsMedCon.PixQuantify;
     b3 = sOptionsMedCon.PixCalibrate;
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);
     if (MDC_QUANTIFY == MDC_NO && MDC_CALIBRATE == MDC_NO) {
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     }else if (MDC_QUANTIFY == MDC_YES) {
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
     }else if (MDC_CALIBRATE == MDC_YES) {
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
     }

     b1 = sOptionsMedCon.PixNegatives;
     b2 = sOptionsMedCon.PixPositives;
     if (MDC_NEGATIVE == MDC_YES) {
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     }else{
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
     }

     b1 = sOptionsMedCon.NormOverFrames;
     b2 = sOptionsMedCon.NormOverAll;
     if (MDC_NORM_OVER_FRAMES == MDC_YES) {
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     }else{
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
     }

     b1 = sOptionsMedCon.FallbackNONE;
     b2 = sOptionsMedCon.FallbackANLZ;
     b3 = sOptionsMedCon.FallbackDICM;
     b4 = sOptionsMedCon.FallbackECAT;
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4), FALSE);
     switch (MDC_FALLBACK_FRMT) {
       case MDC_FRMT_NONE:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
           break;
       case MDC_FRMT_ANLZ:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
           break;
       case MDC_FRMT_DICM:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
           break;
       case MDC_FRMT_ECAT6:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4), TRUE);
           break;
     }

     /* Split Output */
     b1 = sOptionsMedCon.SplitNone;
     b2 = sOptionsMedCon.SplitFrames;
     b3 = sOptionsMedCon.SplitSlices;
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);
     switch (MDC_FILE_SPLIT) {
       case MDC_SPLIT_NONE:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
           break;
       case MDC_SPLIT_PER_FRAME:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
           break;
       case MDC_SPLIT_PER_SLICE:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
           break;
     }

     /* Color */
     b1 = sOptionsMedCon.ColorModeIndexed;
     if (MDC_COLOR_MODE == MDC_COLOR_INDEXED)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.ColorDither;
     if (MDC_DITHER_COLOR == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.ColorMakeGray;
     if (MDC_MAKE_GRAY == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     /* Padding */
     b1 = sOptionsMedCon.PadAround;
     b2 = sOptionsMedCon.PadTopLeft;
     b3 = sOptionsMedCon.PadBottomRight;

     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);

     switch (MDC_PADDING_MODE) {
       case MDC_PAD_AROUND:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
           break;
       case MDC_PAD_TOP_LEFT:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
           break;
       case MDC_PAD_BOTTOM_RIGHT:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
           break;
     }

     /* Output File Names */
     b1 = sOptionsMedCon.NameAlias;
     if (MDC_ALIAS_NAME == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.NameNoPrefix;
     if (MDC_PREFIX_DISABLED == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     /* DICOM */
     b1 = sOptionsMedCon.DicmContrast;
     if (MDC_CONTRAST_REMAP == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.DicmTrueGap;
     if (MDC_TRUE_GAP == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.DicmWriteImplicit;
     if (MDC_DICOM_WRITE_IMPLICIT == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.DicmWriteNoMeta;
     if (MDC_DICOM_WRITE_NOMETA == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     /* Siemens - MOSAIC */
     b1 = sOptionsMedCon.DicmMosaicEnabled;
     if (MDC_DICOM_MOSAIC_ENABLED == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.DicmMosaicForced;
     if (MDC_DICOM_MOSAIC_FORCED == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     e1 = sOptionsMedCon.DicmMosaicWidth;
     gtk_spin_button_set_value(GTK_SPIN_BUTTON(e1),(gfloat)mdc_mosaic_width);

     e1 = sOptionsMedCon.DicmMosaicHeight;
     gtk_spin_button_set_value(GTK_SPIN_BUTTON(e1),(gfloat)mdc_mosaic_height);

     e1 = sOptionsMedCon.DicmMosaicNumber;
     gtk_spin_button_set_value(GTK_SPIN_BUTTON(e1),(gfloat)mdc_mosaic_number);

     b1 = sOptionsMedCon.DicmMosaicDoInterl;
     if (MDC_DICOM_MOSAIC_DO_INTERL == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.DicmMosaicFixVoxel;
     if (MDC_DICOM_MOSAIC_FIX_VOXEL == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     XMdcInitMosaicFrame();

     /* Pixel Types */
     b1 = sOptionsMedCon.PixTypeNONE;
     b2 = sOptionsMedCon.PixTypeBIT8_U;
     b3 = sOptionsMedCon.PixTypeBIT16_S;
     b4 = sOptionsMedCon.BitsUsed12;
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4), FALSE);
     switch (MDC_FORCE_INT) {
       case MDC_NO:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
           XMdcUnsensitiveBitsUsed12(NULL,NULL);
           break;
       case BIT8_U:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
           XMdcUnsensitiveBitsUsed12(NULL,NULL);
           break;
       case BIT16_S:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
           XMdcSensitiveBitsUsed12(NULL,NULL);
           break;
     }
     if (MDC_INT16_BITS_USED == 12)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4), TRUE);


     /* File Endian Type */
     b1 = sOptionsMedCon.FileTypeLITTLE;
     b2 = sOptionsMedCon.FileTypeBIG;
     if (MDC_WRITE_ENDIAN == MDC_LITTLE_ENDIAN) {
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     }else{
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
     }

     /* Flipping - Sorting */
     b1 = sOptionsMedCon.FlipHoriz;
     if (MDC_FLIP_HORIZONTAL == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.FlipVert;
     if (MDC_FLIP_VERTICAL == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.SortReverse;
     if (MDC_SORT_REVERSE== MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     XMdcInitCineButtons();

     /* Matrix */
     b1 = sOptionsMedCon.MakeSqrNo;
     b2 = sOptionsMedCon.MakeSqr1;
     b3 = sOptionsMedCon.MakeSqr2;
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);
     switch (MDC_MAKE_SQUARE) {
       case MDC_NO:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
           break;
       case MDC_TRANSF_SQR1:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
           break;
       case MDC_TRANSF_SQR2:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
           break;
       default:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     }

     /* Analyze */
     b1 = sOptionsMedCon.AnlzSPM;
     if (MDC_ANLZ_SPM == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     /* InterFile */
     b1 = sOptionsMedCon.IntfSkip1;
     if (MDC_SKIP_PREVIEW == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.IntfNoPath;
     if (MDC_IGNORE_PATH == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);

     b1 = sOptionsMedCon.IntfSingleFile;
     if (MDC_SINGLE_FILE == MDC_YES)
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
     else
       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);


     /* ECAT 6 */
     b1 = sOptionsMedCon.EcatSortAnatom;
     b2 = sOptionsMedCon.EcatSortByFrame;
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
     switch (MDC_ECAT6_SORT) {
       case MDC_ANATOMICAL:
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE); break;
       case MDC_BYFRAME   :
           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE); break;
     }

  }

  XMdcShowWidget(woption);

}

void XMdcOptionsRenderSel(GtkWidget *widget, gpointer data)
{
  XMdcRenderingSel();
}

void XMdcOptionsResizeSel(GtkWidget *widget, gpointer data)
{
  XMdcResizeSel();
}

void XMdcOptionsColorMapSel(GtkWidget *widget, gpointer data)
{
  XMdcColorMapSel();
}

void XMdcOptionsLabelSel(GtkWidget *widget, gpointer data)
{
  XMdcLabelSel();
}

void XMdcOptionsPagesSel(GtkWidget *widget, gpointer data)
{
  XMdcPagesSel();
}

void XMdcSensitiveBitsUsed12(GtkWidget *widget, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(sOptionsMedCon.BitsUsed12),TRUE);
}

void XMdcUnsensitiveBitsUsed12(GtkWidget *widget, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(sOptionsMedCon.BitsUsed12),FALSE);
}

void XMdcToggleSensitivityMosaic(GtkWidget *widget, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(wmosaic),
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)));
}

void XMdcToggleSensitivityForced(GtkWidget *widget, gpointer data)
{
    gtk_widget_set_sensitive(GTK_WIDGET(wforced),
        gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)));
}

void XMdcInitMosaicFrame(void)
{
  GtkWidget *b1;

  /* active status */
  b1 = sOptionsMedCon.DicmMosaicEnabled;
  if (MDC_DICOM_MOSAIC_ENABLED == MDC_YES) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
  }else{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
  }

  b1 = sOptionsMedCon.DicmMosaicDoInterl;
  if (MDC_DICOM_MOSAIC_DO_INTERL == MDC_YES) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
  }else{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
  }

  /* sensitivity status */
  if (MDC_DICOM_MOSAIC_ENABLED == MDC_YES) {
    gtk_widget_set_sensitive(wmosaic,TRUE);
  }else{
    gtk_widget_set_sensitive(wmosaic,FALSE);
  }

  if (MDC_DICOM_MOSAIC_FORCED == MDC_YES) {
    gtk_widget_set_sensitive(wforced,TRUE);
  }else{
    gtk_widget_set_sensitive(wforced,FALSE);
  }

}


void XMdcToggleSensitivityCine(GtkWidget *widget, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(sOptionsMedCon.SortCineApply),
       gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)));
  gtk_widget_set_sensitive(GTK_WIDGET(sOptionsMedCon.SortCineUndo),
       gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)));
}

void XMdcInitCineButtons(void)
{
  GtkWidget *b1, *b2, *b3;

  b1 = sOptionsMedCon.SortCine;
  b2 = sOptionsMedCon.SortCineApply;
  b3 = sOptionsMedCon.SortCineUndo;

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), FALSE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), FALSE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), FALSE);

  /* active status */
  if (MDC_SORT_CINE_APPLY == MDC_YES) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2), TRUE);
  }else if (MDC_SORT_CINE_UNDO == MDC_YES) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3), TRUE);
  }

  /* sensitivity status */
  if (MDC_SORT_CINE_APPLY == MDC_YES || MDC_SORT_CINE_UNDO == MDC_YES) {
    gtk_widget_set_sensitive(sOptionsMedCon.SortCineApply,TRUE);
    gtk_widget_set_sensitive(sOptionsMedCon.SortCineUndo,TRUE);
  }else{
    gtk_widget_set_sensitive(sOptionsMedCon.SortCineApply,FALSE);
    gtk_widget_set_sensitive(sOptionsMedCon.SortCineUndo,FALSE);
  }

}

