/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xresize.c                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : resize routines                                          *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcResize()                 - Resize the dimension      *
 *                XMdcResizeSelCallbackApply() - Resize Sel Apply callback *
 *                XMdcResizeSel()              - Select the intial resize  *
 *                XMdcResizeNeeded()           - Resize when image>screen? *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

static GtkWidget *wresize=NULL;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

Uint32 XMdcResize(Uint32 dim)
{
  Uint32 newdim = dim;

  if (my.RESIZE == MDC_NO) return(newdim);

  /* note #1: when divide, integer round up => no pixel loss */
  /* note #2: defined division constants are negative        */

  switch (sResizeSelection.CurType) {
   case XMDC_RESIZE_ORIGINAL:
       break;
   case XMDC_RESIZE_FIFTH   :
       newdim -= (XMDC_RESIZE_FIFTH  + 1); newdim /= -XMDC_RESIZE_FIFTH;
       break;
   case XMDC_RESIZE_FOURTH  :
       newdim -= (XMDC_RESIZE_FOURTH + 1); newdim /= -XMDC_RESIZE_FOURTH;
       break;
   case XMDC_RESIZE_THIRD   :
       newdim -= (XMDC_RESIZE_THIRD  + 1); newdim /= -XMDC_RESIZE_THIRD;
       break;
   case XMDC_RESIZE_HALF    :
       newdim -= (XMDC_RESIZE_HALF   + 1); newdim /= -XMDC_RESIZE_HALF;
       break;
   case XMDC_RESIZE_DOUBLE  :
       newdim *=  XMDC_RESIZE_DOUBLE;
       break;
   case XMDC_RESIZE_TRIPLE  :
       newdim *=  XMDC_RESIZE_TRIPLE;
       break;
   case XMDC_RESIZE_QUAD    :
       newdim *=  XMDC_RESIZE_QUAD;
       break;
   case XMDC_RESIZE_QUINT   :
       newdim *=  XMDC_RESIZE_QUINT;
       break;
  }

  return(newdim);
}

void XMdcResizeSelCallbackApply(GtkWidget *widget, gpointer data)
{
  Int8 type=XMDC_RESIZE_ORIGINAL;

  MdcDebugPrint("initial resize: ");
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Fifth)))          {
    MdcDebugPrint("\t1:5"); type = XMDC_RESIZE_FIFTH;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Fourth)))   {
    MdcDebugPrint("\t1:4"); type = XMDC_RESIZE_FOURTH;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Third)))    {
    MdcDebugPrint("\t1:3"); type = XMDC_RESIZE_THIRD;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Half)))     {
    MdcDebugPrint("\t1:2"); type = XMDC_RESIZE_HALF;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Original))) {
    MdcDebugPrint("\t1:1"); type = XMDC_RESIZE_ORIGINAL;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Double)))   {
    MdcDebugPrint("\t2:1"); type = XMDC_RESIZE_DOUBLE;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Triple)))   {
    MdcDebugPrint("\t3:1"); type = XMDC_RESIZE_TRIPLE;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Quad)))     {
    MdcDebugPrint("\t4:1"); type = XMDC_RESIZE_QUAD;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sResizeSelection.Quint)))    {
    MdcDebugPrint("\t5:1"); type = XMDC_RESIZE_QUINT;
  }

  if (type != sResizeSelection.CurType) {
    sResizeSelection.CurType = type;
    if (XMDC_FILE_OPEN == MDC_YES) {
      XMdcProgressBar(MDC_PROGRESS_BEGIN,0.,"Resizing images:");
      XMdcViewerHide();
      XMdcViewerEnableAutoShrink();
      XMdcViewerReset();
      XMdcDisplayImages();
      XMdcProgressBar(MDC_PROGRESS_END,0.,NULL);
    }
  }else{
      XMdcViewerShow();
  }
}

void XMdcResizeSel(void)
{

  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *separator;
  GSList *group;

  if (wresize == NULL) {

    wresize = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    g_signal_connect(wresize, "destroy",
                       G_CALLBACK(XMdcMedconQuit),NULL);
    g_signal_connect(wresize, "delete_event",
                       G_CALLBACK(XMdcHandlerToHide),NULL);

    gtk_window_set_title(GTK_WINDOW(wresize),"Resize Selection");

    gtk_container_set_border_width (GTK_CONTAINER (wresize), 0);

    box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add (GTK_CONTAINER (wresize), box1);
    gtk_widget_show(box1);

    /* create upper box - Initial Resize */
    box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER(box2), 5);
    gtk_widget_show(box2);

    box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_pack_start(GTK_BOX(box2), box3, TRUE, TRUE, 0);
    gtk_widget_show(box3);

    frame = gtk_frame_new("Initial Resize");
    gtk_box_pack_start(GTK_BOX (box3), frame, TRUE, TRUE, 0);
    gtk_widget_show(frame);

    box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(frame), box4);
    gtk_container_set_border_width(GTK_CONTAINER(box4), 5);
    gtk_widget_show(box4);

    button = gtk_radio_button_new_with_label(NULL, "[1:5] fifth");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_FIFTH)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Fifth = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[1:4] fourth");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_FOURTH)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Fourth = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[1:3] third");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_THIRD)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Third = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[1:2] half");
    gtk_box_pack_start(GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_HALF)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Half = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[1:1] original");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 5);
    if (sResizeSelection.CurType == XMDC_RESIZE_ORIGINAL)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Original = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[2:1] double");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_DOUBLE)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Double = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[3:1] triple");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_TRIPLE)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Triple = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[4:1] quad");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_QUAD)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Quad = button;

    group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label(group, "[5:1] quint");
    gtk_box_pack_start (GTK_BOX(box4), button, TRUE, TRUE, 0);
    if (sResizeSelection.CurType == XMDC_RESIZE_QUINT)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(button), TRUE);
    gtk_widget_show(button);
    sResizeSelection.Quint = button;

    /* create horizontal separator */
    separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start (GTK_BOX (box1), separator, FALSE, FALSE, 0);
    gtk_widget_show (separator);

    /* create bottom button box */
    box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(box1), box2, TRUE, TRUE, 2);
    gtk_widget_show(box2);

    button = gtk_button_new_with_label("Apply");
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
    g_signal_connect_swapped(button, "clicked",
                       G_CALLBACK(gtk_widget_hide), wresize);
    g_signal_connect(button, "clicked",
                       G_CALLBACK(XMdcResizeSelCallbackApply), NULL);
    gtk_widget_show(button);

    button = gtk_button_new_with_label ("Cancel");
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
    g_signal_connect_swapped(button, "clicked",
        G_CALLBACK(gtk_widget_hide), wresize);
    gtk_widget_show(button);

  }else{
    /* set buttons to appropriate state */
    GtkWidget *b1, *b2, *b3, *b4, *b5, *b6, *b7, *b8, *b9;

    gtk_widget_hide(wresize);

    b1 = sResizeSelection.Fifth;
    b2 = sResizeSelection.Fourth;
    b3 = sResizeSelection.Third;
    b4 = sResizeSelection.Half;
    b5 = sResizeSelection.Original;
    b6 = sResizeSelection.Double;
    b7 = sResizeSelection.Triple;
    b8 = sResizeSelection.Quad;
    b9 = sResizeSelection.Quint;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b5),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b6),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b7),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b8),FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b9),FALSE);
    switch (sResizeSelection.CurType) {
      case XMDC_RESIZE_FIFTH   :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1),TRUE); break;
      case XMDC_RESIZE_FOURTH  :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b2),TRUE); break;
      case XMDC_RESIZE_THIRD   :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b3),TRUE); break;
      case XMDC_RESIZE_HALF    :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b4),TRUE); break;
      case XMDC_RESIZE_ORIGINAL:
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b5),TRUE); break;
      case XMDC_RESIZE_DOUBLE  :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b6),TRUE); break;
      case XMDC_RESIZE_TRIPLE  :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b7),TRUE); break;
      case XMDC_RESIZE_QUAD    :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b8),TRUE); break;
      case XMDC_RESIZE_QUINT   :
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b9),TRUE); break;
    }

  }

  XMdcShowWidget(wresize);
}

void XMdcResizeNeeded(void)
{
  if (XMdcScaleW(my.fi->mwidth)  >= (my.screen_width  - XMDC_FREE_BORDER) ||
      XMdcScaleH(my.fi->mheight) >= (my.screen_height - XMDC_FREE_BORDER)) {

    XMdcAskYesNo( G_CALLBACK(XMdcViewerShow),
                  G_CALLBACK(XMdcResizeSel),
                  "Images too big for screen. Show images anyway?");

  }else{

    XMdcViewerShow();

  }

}

