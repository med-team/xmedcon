/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xdefs.c                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : global defines                                           *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <stdio.h>

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

Uint8 XMDC_FILE_OPEN=MDC_NO;
Uint8 XMDC_FILE_TYPE=XMDC_NORMAL;

Uint8 XMDC_IMAGE_BORDER = 1; /* border between images in viewer window */

Uint8 XMDC_MENUBAR_PACK = GTK_PACK_DIRECTION_LTR; /* _LTR = Left-to-Right 
                                                     _RTL = Right-to-Left
                                                     _TTB = Top-to-Bottom
                                                     _BTT = Bottom-to-Top */

GdkCursor *handcursor;
GdkCursor *zoomcursor;

MyMainStruct my;
OptionsMedConStruct sOptionsMedCon;
ColormapSelectionStruct sColormapSelection;
MapPlaceSelectionStruct sMapPlaceSelection;
LabelSelectionStruct sLabelSelection;
RenderSelectionStruct sRenderSelection;
ExtractSelectionStruct sExtractSelection;
RawReadSelectionStruct sRawReadSelection;
ResizeSelectionStruct  sResizeSelection;
PagesSelectionStruct   sPagesSelection;
ColGbcCorrectStruct    sGbc;
EditFileInfoStruct     sEditFI;

char   labelindex[50];
char   labeltimes[50];
Uint32 write_counter=0;
char   xmdcstr[MDC_2KB_OFFSET];
char  *XMEDCONLUT=NULL; /* environ var: dir to color lookup tables    */
char  *XMEDCONRPI=NULL; /* environ var: dir to raw predef input files */

