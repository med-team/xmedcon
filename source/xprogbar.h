/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xprogbar.h                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xprogbar.c header file                                   *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XPROGBAR_H__
#define __XPROGBAR_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcProgressBar(int type, float value, char *label);
void XMdcUpdateDrawing(void);
void XMdcUpdateProgressBar(void);
void XMdcSetProgressBar(float set);
void XMdcIncrProgressBar(float incr);
void XMdcCreateProgressBar(char *labelstring);
void XMdcBeginProgressBar(char *labelstring);
void XMdcEndProgressBar(void);
char *XMdcHandleBarLabel(char *labelstring);

#endif

