/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-conc.h                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-conc.c header file                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Author       : Andy Loening                                             *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_CONC_H__
#define __M_CONC_H__

/****************************************************************************
                              H E A D E R S
****************************************************************************/



/****************************************************************************
                              D E F I N E S
****************************************************************************/

/* Acquisition scanner modality (integer) */
typedef enum {
  MDC_CONC_MODALITY_UNKNOWN = -1,
  MDC_CONC_MODALITY_PET = 0,
  MDC_CONC_MODALITY_CT = 1,
  MDC_CONC_MODALITY_SPECT = 2,
  MDC_CONC_MODALITY_LAST = 3, /* place holder, as list begins at -1 */
  MDC_CONC_NUM_MODALITIES = 4
} MdcConcModalityTypes;

/* siemens/concorde data file types (integer) */
typedef enum {
  MDC_CONC_FILE_UNKNOWN,
  MDC_CONC_FILE_LIST_MODE,
  MDC_CONC_FILE_SINOGRAM,
  MDC_CONC_FILE_NORMALIZATION,
  MDC_CONC_FILE_ATTENUATION,
  MDC_CONC_FILE_IMAGE,
  MDC_CONC_FILE_BLANK,
  MDC_CONC_FILE_RESERVED, /* 7 is skipped for some reason */
  MDC_CONC_FILE_MU_MAP,
  MDC_CONC_FILE_SCATTER_CORRECTION,
  MDC_CONC_FILE_CRYSTAL_EFFICIENCY_DATA,
  MDC_CONC_FILE_CRYSTAL_INTERFERENCE_CORRECTION,
  MDC_CONC_FILE_TRANSAXIAL_GEOMETRIC_CORRECTION,
  MDC_CONC_FILE_AXIAL_GEOMETRIC_CORRECTION,
  MDC_CONC_FILE_CT_PROJECTION_DATA,
  MDC_CONC_FILE_SPECT_RAW_PROJECTION_DATA,
  MDC_CONC_FILE_SPECT_ENERGY_PROJECTION_DATA,
  MDC_CONC_FILE_SPECT_NORMALIZATION_DATA,
  MDC_CONC_NUM_FILE_TYPES
} MdcConcFileTypes;

/* siemens/concorde acquisition modes (integer) */
typedef enum {
  MDC_CONC_ACQ_UNKNOWN,                    /* Unknown acquisition mode */
  MDC_CONC_ACQ_BLANK,                      /* Blank acquisition */
  MDC_CONC_ACQ_EMISSION,                   /* Emission acquisition */
  MDC_CONC_ACQ_DYNAMIC,                    /* Dynamic acquisition */
  MDC_CONC_ACQ_GATED,                      /* Gated acquisition */
  MDC_CONC_ACQ_CONTINUOUS,                 /* Continuous bed motion acquisition */
  MDC_CONC_ACQ_SINGLES,                    /* Singles transmission acquisition */
  MDC_CONC_ACQ_WINDOWED_COINCIDENCE,       /* Windowed coincidence transmission acquisition */
  MDC_CONC_ACQ_NON_WINDOWED_COINCIDENCE,   /* Non-windowed coincidence transmission acquisition */
  MDC_CONC_ACQ_CT_PROJECTION,              /* CT projection acquisition */
  MDC_CONC_ACQ_CT_CALIBRATION,             /* CT calibration acquisition */
  MDC_CONC_ACQ_SPECT_PLANAR_PROJECTION,    /* SPECT planar projection acquisition */
  MDC_CONC_ACQ_SPECT_MULTIPROJECTION,      /* SPECT multi-projection acquisition */
  MDC_CONC_ACQ_SPECT_CALIBRATION,          /* SPECT calibration acquisition */
  MDC_CONC_ACQ_SPECT_NORMALIZATION,        /* SPECT normalization acquisition */
  MDC_CONC_ACQ_SPECT_DETECTOR_SETUP,       /* SPECT detector setup acquisition */
  MDC_CONC_ACQ_SPECT_SCOUT_VIEW,            /* SPECT scout view acquisition */
  MDC_CONC_NUM_ACQ_MODES
} MdcConcAcqModes;

typedef enum {
  MDC_CONC_BED_MOTION_STATIC,              /* Static or unknown bed motion */
  MDC_CONC_BED_MOTION_CONTINOUS,           /* Continuous bed motion */
  MDC_CONC_BED_MOTION_MULTIPLE,            /* Multiple bed positions, i.e. step and shoot */
  MDC_CONC_NUM_BED_MOTIONS
} MdcConcBedMotion;

typedef enum {
  MDC_CONC_TX_SRC_UNKNOWN,                 /* Unknown transmission source type */
  MDC_CONC_TX_SRC_POINT,                   /* Transmission point source */
  MDC_CONC_TX_SRC_LINE,                    /* transmission line source */
  MDC_CONC_NUM_TX_SRC_TYPES
} MdcConcTXSrcTypes;

typedef enum {
  MDC_CONC_DATA_UNKNOWN,      /* Unknown data type */
  MDC_CONC_DATA_SBYTE,        /* Signed 8 bit */
  MDC_CONC_DATA_SSHORT_LE,    /* Signed 16 bit int, little endian */
  MDC_CONC_DATA_SINT_LE,      /* Signed 32 bit int, little endian */
  MDC_CONC_DATA_FLOAT_LE,     /* IEEE Float (32bit), little endian */
  MDC_CONC_DATA_FLOAT_BE,     /* IEEE Float (32bit), big endian */
  MDC_CONC_DATA_SSHORT_BE,    /* Signed 16 bit int, big endian */
  MDC_CONC_DATA_SINT_BE,      /* Signed 32 bit int, big endian */
  MDC_CONC_NUM_DATA_TYPES
} MdcConcDataTypes;

typedef enum {
  MDC_CONC_ORDER_VIEW,        /* Element/Axis/View/Ring_diff - view mode */
  MDC_CONC_ORDER_SINOGRAM,    /* Element/View/Axis/Ring_Diff - sinogram mode */
  MDC_CONC_NUM_ORDER_MODES
} MdcConcOrderModes;

typedef enum {
  MDC_CONC_REBIN_UNKNOWN,     /* Unknown, or no, algorithm type */
  MDC_CONC_REBIN_FULL,        /* Full 3D binning (span and ring difference) */
  MDC_CONC_REBIN_SINGLE,      /* Single-Slice Rebinning */
  MDC_CONC_REBIN_FOURIER,     /* Fourier Rebinning */
  MDC_CONC_NUM_REBIN_TYPES
} MdcConcRebinTypes;

typedef enum {
  MDC_CONC_RECON_UNKNOWN,     /* Unknown, or no, algorithm type */
  MDC_CONC_RECON_FBP,         /* Filtered Backprojection */
  MDC_CONC_RECON_OSEM2D,      /* OSEM2D */
  MDC_CONC_RECON_OSEM3D,      /* OSEM3D */
  MDC_CONC_RECON_UNUSED4,     /* unused */
  MDC_CONC_RECON_UNUSED5,     /* unused */
  MDC_CONC_RECON_OSEM3D_MAP,  /* OSEM3D followed by MAP or FastMAP */
  MDC_CONC_RECON_MAPTR,       /* MAPTR for transmission image */
  MDC_CONC_RECON_FELDKAMP,    /* Feldkamp cone beam */
  MDC_CONC_NUM_RECON_TYPES
} MdcConcReconTypes;

typedef enum {
  MDC_CONC_OSEM2D_UNWEIGHTED,   /* Unweighted osem2d reconstruction */
  MDC_CONC_OSEM2D_ATTENUATION,  /* Attenuation weighted osem2d reconstruction */
  MDC_CONC_NUM_OSEM2D_TYPES,
  MDC_CONC_OSEM2D_UNKNOWN
} MdcConcOSEM2DTypes;

/* deadtime correction applied to the data set */
typedef enum {
  MDC_CONC_DEAD_CORR_NONE,           /* No deadtime correction applied */
  MDC_CONC_DEAD_CORR_GLOBAL,         /* Global estimate based on singles */
  MDC_CONC_DEAD_CORR_CMS,            /* CMS estimate based on singles */
  MDC_CONC_DEAD_CORR_GLOBAL_RUNNING, /* Global estimate based on running average */
  MDC_CONC_DEAD_CORR_BLOCK,          /* Blank/TX singles estimate (block based) */
  MDC_CONC_NUM_DEAD_CORR_TYPES
} MdcConcDeadCorrTypes;

typedef enum {
  MDC_CONC_ATTN_CORR_NONE,                        /* No attenuation applied */
  MDC_CONC_ATTN_CORR_PT_WINDOWED_COINCIDENCE,     /* Point source in windowed TX coincidence */
  MDC_CONC_ATTN_CORR_PT_SINGLES,                  /* Point source singles based TX */
  MDC_CONC_ATTN_CORR_SEG_COINCIDENCE,             /* Segmented point src in TX coincidence*/
  MDC_CONC_ATTN_CORR_SEG_SINGLES,                 /* Segmented point src singles based TX */
  MDC_CONC_ATTN_CORR_GEOMETRY,                    /* Calculated by geometry */
  MDC_CONC_ATTN_CORR_NON_BETA_SINGLES,            /* Non-positron source singles based TX */
  MDC_CONC_ATTN_CORR_PT_NON_WINDOWED_COINCIDENCE, /* Point source in non-windowed TX coincidence */
  MDC_CONC_NUM_ATTN_CORR_TYPES
} MdcConcAttnCorrTypes;

typedef enum {
  MDC_CONC_SCATTER_CORR_NONE,            /* No scatter correction applied */
  MDC_CONC_SCATTER_CORR_TAILS,           /* Fit of emission tail */
  MDC_CONC_SCATTER_CORR_MONTE_CARLO,     /* Monte Carlo of emission and transmission data */
  MDC_CONC_SCATTER_CORR_DIRECT,          /* Direct calculation from analytical formulas */
  MDC_CONC_SCATTER_CORR_MODEL,           /* Model-based scatter for singles TX */
  MDC_CONC_SCATTER_CORR_OFF_WINDOW,      /* TX off-window windowed coincidence subtraction */
  MDC_CONC_SCATTER_CORR_SCALED,          /* Singles TX scaled scatter from attenuation subtraction */
  MDC_CONC_NUM_SCATTER_CORR_TYPES
} MdcConcScatterCorrTypes;


typedef enum {
  MDC_CONC_EVENT_UNKNOWN,         /* Unknown event type */
  MDC_CONC_EVENT_SINGLES,         /* Singles */
  MDC_CONC_EVENT_COINCIDENCES,    /* Prompt events (coincidences) */
  MDC_CONC_EVENT_DELAYS,          /* Delay events */
  MDC_CONC_EVENT_TRUES,           /* Trues */
  MDC_CONC_EVENT_ENERGY_SPECTRUM, /* Energy spectrum data */
  MDC_CONC_NUM_EVENT_TYPES
} MdcConcEventTypes;

typedef enum {
  MDC_CONC_FILTER_NONE,          /* No filter */
  MDC_CONC_FILTER_RAMP,          /* Ramp filter (backprojection) or no filter */
  MDC_CONC_FILTER_BUTTERWORTH_1, /* First-order Butterworth window */
  MDC_CONC_FILTER_HANNING,       /* Hanning window */
  MDC_CONC_FILTER_HAMMING,       /* Hamming window */
  MDC_CONC_FILTER_PARZEN,        /* Parzen window */
  MDC_CONC_FILTER_SHEPP,         /* Shepp filter */
  MDC_CONC_FILTER_BUTTERWORTH_2, /* Second-order Butterworth window */
  MDC_CONC_NUM_FILTER_TYPES
} MdcConcFilterTypes;

typedef enum {
  MDC_CONC_NORM_NONE,            /* No normalization applied */
  MDC_CONC_NORM_POINT_INVERSE,   /* Point source inversion */
  MDC_CONC_NORM_POINT_COMPONENT, /* Point source component based */
  MDC_CONC_NORM_CYL_INVERSE,     /* Cylinder source inversion */
  MDC_CONC_NORM_CYL_COMPONENT,   /* Cylinder source component based */
  MDC_CONC_NUM_NORM_TYPES
} MdcConcNormTypes;

typedef enum {
  MDC_CONC_CALIB_UNITS_UNKNOWN,        /* Unknown calibration units */
  MDC_CONC_CALIB_UNITS_NANOCURIES,     /* nanoCuries/cc */
  MDC_CONC_CALIB_UNITS_BEQUERELS,      /* bequerels/cc */
  MDC_CONC_NUM_CALIB_UNITS
} MdcConcCalibUnits;

typedef enum {
  MDC_CONC_DOSE_UNITS_UNKNOWN,        /* Unknown calibration units */
  MDC_CONC_DOSE_UNITS_MILLICURIES,    /* mCi */
  MDC_CONC_DOSE_UNITS_MEGA_BEQUERELS, /* MBq */
  MDC_CONC_NUM_DOSE_UNITS
} MdcConcDoseUnits;

typedef enum {
  MDC_CONC_SUBJECT_ORIENTATION_UNKNOWN,      /* 0 - Unknown subject orientation */
  MDC_CONC_SUBJECT_ORIENTATION_FEET_PRONE,   /* 1 - Feet first, prone */
  MDC_CONC_SUBJECT_ORIENTATION_HEAD_PRONE,   /* 2 - Head first, prone */
  MDC_CONC_SUBJECT_ORIENTATION_FEET_SUPINE, /* 3 - Feet first, supine */
  MDC_CONC_SUBJECT_ORIENTATION_HEAD_SUPINE, /* 4 - Head first, supine */
  MDC_CONC_SUBJECT_ORIENTATION_FEET_RIGHT,   /* 5 - Feet first, right */
  MDC_CONC_SUBJECT_ORIENTATION_HEAD_RIGHT,   /* 6 - Head first, right */
  MDC_CONC_SUBJECT_ORIENTATION_FEET_LEFT,    /* 7 - Feet first, left */
  MDC_CONC_SUBJECT_ORIENTATION_HEAD_LEFT,    /* 8 - Head first, left */
  MDC_CONC_NUM_SUBJECT_ORIENTATIONS
} MdcConcSubjectOrientation;

typedef enum {
  MDC_CONC_LENGTH_UNITS_UNKNOWN,       /* 0 - Unknown length units */
  MDC_CONC_LENGTH_UNITS_MILLIMETERS,   /* 1 - millimeters */
  MDC_CONC_LENGTH_UNITS_CENTIMETERS,   /* 2 - centimeters */
  MDC_CONC_LENGTH_UNITS_INCHES,        /* 3 - inches */
  MDC_CONC_NUM_LENGTH_UNITS
} MdcConcLengthUnits;

typedef enum {
  MDC_CONC_WEIGHT_UNITS_UNKNOWN,       /* 0 - Unknown weight units */
  MDC_CONC_WEIGHT_UNITS_GRAMS,         /* 1 - grams */
  MDC_CONC_WEIGHT_UNITS_OUNCES,        /* 2 - ounces */
  MDC_CONC_WEIGHT_UNITS_KILOGRAMS,     /* 3 - kilograms */
  MDC_CONC_WEIGHT_UNITS_POUNDS,        /* 4 - pounds */
  MDC_CONC_NUM_WEIGHT_UNITS
} MdcConcWeightUnits;

/* what can appear in the top of the header */
typedef enum {
  /* Version of header parameters (float) */
  MDC_CONC_HDR_VERSION,

  /* Manufacturer's name (string) */
  MDC_CONC_HDR_MANUFACTURER,

  /* Scanner model (integer)
     0 - Unknown
     2000 - Primate
     2001 - Rodent
     2002 - microPET2
     2500 - Focus_220
     2501 - Focus_120
     3000 - mCAT
     3500 - mCATII
     4000 - mSPECT
     5000 - Inveon_Dedicated_PET
     5001 - Inveon_MM_Platform
     6000 - MR_PET_Head_Insert
     8000 - Tuebingen_PET_MR */
  MDC_CONC_HDR_MODEL,

  /* Acquisition scanner modality (integer)
     -1 - Unknown acquisition modality
      0 - PET acquisition
      1 - CT acquisition
      2 - SPECT acquisition */
  MDC_CONC_HDR_MODALITY,

  /* Scanner modality configuration number (integer)
     0 - Unknown
     2000 - Primate
     2001 - Rodent
     2002 - microPET2
     2500 - Focus_220
     2501 - Focus_120
     3000 - mCAT
     3500 - mCATII
     3600 - Inveon_MM_Std_CT
     3601 - Inveon_MM_HiRes_Std_CT
     3602 - Inveon_MM_Std_LFOV_CT
     3603 - Inveon_MM_HiRes_LFOV_CT
     5000 - Inveon_Dedicated_PET
     6000 - MR_PET_Head_Insert
     8000 - Tuebingen_PET_MR
     5500 - Inveon_MM_PET */
  MDC_CONC_HDR_MODALITY_CONFIGURATION,

  /* Institution identification (string) */
  MDC_CONC_HDR_INSTITUTION,

  /* Study type/description (string) */
  MDC_CONC_HDR_STUDY,

  /* Data filename, possibly including path (string) */
  /* NOTE: Filename may contain spaces, therefore the ENTIRE */
  /*       line, up to the EOL, is used after the parameter name. */
  MDC_CONC_HDR_FILE_NAME,

  /* Data file type (integer)
   0 - Unknown data file type
   1 - List mode data file
   2 - Sinogram data file
   3 - Normalization data file
   4 - Attenuation correction data file
   5 - Image data file
   6 - Blank data file
   8 - Mu map data file
   9 - Scatter correction data file
   10 - Crystal efficiency data
   11 - Crystal interference correction
   12 - Transaxial geometric correction
   13 - Axial geometric correction
   14 - CT projection data
   15 - SPECT raw projection data
   16 - SPECT energy data from projections
   17 - SPECT normalization data */
  MDC_CONC_HDR_FILE_TYPE,

  /* Acquisition mode (integer)
   0 - Unknown acquisition mode
   1 - Blank acquisition
   2 - Emission acquisition
   3 - Dynamic acquisition
   4 - Gated acquisition
   5 - Continuous bed motion acquisition
   6 - Singles transmission acquisition
   7 - Windowed coincidence transmission acquisition
   8 - Non-windowed coincidence transmission acquisition
   9 - CT projection acquisition
   10 - CT calibration acquisition
   11 - SPECT planar projection acquisition
   12 - SPECT multi-projection acquisition
   13 - SPECT calibration acquisition */
  MDC_CONC_HDR_ACQUISITION_MODE,

  /* Bed control (integer)
     0 - Unknown bed control
     1 - Dedicated PET
     2 - microCAT II
     3 - Multimodality bed control
     4 - microPET bed control */
  MDC_CONC_HDR_BED_CONTROL,

  /* Bed motion (integer) */
  /*   0 - Static or unknown bed motion */
  /*   1 - Continuous bed motion */
  /*   2 - Multiple bed positions, i.e. step and shoot */
  MDC_CONC_HDR_BED_MOTION,

  /* Number of bed positions in data file (integer) */
  MDC_CONC_HDR_NUMBER_BED_POSITIONS,

  /* Horizontal bed calibration, in microns (float) */
  MDC_CONC_HDR_HORIZONTAL_BED_CALIBRATION,

  /* Vertical bed calibration, in microns (float) */
  MDC_CONC_HDR_VERTICAL_BED_CALIBRATION,

  /* Number of frames in data file (integer) */
  MDC_CONC_HDR_TOTAL_FRAMES,

  /* Number of time frames in data file (integer) */
  MDC_CONC_HDR_TIME_FRAMES,

  /* Isotope description (string) */
  MDC_CONC_HDR_ISOTOPE,

  /* Isotope half-life, in secs (float) */
  MDC_CONC_HDR_ISOTOPE_HALF_LIFE,

  /* Isotope branching fraction (float) */
  /*   NOTE: Frame scale factor DOES NOT include */
  /*         isotope branching fraction. */
  MDC_CONC_HDR_ISOTOPE_BRANCHING_FRACTION,

  /* Transaxial crystals per block (integer) */
  MDC_CONC_HDR_TRANSAXIAL_CRYSTALS_PER_BLOCK,

  /* Axial crystals per block (integer) */
  MDC_CONC_HDR_AXIAL_CRYSTALS_PER_BLOCK,

  /* Crystal offset for intrinsic rotation (integer) */
  MDC_CONC_HDR_INTRINSIC_CRYSTAL_OFFSET,

  /* Number of transaxial blocks (integer) */
  MDC_CONC_HDR_TRANSAXIAL_BLOCKS,

  /* Number of axial blocks (integer) */
  MDC_CONC_HDR_AXIAL_BLOCKS,

  /* Transaxial crystal pitch, in cm (float) */
  MDC_CONC_HDR_TRANSAXIAL_CRYSTAL_PITCH,

  /* Axial crystal pitch, in cm (float) */
  MDC_CONC_HDR_AXIAL_CRYSTAL_PITCH,

  /* Ring radius to crystal face, in cm (float) */
  MDC_CONC_HDR_RADIUS,

  /* Radial field-of-view, in cm (float) */
  MDC_CONC_HDR_RADIAL_FOV,

  /* (Point) source radius, in cm (float) */
  MDC_CONC_HDR_PT_SRC_RADIUS, /* deprecated? */
  MDC_CONC_HDR_SRC_RADIUS,

  /* Source axial cm per revolution, in cm (float) */
  MDC_CONC_HDR_SRC_CM_PER_REV,

  /* Source type (integer) */
  /* 0 - Unknown TX source type */
  /* 1 - TX point source */
  /* 2 - TX line source */
  MDC_CONC_HDR_TX_SRC_TYPE,

  /* (Point) source encoder steps per revolution (integer) */
  MDC_CONC_HDR_PT_SRC_STEPS_PER_REV, /* deprecated? */
  MDC_CONC_HDR_SRC_STEPS_PER_REV,

  /* Default number of projections (integer) */
  MDC_CONC_HDR_DEFAULT_PROJECTIONS,

  /* Default number of transaxial angles (integer) */
  MDC_CONC_HDR_DEFAULT_TRANSAXIAL_ANGLES,

  /* Crystal thickness, in cm (float) */
  MDC_CONC_HDR_CRYSTAL_THICKNESS,

  /* Depth of interaction, in cm (float) */
  MDC_CONC_HDR_DEPTH_OF_INTERACTION,

  /* Transaxial projection bin size, in cm (float) */
  MDC_CONC_HDR_TRANSAXIAL_BIN_SIZE,

  /* Axial plane size, in cm (float) */
  MDC_CONC_HDR_AXIAL_PLANE_SIZE,

  /* Number of detector panels ("rings/CT" are "1") (integer) */
  MDC_CONC_HDR_NUMBER_DETECTOR_PANELS,

  /* Lower level energy threshold, in KeV (float) */
  MDC_CONC_HDR_LLD,

  /* Upper level energy threshold, in KeV (float) */
  MDC_CONC_HDR_ULD,

  /* Coincidence timing window, in nsecs (int) */
  MDC_CONC_HDR_TIMING_WINDOW,

  /* Data type (integer) */
  /*   0 - Unknown data type */
  /*   1 - Byte (8-bits) data type */
  /*   2 - 2-byte integer - Little Endian */
  /*   3 - 4-byte integer - Little Endian */
  /*   4 - 4-byte IEEE float - Little Endian */
  /*   5 - 4-byte IEEE float - Big Endian */
  /*   6 - 2-byte integer - Big Endian */
  /*   7 - 4-byte integer - Big Endian */
  MDC_CONC_HDR_DATA_TYPE,

  /* Data order (integer) */
  /*   0 - Element/Axis/View/Ring_Diff - view mode */
  /*   1 - Element/View/Axis/Ring_Diff - sinogram mode */
  /*  NOTE that ElVwAxRd (XYZW) is the data order for images. */
  /*    ElVwAxRd for images means that Z and Y are flipped. */
  MDC_CONC_HDR_DATA_ORDER,

  /* Span of data set (integer) */
  MDC_CONC_HDR_SPAN,

  /* Maximum ring difference of data set (integer) */
  MDC_CONC_HDR_RING_DIFFERENCE,

  /* Number of dimensions in data set (integer) */
  /*     Order from fastest to slowest is XYZW */
  MDC_CONC_HDR_NUMBER_OF_DIMENSIONS,

  /* Size of X dimension in data set (integer) */
  MDC_CONC_HDR_X_DIMENSION,

  /* Size of Y dimension in data set (integer) */
  MDC_CONC_HDR_Y_DIMENSION,

  /* Size of Z dimension in data set (integer) */
  MDC_CONC_HDR_Z_DIMENSION,

  /* Size of W dimension in data set (integer) */
  MDC_CONC_HDR_W_DIMENSION,

  /* Size of 'changing' dimension at each step (integer integer) */
  MDC_CONC_HDR_DELTA_ELEMENTS,

  /* X filter and/or apodizing windows type (integer) */
  /*       and cutoff (float) */
  /*   0 - No filter */
  /*   1 - Ramp filter (backprojection) or no filter */
  /*   2 - First-order Butterworth window */
  /*   3 - Hanning window */
  /*   4 - Hamming window */
  /*   5 - Parzen window */
  /*   6 - Shepp filter */
  /*   7 - Second-order Butterworth window */
  /*   NOTE that a cutoff of 0.5 is the Nyquist point */
  /*     i.e 1.0 / (2.0 * sampling). */
  /*     Also, the Ramp and Shepp should ONLY be used */
  /*     for backprojection */
  MDC_CONC_HDR_X_FILTER,

  /* Y apodizing filter type (integer) and cutoff (float) */
  /*   0 - No filter */
  /*   2 - First-order Butterworth window */
  /*   3 - Hanning window */
  /*   4 - Hamming window */
  /*   5 - Parzen window */
  /*   7 - Second-order Butterworth window */
  /*   NOTE that a cutoff of 0.5 is the Nyquist point */
  /*     i.e 1.0 / (2.0 * sampling). */
  MDC_CONC_HDR_Y_FILTER,

  /* Z apodizing filter type (integer) and cutoff (float) */
  /*   0 - No filter */
  /*   2 - First-order Butterworth window */
  /*   3 - Hanning window */
  /*   4 - Hamming window */
  /*   5 - Parzen window */
  /*   7 - Second-order Butterworth window */
  /*   NOTE that a cutoff of 0.5 is the Nyquist point */
  /*     i.e 1.0 / (2.0 * sampling). */
  MDC_CONC_HDR_Z_FILTER,

  /* Version of histogram program used (float) */
  MDC_CONC_HDR_HISTOGRAM_VERSION,

  /* Rebinning type (integer) */
  /*   0 - Unknown, or no, algorithm type */
  /*   1 - Full 3D binning (span and ring difference) */
  /*   2 - Single-Slice Rebinning */
  /*   3 - Fourier Rebinning */
  MDC_CONC_HDR_REBINNING_TYPE,

  /* Version of rebinning program used (float) */
  MDC_CONC_HDR_REBINNING_VERSION,

  /* Reconstruction type (integer) */
  /*   0 - Unknown, or no, algorithm type */
  /*   1 - Filtered Backprojection */
  /*   2 - OSEM2D */
  /*   3 - OSEM3D */
  /*   4 - unused */
  /*   5 - unused */
  /*   6 - OSEM3D followed by MAP or FastMAP */
  /*   7 - MAPTR for transmission image */
  /*   8 - MAP 3D reconstruction */
  /*   9 - Feldkamp cone beam */
  MDC_CONC_HDR_RECON_ALGORITHM,

  /* Version of reconstruction program used (float) */
  MDC_CONC_HDR_RECON_VERSION,

  /* Number of osem3d subsets in MAP reconstruction (integer) */
  MDC_CONC_HDR_MAP_SUBSETS,

  /* Number of osem3d iterations in MAP reconstruction (integer) */
  MDC_CONC_HDR_MAP_OSEM3D_ITERATIONS,

  /* Number of MAP iterations after osem3d iterations (integer) */
  MDC_CONC_HDR_MAP_ITERATIONS,

  /* Beta value for MAP reconstruction (float) */
  MDC_CONC_HDR_MAP_BETA,

  /* MAP blur kernel type (int) */
  MDC_CONC_HDR_MAP_BLUR_TYPE,

  /* MAP prior type (int) */
  MDC_CONC_HDR_MAP_PRIOR_TYPE,

  /* MAP blur kernel file prefix (string) */
  MDC_CONC_HDR_MAP_BLUR_FILE,

  /* MAP P matrix file prefix (string) */
  MDC_CONC_HDR_MAP_PMATRIX_FILE,

  /* OSEM2D method (integer) */
  /* 0 - Unweighted osem2d reconstruction */
  /* 1 - Attenuation weighted osem2d reconstruction */
  MDC_CONC_HDR_OSEM2D_METHOD,

  /* Number of osem2d subsets (integer) */
  MDC_CONC_HDR_OSEM2D_SUBSETS,

  /* Number of osem2d iterations (integer) */
  MDC_CONC_HDR_OSEM2D_ITERATIONS,

  /* Number of EM iterations after osem2d iterations (integer) */
  MDC_CONC_HDR_OSEM2D_EM_ITERATIONS,

  /* Epsilon and power values for map regularization (float integer) */
  MDC_CONC_HDR_OSEM2D_MAP,

  /* Large object osem2d x_offset in cm (float) */
  MDC_CONC_HDR_OSEM2D_X_OFFSET,

  /* Large object osem2d y_offset in cm (float) */
  MDC_CONC_HDR_OSEM2D_Y_OFFSET,

  /* Large object osem2d zoom (float) */
  MDC_CONC_HDR_OSEM2D_ZOOM,

  /* Deadtime correction applied to data set (integer) */
  /*   0 - No deadtime correction applied */
  /*   1 - Global estimate based on singles */
  /*   2 - CMS estimate based on singles */
  /*   3 - Global estimate based on running deadtime average */
  /*   4 - Blank/TX singles estimate (block based) */
  MDC_CONC_HDR_DEADTIME_CORRECTION_APPLIED,

  /* Decay correction applied to data set (integer) */
  /*     0 (FALSE) - Decay correction has NOT been applied */
  /*     !0 (TRUE) - Decay correction has been applied */
  MDC_CONC_HDR_DECAY_CORRECTION_APPLIED,

  /* Normalization applied to data set (integer) */
  /*   0 - No normalization applied */
  /*   1 - Point source inversion */
  /*   2 - Point source component based */
  /*   3 - Cylinder source inversion */
  /*   4 - Cylinder source component based */
  MDC_CONC_HDR_NORMALIZATION_APPLIED,

  /* Normalization filename, possibly including path (string) */
  /*    NOTE: Filename may contain spaces, therefore the ENTIRE */
  /*          line, up to the EOL, is used after the parameter name. */
  MDC_CONC_HDR_NORMALIZATION_FILENAME,

  /* Attenuation applied to data set (integer) */
  /*   0 - No attenuation applied */
  /*   1 - Point source in windowed TX coincidence */
  /*   2 - Point source singles based TX */
  /*   3 - Segmented point source in TX coincidence */
  /*   4 - Segmented point source singles based TX */
  /*   5 - Calculated by geometry */
  /*   6 - Non-positron source singles based TX */
  /*   7 - Point source in non-windowed TX coincidence */
  /*   8 - Generated from CT image */
  MDC_CONC_HDR_ATTENUATION_APPLIED,

  /* Attenuation correction filename, possibly including path (string) */
  /*    NOTE: Filename may contain spaces, therefore the ENTIRE */
  /*          line, up to the EOL, is used after the parameter name. */
  MDC_CONC_HDR_ATTENUATION_FILENAME,

  /* Scatter correction applied to data set (integer) */
  /*   0 - No scatter correction applied */
  /*   1 - Fit of emission tail */
  /*   2 - Monte Carlo of emission and transmission data */
  /*   3 - Direct calculation from analytical formulas */
  /*   4 - Model-based scatter for singles TX */
  /*   5 - TX off-window windowed coincidence subtraction */
  /*   6 - Singles TX scaled scatter from attenuation subtraction */
  MDC_CONC_HDR_SCATTER_CORRECTION,

  /* Version of scatter program used (float) */
  MDC_CONC_HDR_SCATTER_VERSION,

  /* Arc correction applied to data set (integer) */
  /*     0 (FALSE) - Arc correction has NOT been applied */
  /*     !0 (TRUE) - Arc correction has been applied */
  MDC_CONC_HDR_ARC_CORRECTION_APPLIED,

  /* Rotation, in degrees, applied to data set (float) */
  MDC_CONC_HDR_ROTATION,

  /* X offset, in cm, applied to data set (float) */
  MDC_CONC_HDR_X_OFFSET,

  /* Y offset, in cm, applied to data set (float) */
  MDC_CONC_HDR_Y_OFFSET,

  /* Z offset, in cm, applied to data set (float) */
  MDC_CONC_HDR_Z_OFFSET,

  /* Zoom applied to data set (float) */
  MDC_CONC_HDR_ZOOM,

  /* X origin of volume, in voxels (integer) */
  MDC_CONC_HDR_VOLUME_ORIGIN_X,

  /* Y origin of volume, in voxels (integer) */
  MDC_CONC_HDR_VOLUME_ORIGIN_Y,

  /* Z origin of volume, in voxels (integer) */
  MDC_CONC_HDR_VOLUME_ORIGIN_Z,

  /* Registration data available (integer)
     0 - No registration data available
     1 - CT registration data available
     2 - PET registration data available */
  MDC_CONC_HDR_REGISTRATION_AVAILABLE,

  /* Transformation matrix filename, possibly including path (string)
     NOTE: Filename may contain spaces, therefore the ENTIRE
     line, up to the EOL, is used after the parameter name. */
  MDC_CONC_HDR_TRANSFORMATION_MATRIX,

  /* Spatial identification for registration (string) */
  MDC_CONC_HDR_SPATIAL_IDENTIFIER,

  /* Reconstructed pixel size, in cm (float) */
  /*     NOTE: pixel_size = (((X_crystal_pitch / 2.0) * X_dim) / */
  /*           (image_size * zoom)) * (effective_radius / radius) */
  MDC_CONC_HDR_PIXEL_SIZE,

  /* Reconstructed pixel size in X, in mm (float) */
  MDC_CONC_HDR_PIXEL_SIZE_X,

  /* Reconstructed pixel size in Y, in mm (float) */
  MDC_CONC_HDR_PIXEL_SIZE_Y,

  /* Reconstructed pixel size in Z, in mm (float) */
  MDC_CONC_HDR_PIXEL_SIZE_Z,

  /* Calibration units (integer) */
  /*   0 - Unknown calibration units */
  /*   1 - nanoCuries/cc */
  /*   2 - bequerels/cc */
  MDC_CONC_HDR_CALIBRATION_UNITS,

  /* Calibration factor (float) */
  /*   NOTE: Frame scale factor DOES NOT include calibration factor. */
  MDC_CONC_HDR_CALIBRATION_FACTOR,

  /* Calibration source branching fraction (float) */
  /* NOTE: Frame scale factor DOES NOT include
   *       calibration source branching fraction.  */
  MDC_CONC_HDR_CALIBRATION_BRANCHING_FRACTION,

  /* Number of singles rates in subheader (integer) */
  /*   NOTE: This normally is the number of blocks. */
  MDC_CONC_HDR_NUMBER_OF_SINGLES_RATES,

  /* Investigator identification (string) */
  MDC_CONC_HDR_INVESTIGATOR,

  /* Operator identification (string) */
  MDC_CONC_HDR_OPERATOR,

  /* Study identification (string) */
  MDC_CONC_HDR_STUDY_IDENTIFIER,

  /* Acquisition user ID (string) */
  MDC_CONC_HDR_ACQUISITION_USER_ID,

  /* Histogram user ID (string) */
  MDC_CONC_HDR_HISTOGRAM_USER_ID,

  /* Reconstruction user ID (string) */
  MDC_CONC_HDR_RECONSTRUCTION_USER_ID,

  /* Scatter correction user ID (string) */
  MDC_CONC_HDR_SCATTER_CORRECTION_USER_ID,

  /* Acquisition notes (string) */
  MDC_CONC_HDR_ACQUISITION_NOTES,

  /* Scan start date and time (string) */
  /* Format is: Sun Sep 16 01:03:52 1973 */
  MDC_CONC_HDR_SCAN_TIME,

  /* Scan start date and time - GMT-based (string)
     Format is: Sun Sep 16 01:03:52 1973 */
  MDC_CONC_HDR_GMT_SCAN_TIME,

  /* Injected compound (string) */
  MDC_CONC_HDR_INJECTED_COMPOUND,

  /* Dose units (integer) */
  /* 0 - Unknown dose units */
  /* 1 - mCi */
  /* 2 - MBq */
  MDC_CONC_HDR_DOSE_UNITS,

  /* Injected dose (float) */
  MDC_CONC_HDR_INJECTED_DOSE,

  /* Injection date and time (string) */
  /* Format is: Sun Sep 16 01:03:52 1973 */
  MDC_CONC_HDR_INJECTION_TIME,

  /* Injection decay correction factor (float) */
  /* NOTE: Frame scale factor and decay correction factor */
  /* DO NOT include injection decay correction factor. */
  MDC_CONC_HDR_INJECTION_DECAY_CORRECTION,

  /* Pre- and residual activity units (integer)
     0 - Unknown dose units
     1 - mCi
     2 - MBq */
  MDC_CONC_HDR_ACTIVITY_UNITS,

  /* Activity before injection (float) */
  MDC_CONC_HDR_ACTIVITY_BEFORE_INJECTION,

  /* Activity before injection measurement date and time (string)
     Format is: Sun Sep 16 01:03:52 1973 */
  MDC_CONC_HDR_ACTIVITY_BEFORE_INJECTION_TIME,

  /* Residual activity (float) */
  MDC_CONC_HDR_RESIDUAL_ACTIVITY,

  /* Residual activity measurement date and time (string)
     Format is: Sun Sep 16 01:03:52 1973 */
  MDC_CONC_HDR_RESIDUAL_ACTIVITY_TIME,

  /* Number of gating inputs in study (integer) */
  /* NOTE: This is ONLY present when gating inputs are present. */
  MDC_CONC_HDR_GATE_INPUTS,

  /* Gate input bins per cycle and gate input range array (integer  integer  float   float) */
  /* NOTE: This is ONLY present when gating inputs are present. */
  /* If the minimum/maximum gate input ranges are < 0.0, all values up to or below are accepted. */
  /* If they are NOT present, all values are accepted. */
  /* gate_input    gate_bins/cycle    minimum gate cycle (secs)   maximum gate cycle (secs) */
  MDC_CONC_HDR_GATE_BINS,

  /* Gate input description (integer  string) */
  /* NOTE: This is ONLY present when gating inputs are present. */
  /* gate_input    gate_description */
  MDC_CONC_HDR_GATE_DESCRIPTION,

  /* Subject identifier (string) */
  MDC_CONC_HDR_SUBJECT_IDENTIFIER,

  /* Subject genus (string) */
  MDC_CONC_HDR_SUBJECT_GENUS,

  /* Subject orientation (integer) */
  /* 0 - Unknown subject orientation */
  /* 1 - Feet first, prone */
  /* 2 - Head first, prone */
  /* 3 - Feet first, supine */
  /* 4 - Head first, supine */
  /* 5 - Feet first, right */
  /* 6 - Head first, right */
  /* 7 - Feet first, left */
  /* 8 - Head first, left */
  MDC_CONC_HDR_SUBJECT_ORIENTATION,

  /* Length units (integer) */
  /* 0 - Unknown length units */
  /* 1 - millimeters */
  /* 2 - centimeters */
  /* 3 - inches */
  MDC_CONC_HDR_SUBJECT_LENGTH_UNITS,

  /* Subject length (float) */
  MDC_CONC_HDR_SUBJECT_LENGTH,

  /* Weight units (integer) */
  /* 0 - Unknown weight units */
  /* 1 - grams */
  /* 2 - ounces */
  /* 3 - kilograms */
  /* 4 - pounds */
  MDC_CONC_HDR_SUBJECT_WEIGHT_UNITS,

  /* Subject weight (float) */
  MDC_CONC_HDR_SUBJECT_WEIGHT,

  /* Subject phenotype (string) */
  MDC_CONC_HDR_SUBJECT_PHENOTYPE,

  /* Study model (string) */
  MDC_CONC_HDR_STUDY_MODEL,

  /* Subject anesthesia (string) */
  MDC_CONC_HDR_ANESTHESIA,

  /* Subject analgesia (string) */
  MDC_CONC_HDR_ANALGESIA,

  /* Other drugs (string) */
  MDC_CONC_HDR_OTHER_DRUGS,

  /* Food access (string) */
  MDC_CONC_HDR_FOOD_ACCESS,

  /* Water access (string) */
  MDC_CONC_HDR_WATER_ACCESS,

  /* Subject date of birth (string) */
  MDC_CONC_HDR_SUBJECT_DOB,

  /* Subject age (string) */
  MDC_CONC_HDR_SUBJECT_AGE,

  /*Subject sex (string) */
  MDC_CONC_HDR_SUBJECT_SEX,

  /* Subject scan region (string) */
  MDC_CONC_HDR_SUBJECT_SCAN_REGION,

  /* Subject glucose level (string) */
  MDC_CONC_HDR_SUBJECT_GLUCOSE_LEVEL,

  /* Subject glucose level measurement time (string) */
  MDC_CONC_HDR_SUBJECT_GLUCOSE_LEVEL_TIME,

  /* Original acquisition filename, DOES NOT INCLUDE path (string)
    NOTE: Filename may contain spaces, therefore the ENTIRE
    line, up to the EOL, is used after the parameter name. */
  MDC_CONC_HDR_ACQUISITION_FILE_NAME,


  /* Following parameters are used for CT or SPECT modalities */


  /* Gantry rotation (integer)
     0 - No gantry rotation
     1 - Rotation with discrete steps
     2 - Continuous rotation */
  MDC_CONC_HDR_GANTRY_ROTATION,

  /* Rotation direction (integer)
     0 - Clockwise
     1 - Counterclockwise */
  MDC_CONC_HDR_ROTATION_DIRECTION,

  /* Rotating gantry starting angle, in degrees (float) */
  MDC_CONC_HDR_ROTATING_STAGE_START_POSITION,

  /* Rotating gantry stop angle, in degrees (float) */
  MDC_CONC_HDR_ROTATING_STAGE_STOP_POSITION,

  /* Number of rotation projections for rotating gantry (integer) */
  MDC_CONC_HDR_NUMBER_OF_PROJECTIONS,

  /* Number of gantry revolutions for rotating gantry (float) */
  MDC_CONC_HDR_GANTRY_ROTATIONS,

  /* Following parameters are used for CT modality only */

  /* CAT file version (integer) */
  MDC_CONC_HDR_CT_FILE_VERSION,

  /* Header size of CAT files before dark and light projections (integer) */
  MDC_CONC_HDR_CT_HEADER_SIZE,

  /* CT transaxial projection size, in pixels (integer) */
  MDC_CONC_HDR_CT_PROJ_SIZE_TRANSAXIAL,

  /* CT axial projection size, in pixels (integer) */
  MDC_CONC_HDR_CT_PROJ_SIZE_AXIAL,

  /* Number to average the dark calibration projections (integer) */
  MDC_CONC_HDR_CT_AVERAGE_DARK_PROJECTIONS,

  /* Number to average the light calibration projections (integer) */
  MDC_CONC_HDR_CT_AVERAGE_LIGHT_PROJECTIONS,

  /* Total positions to acquire the light calibration projections (integer) */
  MDC_CONC_HDR_CT_LIGHT_CALIBRATION_PROJECTIONS,

  /* Indicates if the positions to acquire light projections are same as scan projection positions (integer) */
  MDC_CONC_HDR_CT_DEPENDENT_LIGHT_CALIBRATION_PROJECTIONS,

  /* CT X-ray detector offset, in mm (float) */
  MDC_CONC_HDR_CT_XRAY_DETECTOR_OFFSET,

  /* CT detector transaxial position, in cm (float) */
  MDC_CONC_HDR_CT_DETECTOR_TRANSAXIAL_POSITION,

  /* CT detector uncropped transaxial pixels (integer) */
  MDC_CONC_HDR_CT_UNCROPPED_TRANSAXIAL_PIXELS,

  /* CT detector uncropped axial pixels (integer) */
  MDC_CONC_HDR_CT_UNCROPPED_AXIAL_PIXELS,

  /* CT detector cropped transaxial pixels (integer) */
  MDC_CONC_HDR_CT_CROPPED_TRANSAXIAL_PIXELS,

  /* CT detector cropped axial pixels (integer) */
  MDC_CONC_HDR_CT_CROPPED_AXIAL_PIXELS,

  /* CT X-ray detector pitch, in um (float) */
  MDC_CONC_HDR_CT_XRAY_DETECTOR_PITCH,

  /* CT horizontal rotation-axis-bed angle, in degrees (float) */
  MDC_CONC_HDR_CT_HORIZ_ROT_AXIS_BED_ANGLE,

  /* CT vertical rotation-axis-bed angle, in degrees (float) */
  MDC_CONC_HDR_CT_VERT_ROT_AXIS_BED_ANGLE,

  /* CT exposure time, in msecs (float) */
  MDC_CONC_HDR_CT_EXPOSURE_TIME,

  /* CT total scan time, in min:sec (integer:integer) */
  MDC_CONC_HDR_CT_SCAN_TIME,

  /* CT warping type (integer)
   0 - Unknown warping type
   1 - No warping
   2 - Bilinear warping
   3 - Nearest neighbor warping */
  MDC_CONC_HDR_CT_WARPING,

  /* CT defect map filename, possibly including path (string)
    NOTE: Filename may contain spaces, therefore the ENTIRE
    line, up to the EOL, is used after the parameter name. */
  MDC_CONC_HDR_CT_DEFECT_MAP_FILE_NAME,

  /* CT x-ray voltage, in kVp (float) */
  MDC_CONC_HDR_CT_XRAY_VOLTAGE,

  /* CT anode current, in uA (float) */
  MDC_CONC_HDR_CT_ANODE_CURRENT,

  /* Number of CT calibration exposures (integer) */
  MDC_CONC_HDR_CT_CALIBRATION_EXPOSURES,

  /* CT cone angle, in degrees (float) */
  MDC_CONC_HDR_CT_CONE_ANGLE,

  /* CT projection interpolation type (integer)
   0 - Unknown projection interpolation type
   1 - Bilinear projection interpolation
   2 - Nearest neighbor projection interpolation */
  MDC_CONC_HDR_CT_PROJECTION_INTERPOLATION,

  /* CT source to detector distance, in cm (float) */
  MDC_CONC_HDR_CT_SOURCE_TO_DETECTOR,

  /* CT source to center of rotation, in cm (float) */
  MDC_CONC_HDR_CT_SOURCE_TO_CROT,

  /* CT vetical detector offset, in pixels (float) */
  MDC_CONC_HDR_CT_DETECTOR_VERTICAL_OFFSET,

  /* CT detector tilt relative to horizontal axis, in degrees (float) */
  MDC_CONC_HDR_CT_DETECTOR_HORIZONTAL_TILT,

  /* CT detector tilt relative to vertical axis, in degrees (float) */
  MDC_CONC_HDR_CT_DETECTOR_VERTICAL_TILT,

  /* CT axial projection bin factor, in pixels (integer) */
  MDC_CONC_HDR_CT_TRANSAXIAL_BIN_FACTOR,

  /* CT axial projection bin factor, in pixels (integer) */
  MDC_CONC_HDR_CT_AXIAL_BIN_FACTOR,

  /* CT gating signal used (integer) */
  MDC_CONC_HDR_CT_GATING,

  /* CT Hounsfield scale (float) */
  MDC_CONC_HDR_CT_HOUNSFIELD_SCALE,

  /* CT Hounsfield offset (float) */
  MDC_CONC_HDR_CT_HOUNSFIELD_OFFSET,

  /* CT projection downsample factor (integer) */
  MDC_CONC_HDR_CT_PROJ_DOWNSAMPLE_FACTOR,

  /* CT first projection used in reconstruction (integer) */
  MDC_CONC_HDR_CT_FIRST_RECON_PROJ,

  /* CT last projection used in reconstruction (integer) */
  MDC_CONC_HDR_CT_LAST_RECON_PROJ,

  /* CT every Nth projection used for reconstruction (integer) */
  MDC_CONC_HDR_CT_RECON_EVERY_NTH_PROJ,

  /* CT attenuation of water, in cm^-1 (float) */
  MDC_CONC_HDR_CT_ATTENUATION_WATER,

  /* CT TX rotation offsets: X Y Z, in mm (float) */
  MDC_CONC_HDR_CT_TX_ROTATION_OFFSETS,

  /* CT TX transaxial offsets: X Y Z, in mm (float) */
  MDC_CONC_HDR_CT_TX_TRANSAXIAL_OFFSETS,

  /* CT BH correction applied (int)
     0 (FALSE) - CT BH correction has NOT been applied
     !0 (TRUE) - CT BH correction has been applied */
  MDC_CONC_HDR_CT_BH_CORRECTION,

  /* CT aluminum filter thickness (mm) (float) */
  MDC_CONC_HDR_CT_ALUMINUM_FILTER_THICKNESS,

  /* CT projection array (integer  float)
     projection_number    acquisition_position (degrees) */
  MDC_CONC_HDR_PROJECTION,

  /* CT projection average center offset (float) */
  MDC_CONC_HDR_CT_PROJECTION_AVERAGE_CENTER_OFFSET,

  /* CT projection average center offset array (integer  float)
     projection_number    average center offset (mm) */
  MDC_CONC_HDR_CT_PROJECTION_CENTER_OFFSET,

  /* CT projection horizontal bed offset array (integer  float)
     projection_number    horizontal bed offset (mm) */
  MDC_CONC_HDR_CT_PROJECTION_HORIZONTAL_BED_OFFSET,

  /* End of Header indicator */
  MDC_CONC_HDR_END_OF_HEADER,

  MDC_CONC_NUM_HDR_VALUES,
  MDC_CONC_HDR_UNKNOWN,
  MDC_CONC_HDR_EOF
} MdcConcHdrValue;

/* what can appear in the bottom of the header */
typedef enum {
  /* Frame number (integer) */
  MDC_CONC_BLOCK_FRAME,

  /* Detector panel - NOTE: "ring" systems are "0" ONLY (integer) */
  MDC_CONC_BLOCK_DETECTOR_PANEL,

  /* Event type (integer) */
  /*   0 - Unknown event type */
  /*   1 - Singles */
  /*   2 - Prompt events (coincidences) */
  /*   3 - Delay events */
  /*   4 - Trues (prompts - delays) */
  /*   5 - Energy spectrum data */
  MDC_CONC_BLOCK_EVENT_TYPE,

  /* Energy window - NOTE PET/CT systems are typically "0" only (integer) */
  MDC_CONC_BLOCK_ENERGY_WINDOW,

  /* Gate number (integer) */
  MDC_CONC_BLOCK_GATE,

  /* Bed number (integer) */
  MDC_CONC_BLOCK_BED,

  /* Bed offset, in cm (float) */
  MDC_CONC_BLOCK_BED_OFFSET,

  /* Ending horizontal bed offset, in cm (float) */
  MDC_CONC_BLOCK_ENDING_BED_OFFSET,

  /* Number of bed passes during frame (integer) */
  MDC_CONC_BLOCK_BED_PASSES,

  /* Vertical bed offset, in cm (float) */
  MDC_CONC_BLOCK_VERTICAL_BED_OFFSET,

  /* Data file offset to start of data (2 longs) */
  /* Values are: low_part */
  /*         or: high_part low_part */
  MDC_CONC_BLOCK_DATA_FILE_POINTER,

  /* Frame start time, in secs (float) */
  MDC_CONC_BLOCK_FRAME_START,

/* Frame duration, in secs (float) */
  MDC_CONC_BLOCK_FRAME_DURATION,

  /* Scale factor for data set (float) */
  MDC_CONC_BLOCK_SCALE_FACTOR,

  /* Minimum value in data set (float) */
  MDC_CONC_BLOCK_MINIMUM,

  /* Maximum value in data set (float) */
  MDC_CONC_BLOCK_MAXIMUM,

  /* Deadtime correction for data set (float) */
  /*             NOTE: Scale factor INCLUDES this value. */
  MDC_CONC_BLOCK_DEADTIME_CORRECTION,

  /* Global decay correction applied to data set (float) */
  /*     NOTE: Scale factor INCLUDES this value. */
  MDC_CONC_BLOCK_DECAY_CORRECTION,

  /* Prompts count for data set (long) */
  MDC_CONC_BLOCK_PROMPTS,

  /* Delays count for data set (long) */
  MDC_CONC_BLOCK_DELAYS,

  /* Trues count for data set (long) */
  MDC_CONC_BLOCK_TRUES,

  /* Prompts countrate per sec before histogramming (int) */
  MDC_CONC_BLOCK_PROMPTS_RATE,

  /* Delays countrate per sec before histogramming (int) */
  MDC_CONC_BLOCK_DELAYS_RATE,

  /* Singles rate array (integer float) */
  /*       block_number    singles/sec */
  MDC_CONC_BLOCK_SINGLES,

  /* End of Header indicator */
  MDC_CONC_BLOCK_END_OF_HEADER,

  MDC_CONC_NUM_BLOCK_VALUES,
  MDC_CONC_BLOCK_UNKNOWN,
  MDC_CONC_BLOCK_EOF
} MdcConcBlockValue;



/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

const char *MdcLoadPlaneCONC(FILEINFO *fi, int img);
const char *MdcLoadHeaderCONC(FILEINFO *fi);
const char *MdcLoadCONC(FILEINFO *fi);
const char *MdcSavePlaneCONC(FILEINFO *fi, int img);
const char *MdcSaveInitCONC(FILEINFO *fi, char *raw_filename);
const char *MdcSaveHeaderCONC(FILEINFO *fi, char *raw_filename);
const char *MdcSaveCONC(FILEINFO *fi);

int MdcCheckCONC(FILEINFO *fi);
const char *MdcReadCONC(FILEINFO *fi);
const char *MdcWriteCONC(FILEINFO *fi);

#endif

