/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xfilesel.h                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xfilesel.c header file                                   *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XFILESEL_H__
#define __XFILESEL_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/
void XMdcFileSelOpenOk(GtkWidget *fs);
void XMdcFileSelOpen(GtkWidget *widget, guint otype);
void XMdcFileSelSaveSetDefaultName(GtkWidget *fs);
GtkWidget *XMdcFileSelSaveCreateFormatMenu(GtkWidget *fs, guint format);
GtkWidget *XMdcFileSelSaveCreateBtnAlias(GtkWidget *fs);
GtkWidget *XMdcFileSelSaveCreateBtnDefault(GtkWidget *fs);
void XMdcFileSelSaveCallbackFormatMenu(GtkWidget *widget, gpointer data);
void XMdcFileSelSaveCallbackAlias(GtkWidget *fs, char *filename);
void XMdcFileSelSaveCallbackDefault(GtkWidget *fs, char *filename);
void XMdcFileSelSaveCancel(GtkWidget *fs);
void XMdcFileSelSaveOk(GtkWidget *fs);
void XMdcFileSelSave(GtkWidget *widget, guint format);
void XMdcLutSelOpenOk(GtkWidget *fs);
void XMdcLutSelOpen(void);
void XMdcRawPredefSelSaveOk(GtkWidget *fs);
void XMdcRawPredefSelSave(GtkWidget *widget, gpointer data);
void XMdcRawPredefSelOpenOk(GtkWidget *fs);
void XMdcRawPredefSelOpen(GtkWidget *widget, gpointer data);

#endif

