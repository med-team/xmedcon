/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xvifi.c                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : edit FILEINFO structure                                  *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcEditFileInfoCallbackApply - Apply FILEINFO changes   *
 *                XMdcEditFileInfo()            - Edit FILEINFO struct     *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/


/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcEditFileInfoCallbackApply(GtkWidget *widget, gpointer data)
{

  IMG_DATA *id;
  DYNAMIC_DATA *dd;
  const char *entry;
  char *msg;
  float pixel_size, slice_width, slice_spacing, frame_duration;
  Uint32 i, planes, frames, gates, beds, windows, number;

  if (XMdcNoFileOpened()) return;

  /* Orientation */
  for (i=0; i < MDC_MAX_ORIENT; i++) {
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.PatSliceOrient[i]))) {
       my.fi->pat_slice_orient = (Int8)i; break;
     }
  }

  strcpy(my.fi->pat_pos,MdcGetStrPatPos(my.fi->pat_slice_orient));
  strcpy(my.fi->pat_orient,MdcGetStrPatOrient(my.fi->pat_slice_orient));

  /* Sizes/Time */
  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.PixelSize));
  pixel_size = (float)atof(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.SliceWidth));
  slice_width = (float)atof(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.SliceSpacing));
  slice_spacing = (float)atof(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.FrameDuration));
  frame_duration = (float)atof(entry);

  /* Dimensions */
  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.NrDimPlanes));
  planes = (Uint32)atoi(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.NrDimFrames));
  frames = (Uint32)atoi(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.NrDimGates));
  gates = (Uint32)atoi(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.NrDimBeds));
  beds = (Uint32)atoi(entry);

  entry = gtk_entry_get_text(GTK_ENTRY(sEditFI.NrDimWindows));
  windows = (Uint32)atoi(entry);

  number = planes * frames * gates * beds * windows;
  if (number == my.fi->number) {
    my.fi->dim[3] = planes;
    my.fi->dim[4] = frames;
    my.fi->dim[5] = gates;
    my.fi->dim[6] = beds;
    my.fi->dim[7] = windows;
  }else{
    XMdcDisplayWarn("Incorrect dimensions not applied");
  }

  /* set proper dim[0] */
  for (i=7; i>=3; i--) {
     if (my.fi->dim[i] > 1) {
       my.fi->dim[0] = (Int16) i;
       i=1; /* last found, so leave */
     }
  }


  /* Study Parameters */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.Reconstructed))) {
    my.fi->reconstructed = MDC_YES;
  }else{
    my.fi->reconstructed = MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.Planar))) {
    my.fi->planar = MDC_YES;
  }else{
    my.fi->planar = MDC_NO;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.ModalityNM))) {
    sEditFI.CurModality = M_NM;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.ModalityPT))) {
    sEditFI.CurModality = M_PT;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.ModalityCT))) {
    sEditFI.CurModality = M_CT;
  }else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.ModalityMR))) {
    sEditFI.CurModality = M_MR;
  }

  for (i=0; i < MDC_MAX_ACQUISITIONS; i++) {
     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sEditFI.AcquisitionType[i]))) {
       my.fi->acquisition_type = (Int16) i; break;
     }
  }

  /* reset other data structs */
  msg = MdcResetODs(my.fi);
  if (msg != NULL) {
    XMdcDisplayFatalErr(MDC_BAD_CODE,msg);
    return;
  }

  /* fill in FI struct */

  my.fi->modality = sEditFI.CurModality;

  if (my.fi->pixdim[0] < 4) my.fi->pixdim[0] = 4; /* at least */
  my.fi->pixdim[1] = pixel_size;
  my.fi->pixdim[2] = pixel_size;
  my.fi->pixdim[3] = slice_width;
  my.fi->pixdim[4] = frame_duration;

  /* fill in IMG_DATA structs */
  for (i=0; i<my.fi->number; i++) {
     id = &my.fi->image[i];

     id->pixel_xsize = pixel_size;
     id->pixel_ysize = pixel_size;
     id->slice_width = slice_width;
     id->slice_spacing = slice_spacing;

     MdcFillImgPos(my.fi,i,i%my.fi->dim[3],0.0);
     MdcFillImgOrient(my.fi,i);
  }

  /* fill DYNAMIC_DATA structs */
  for (i=0; i<my.fi->dynnr; i++) {
     dd = &my.fi->dyndata[i];

     dd->nr_of_slices = my.fi->dim[3];
     dd->time_frame_duration = frame_duration;
  }

  /* some final completions */
  msg = MdcImagesPixelFiddle(my.fi);
  if (msg != NULL) {
    XMdcDisplayFatalErr(MDC_BAD_CODE,msg);
    return;
  }

  /* reframe images */
  XMdcProgressBar(MDC_PROGRESS_BEGIN,0.,"Reframe images:");

  XMdcViewerHide();
  XMdcViewerEnableAutoShrink();
  XMdcViewerReset();
  XMdcDisplayImages();

  XMdcProgressBar(MDC_PROGRESS_END,0.,NULL);

  XMDC_FILE_TYPE = XMDC_EDITFI;

}

void XMdcEditFileInfo(void)
{
   GtkWidget *window=NULL;
   GtkWidget *box1;
   GtkWidget *box2;
   GtkWidget *box3;
   GtkWidget *box4;
   GtkWidget *frame;
   GtkWidget *top, *left, *right;
   GtkWidget *label;
   GtkWidget *grid;
   GtkWidget *tablabel;
   GtkWidget *entry;
   GtkWidget *button;
   GtkWidget *separator;
   GtkWidget *notebook;
   GSList *group;
   EditFileInfoStruct *vifi = &sEditFI;
   int i;

   if (XMdcNoFileOpened()) return;

   window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

   g_signal_connect(window, "destroy",
                      G_CALLBACK(gtk_widget_destroy),NULL);

   gtk_window_set_title(GTK_WINDOW(window),"Edit FileInfo");
   gtk_container_set_border_width(GTK_CONTAINER(window),0);

   box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_container_add(GTK_CONTAINER(window),box1);
   gtk_container_set_border_width(GTK_CONTAINER(box1),5);
   gtk_widget_show(box1);

   label = gtk_label_new(
         " \n** any change can seriously damage study integrity **\n ");
   gtk_widget_set_name(label, "WarningLabel");
   gtk_widget_set_halign(label, GTK_ALIGN_CENTER);
   gtk_widget_set_valign(label, GTK_ALIGN_CENTER);
   gtk_box_pack_start(GTK_BOX(box1),label,TRUE,TRUE,0);
   gtk_widget_show(label);

   notebook = gtk_notebook_new();
   gtk_container_add(GTK_CONTAINER(box1),notebook);
   gtk_container_set_border_width(GTK_CONTAINER(notebook), 10);
   gtk_widget_show(notebook);

   /* tab page Patient Slice Orientation */
   box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
   gtk_widget_show(box2);

   tablabel = gtk_label_new("Orientation");
   gtk_widget_show(tablabel);
   gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

   box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_container_add(GTK_CONTAINER(box2),box3);
   gtk_container_set_border_width(GTK_CONTAINER(box3),5);
   gtk_widget_show(box3);

   grid = gtk_grid_new();
   gtk_container_add(GTK_CONTAINER(box3),grid);
   gtk_widget_show(grid);

   top = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_grid_attach(GTK_GRID(grid),top,0,0,1,1);
   gtk_widget_show(top);

   left = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_grid_attach(GTK_GRID(grid),left,0,1,1,1);
   gtk_widget_show(left);

   right = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_grid_attach(GTK_GRID(grid),right,1,1,1,1);
   gtk_widget_show(right);


   button = NULL; group = NULL;
   for (i=0; i < MDC_MAX_ORIENT; i++) {

      if (i == 0) {
        button = gtk_radio_button_new_with_label(NULL,MdcGetStrPatSlOrient(i));
      }else{
        group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
        button = gtk_radio_button_new_with_label(group,MdcGetStrPatSlOrient(i));
      }

      if (i == 0) {
        /* unknown singled on top */
        gtk_box_pack_start(GTK_BOX(top),button,TRUE,TRUE,0);
      }else if (i <= MDC_MAX_ORIENT/2) {
        /* all supine left */
        gtk_box_pack_start(GTK_BOX(left),button,TRUE,TRUE,0);
      }else{
        /* all prone right */
        gtk_box_pack_start(GTK_BOX(right),button,TRUE,TRUE,0);
      }

      if (my.fi->pat_slice_orient == i) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
      }else{
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), FALSE);
      }
      gtk_widget_show(button);

      vifi->PatSliceOrient[i] = button;
   }

   /* tab page Sizes */
   box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
   gtk_widget_show(box2);

   tablabel = gtk_label_new("Sizes / Time");
   gtk_widget_show(tablabel);
   gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

   label = gtk_label_new("Note: Following entries require float values\n\
       Examples: 10.0  1.0e+1");

   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_LEFT);
   gtk_widget_set_name (label, "FixedLabel");
   gtk_box_pack_start(GTK_BOX(box2),label,TRUE,TRUE,5);
   gtk_widget_show(label);

   box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_container_add(GTK_CONTAINER(box2),box3);
   gtk_container_set_border_width(GTK_CONTAINER(box3),5);
   gtk_widget_show(box3);

   grid = gtk_grid_new();
   gtk_container_add(GTK_CONTAINER(box3),grid);
   gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
   gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
   gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
   gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
   gtk_widget_set_vexpand(grid, TRUE);
   gtk_widget_show(grid);

   label = gtk_label_new("Pixel Size");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,0,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%e",my.fi->image[0].pixel_xsize);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,0,1,1);
   gtk_widget_show(entry);
   vifi->PixelSize = entry;

   label = gtk_label_new("[mm]");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,0,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("Slice Width");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,1,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%e",my.fi->image[0].slice_width);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,1,1,1);
   gtk_widget_show(entry);
   vifi->SliceWidth = entry;

   label = gtk_label_new("[mm]");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,1,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("Slice Separation");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,2,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%e",my.fi->image[0].slice_spacing);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,2,1,1);
   gtk_widget_show(entry);
   vifi->SliceSpacing = entry;

   label = gtk_label_new("[mm]");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,2,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("Frame Duration");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,3,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   if ((my.fi->dynnr > 0) && (my.fi->dyndata != NULL)) {
     sprintf(mdcbufr,"%e",my.fi->dyndata[0].time_frame_duration);
   }else{
     sprintf(mdcbufr,"%e",0.);
   }

   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,3,1,1);
   gtk_widget_show(entry);
   vifi->FrameDuration = entry;

   label = gtk_label_new("[ms]");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,3,1,1);
   gtk_widget_show(label);

   /* tab page Dimensions */
   box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
   gtk_widget_show(box2);

   tablabel = gtk_label_new("Dimensions");
   gtk_widget_show(tablabel);
   gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

   sprintf(mdcbufr,"Note: Each entry must be a 1-based integer and the\n\
  product of dim[]-values = total number of images (%u)",my.fi->number);
   label = gtk_label_new(mdcbufr);
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_LEFT);
   gtk_widget_set_name (label, "FixedLabel");
   gtk_box_pack_start(GTK_BOX(box2),label,TRUE,TRUE,5);
   gtk_widget_show(label);

   box3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_container_add(GTK_CONTAINER(box2),box3);
   gtk_container_set_border_width(GTK_CONTAINER(box3),5);
   gtk_widget_show(box3);

   grid = gtk_grid_new();
   gtk_container_add(GTK_CONTAINER(box3),grid);
   gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
   gtk_grid_set_column_spacing(GTK_GRID(grid), 2);
   gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
   gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
   gtk_widget_set_vexpand(grid, TRUE);
   gtk_widget_show(grid);

   label = gtk_label_new("dim[3] = ");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,0,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%u",my.fi->dim[3]);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,0,1,1);
   gtk_widget_show(entry);
   vifi->NrDimPlanes = entry;

   label = gtk_label_new("(planes | (time) slices)");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,0,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("dim[4] = ");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,1,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%u",my.fi->dim[4]);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,1,1,1);
   gtk_widget_show(entry);
   vifi->NrDimFrames = entry;

   label = gtk_label_new(" (frames | time slots | phases)");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,1,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("dim[5] = ");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,2,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%u",my.fi->dim[5]);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,2,1,1);
   gtk_widget_show(entry);
   vifi->NrDimGates = entry;

   label = gtk_label_new("(gates | R-R intervals)");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,2,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("dim[6] = ");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,3,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%u",my.fi->dim[6]);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,3,1,1);
   gtk_widget_show(entry);
   vifi->NrDimBeds = entry;

   label = gtk_label_new("(beds | detector heads)");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,3,1,1);
   gtk_widget_show(label);

   label = gtk_label_new("dim[7] = ");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,0,4,1,1);
   gtk_widget_show(label);

   entry = gtk_entry_new();
   gtk_entry_set_max_length(GTK_ENTRY(entry), 15);
   sprintf(mdcbufr,"%u",my.fi->dim[7]);
   gtk_entry_set_text(GTK_ENTRY(entry),mdcbufr);
   gtk_editable_select_region(GTK_EDITABLE(entry),0,-1);
   gtk_grid_attach(GTK_GRID(grid),entry,1,4,1,1);
   gtk_widget_show(entry);
   vifi->NrDimWindows = entry;

   label = gtk_label_new("(energy windows)");
   gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_RIGHT);
   gtk_widget_set_name(label, "FixedLabel");
   gtk_grid_attach(GTK_GRID(grid),label,2,4,1,1);
   gtk_widget_show(label);

   /* tab page Study Parameters */
   box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
   gtk_widget_show(box2);

   tablabel = gtk_label_new("Study Parameters");
   gtk_widget_show(tablabel);
   gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box2, tablabel);

   box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,5);
   gtk_container_add(GTK_CONTAINER(box2),box3);
   gtk_container_set_border_width(GTK_CONTAINER(box3),5);
   gtk_box_set_homogeneous(GTK_BOX(box3), TRUE);
   gtk_widget_show(box3);

   frame = gtk_frame_new("Reconstructed");
   gtk_container_add(GTK_CONTAINER(box3),frame);
   gtk_container_set_border_width(GTK_CONTAINER(frame),5);
   gtk_widget_show(frame);

   box4 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,5);
   gtk_container_add(GTK_CONTAINER(frame),box4);
   gtk_container_set_border_width(GTK_CONTAINER(box4),0);
   gtk_widget_show(box4);

   button = gtk_radio_button_new_with_label(NULL,"Yes");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   /* MARK: only save the Yes option */
   if (my.fi->reconstructed == MDC_YES)
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   gtk_widget_show(button);
   vifi->Reconstructed = button;

   group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
   button = gtk_radio_button_new_with_label(group,"No");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   if (my.fi->reconstructed == MDC_NO)
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   gtk_widget_show(button); /* MARK: no need to preserve, only two supported */

   frame = gtk_frame_new("Planar");
   gtk_container_add(GTK_CONTAINER(box3),frame);
   gtk_container_set_border_width(GTK_CONTAINER(frame),5);
   gtk_widget_show(frame);

   box4 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,5);
   gtk_container_add(GTK_CONTAINER(frame),box4);
   gtk_container_set_border_width(GTK_CONTAINER(box4),0);
   gtk_widget_show(box4);

   button = gtk_radio_button_new_with_label(NULL,"Yes");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   /* MARK: only save the Yes option */
   if (my.fi->planar == MDC_YES)
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   gtk_widget_show(button);
   vifi->Planar = button;

   group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
   button = gtk_radio_button_new_with_label(group,"No");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   if (my.fi->planar == MDC_NO)
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   gtk_widget_show(button); /* MARK: no need to preserve, only two supported */

   box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,5);
   gtk_container_add(GTK_CONTAINER(box2),box3);
   gtk_container_set_border_width(GTK_CONTAINER(box3),5);
   gtk_box_set_homogeneous(GTK_BOX(box3), TRUE);
   gtk_widget_set_vexpand(box3, TRUE); /* MARK */
   gtk_widget_show(box3);

   frame = gtk_frame_new("Acquisition Type");
   gtk_container_add(GTK_CONTAINER(box3),frame);
   gtk_container_set_border_width(GTK_CONTAINER(frame),5);
   gtk_widget_show(frame);

   box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_container_add(GTK_CONTAINER(frame),box4);
   gtk_container_set_border_width(GTK_CONTAINER(box4),0);
   gtk_widget_show(box4);

   for (i=0; i < MDC_MAX_ACQUISITIONS; i++) {
      if (i == 0) {
        button = gtk_radio_button_new_with_label(NULL,MdcGetStrAcquisition(i));
      }else{
        group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
        button = gtk_radio_button_new_with_label(group,MdcGetStrAcquisition(i));      }

      gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
      if (my.fi->acquisition_type == i) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
      }else{
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), FALSE);
      }
      gtk_widget_show(button);

      vifi->AcquisitionType[i] = button;
   }

   frame = gtk_frame_new("Modality");
   gtk_container_add(GTK_CONTAINER(box3),frame);
   gtk_container_set_border_width(GTK_CONTAINER(frame),5);
   gtk_widget_show(frame);

   box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL,5);
   gtk_container_add(GTK_CONTAINER(frame),box4);
   gtk_container_set_border_width(GTK_CONTAINER(box4),0);
   gtk_widget_show(box4);

   vifi->CurModality = my.fi->modality;

   button = gtk_radio_button_new_with_label(NULL,"NM");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   /*if (vifi->CurModality == M_NM) {
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   }*/
   gtk_widget_show(button);
   vifi->ModalityNM = button;

   group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
   button = gtk_radio_button_new_with_label(group,"PT");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   /*if (vifi->CurModality == M_PT) {
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   }*/
   gtk_widget_show(button);
   vifi->ModalityPT = button;

   group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
   button = gtk_radio_button_new_with_label(group,"CT");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   /*if (vifi->CurModality == M_CT) {
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   }*/
   gtk_widget_show(button);
   vifi->ModalityCT = button;

   group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
   button = gtk_radio_button_new_with_label(group,"MR");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   /*if (vifi->CurModality == M_MR) {
     gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   }*/
   gtk_widget_show(button);
   vifi->ModalityMR = button;

   group  = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
   button = gtk_radio_button_new_with_label(group,"keep current");
   gtk_box_pack_start(GTK_BOX(box4),button,TRUE,TRUE,0);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
   gtk_widget_show(button);
   vifi->ModalityCurrent = button;

   /* create horizontal separator */
   separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
   gtk_box_pack_start(GTK_BOX(box1),separator,FALSE,FALSE,0);
   gtk_widget_show(separator);

   /* create bottom buttons */
   box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
   gtk_box_pack_start(GTK_BOX(box1),box2,TRUE,TRUE,2);
   gtk_widget_show(box2);

   button = gtk_button_new_with_label("Apply");
   gtk_box_pack_start(GTK_BOX(box2),button,TRUE,TRUE,2);
   g_signal_connect_swapped(button, "clicked",
                      G_CALLBACK(gtk_widget_hide), window);
   g_signal_connect(button, "clicked",
                      G_CALLBACK(XMdcEditFileInfoCallbackApply), NULL);
   g_signal_connect_swapped(button, "clicked",
                      G_CALLBACK(gtk_widget_destroy), window);
   gtk_widget_show(button);

   button = gtk_button_new_with_label("Cancel");
   gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 2);
   g_signal_connect_swapped(button, "clicked",
                      G_CALLBACK(gtk_widget_destroy), window);
   gtk_widget_show(button);

   XMdcShowWidget(window);
}
