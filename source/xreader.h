/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xreader.h                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xreader.c header file                                    *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XREADER_H__
#define __XREADER_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

int XMdcReadFile(char *fname);
void XMdcRawReadFinish(guint otype);
void XMdcRawReadInteractive(char *fname);
void XMdcRawReadPredef(char *fname);
void XMdcRawReadCancel(guint otype);
void XMdcRawReadCallbackCancel(void);
Int16 XMdcGetImageInfoPixelType(void);
void XMdcGetImageInfoCallbackApply(void);
void XMdcGetImageInfo(void);
void XMdcGetHeaderInfoCallbackApply(void);
void XMdcGetHeaderInfo(void);

#endif

