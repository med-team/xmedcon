/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xlabels.h                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xlabels.c header file                                    *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XLABELS_H__
#define __XLABELS_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcGetEcatLabelNumbers(Uint32 realnumber, Uint32 *plane, Uint32 *frame,
                                                Uint32 *gate,  Uint32 *bed);
char *XMdcGetImageLabelIndex(Uint32 nr);
char *XMdcGetImageLabelTimes(Uint32 nr);
void XMdcPrintImageLabelIndex(GtkWidget *widget, Uint32 nr);
void XMdcPrintImageLabelTimes(GtkWidget *widget, Uint32 nr);
void XMdcLabelSelCallbackApply(GtkWidget *widget, gpointer data);
void XMdcUnsensitiveColNumFrames(GtkWidget *widget, gpointer data);
void XMdcSensitiveColNumFrames(GtkWidget *widget, gpointer data);
void XMdcLabelSel(void);

#endif

