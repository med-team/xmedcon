/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xmedcon.h                                                     *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xmedcon.c header file                                    *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XMEDCON_H__
#define __XMEDCON_H__

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "medcon.h"
#include "xicons.h"
#include "xerror.h"
#include "xprogbar.h"
#include "xfilesel.h"
#include "xmnuftry.h"
#include "xdefs.h"
#include "xfiles.h"
#include "xreset.h"
#include "xreader.h"
#include "xwriter.h"
#include "xoptions.h"
#include "xviewer.h"
#include "ximages.h"
#include "xzoom.h"
#include "xinfo.h"
#include "xlabels.h"
#include "xcolmap.h"
#include "xcolgbc.h"
#include "xresize.h"
#include "xrender.h"
#include "xpages.h"
#include "xfancy.h"
#include "xhelp.h"
#include "xutils.h"
#include "xextract.h"
#include "xreslice.h"
#include "xtransf.h"
#include "xvifi.h"

#endif

