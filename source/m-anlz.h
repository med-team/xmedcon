/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-anlz.h                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-anlz.c header file                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_ANLZ_H__
#define __M_ANLZ_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

#define MDC_ANLZ_SIG   'r'

#define MDC_ANLZ_MAX_DIMS             8  /* maximum number of dimensions */

/* datatypes */
#define MDC_ANLZ_DT_UNKNOWN           0
#define MDC_ANLZ_DT_BINARY            1  /* 1-bit     */
#define MDC_ANLZ_DT_UNSIGNED_CHAR     2  /* Uint8     */
#define MDC_ANLZ_DT_SIGNED_SHORT      4  /* Int16     */
#define MDC_ANLZ_DT_SIGNED_INT        8  /* Int32     */
#define MDC_ANLZ_DT_FLOAT            16  /* float     */
#define MDC_ANLZ_DT_COMPLEX          32  /* 2 x float */ /* unsupported */
#define MDC_ANLZ_DT_DOUBLE           64  /* double    */
#define MDC_ANLZ_DT_RGB             128  /* 3 x Uint8 */ /* unsupported */
#define MDC_ANLZ_DT_ALL             255  /* All       */ /* unsupported */

/* orient types */
#define MDC_ANLZ_TRANS_UNFLIPPED      0
#define MDC_ANLZ_CORON_UNFLIPPED      1
#define MDC_ANLZ_SAGIT_UNFLIPPED      2
#define MDC_ANLZ_TRANS_FLIPPED        3
#define MDC_ANLZ_CORON_FLIPPED        4
#define MDC_ANLZ_SAGIT_FLIPPED        5

typedef struct Header_Key_t {
        Int32  sizeof_hdr;               /* 348 or 148                 */
        char data_type[10];              /* "dsr"                      */
        char db_name[18];                /* filename without extension */
        Int32 extents;
        Int16 session_error;
        char regular;                    /* 'r'                        */
        char hkey_un0;
} MDC_ANLZ_HEADER_KEY;

#define MDC_ANLZ_HK_SIZE 40

typedef struct Image_Dimensions_t {
        Int16 dim[MDC_ANLZ_MAX_DIMS];    /* [0] = # of dimensions      */
                                         /* [1] = X-dim                */
                                         /* [2] = Y-dim                */
                                         /* [3] = Z-dim                */
                                         /* [4] = t-dim                */
                                         /* ...                        */
        char avw_vox_units[4];           /* AVW real world dim units   */
        char avw_cal_units[8];           /* AVW real world pix units   */
        Int16 unused1;
        Int16 datatype;                  /* pixel type                 */
                                         /*  0 = Unknown   1 = one-bit */
                                         /*  2 = Uint8     4 = Int16   */
                                         /*  8 = Int32    16 = float   */
                                         /* 32 = complex  64 = double  */

        Int16 bitpix;                    /* bits per pixel             */
        Int16 dim_un0;
        float pixdim[MDC_ANLZ_MAX_DIMS]; /* [0] = # of dimensions      */
                                         /* [1] = X-dim (mm)           */
                                         /* [2] = Y-dim (mm)           */
                                         /* [3] = Z-dim (mm)           */
                                         /* [4] = t-dim (ms)           */
                                         /* ...                        */
        float avw_vox_offset;            /* AVW offset to pixel data   */
        float spm_pix_rescale;           /* SPM pixel rescale factor   */
        float funused1;
        float funused2;
        float avw_cal_max;               /* AVW max calibrated values  */
        float avw_cal_min;               /* AVW min calibrated values  */
        float compressed;
        float verified;
        Int32 glmax,glmin;
} MDC_ANLZ_IMAGE_DIMS;

#define MDC_ANLZ_IMD_SIZE 108

typedef struct Data_History_t {
        char descrip[80];
        char aux_file[24];
        char orient;                     /* patient orientation        */
                                         /* 0 = transverse unflipped   */
                                         /* 1 = coronal unflipped      */
                                         /* 2 = sagittal unflipped     */
                                         /* 3 = transverse flipped     */
                                         /* 4 = coronal flipped        */
                                         /* 5 = sagittal flipped       */

        char originator[10];
        char generated[10];
        char scannum[10];
        char patient_id[10];
        char exp_date[10];
        char exp_time[10];
        char hist_un0[3];
        Int32 views;
        Int32 vols_added;
        Int32 start_field;
        Int32 field_skip;
        Int32 omax, omin;
        Int32 smax, smin;
} MDC_ANLZ_DATA_HIST;

#define MDC_ANLZ_DH_SIZE 200


typedef struct MdcSpmOpt_t {
        Int16 origin_x;
        Int16 origin_y;
        Int16 origin_z;
        float offset;
} MDC_SPMOPT;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckANLZ(FILEINFO *fi);
const char *MdcReadANLZ(FILEINFO *fi);
const char *MdcWriteANLZ(FILEINFO *fi);
int MdcWriteHeaderKey(FILEINFO *fi);
int MdcWriteImageDimension(FILEINFO *fi, MDC_SPMOPT *opt);
int MdcWriteDataHistory(FILEINFO *fi, MDC_SPMOPT *opt);
char *MdcWriteImagesData(FILEINFO *fi);
void MdcGetSpmOpt(FILEINFO *fi, MDC_SPMOPT *opt);

#endif

