/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-acr.h                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-acr.c header file                                      *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_ACR_H__
#define __M_ACR_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/


#define MDC_ACR_TAG_SIZE 8  /* length of the group, element, length fields */

#define MDC_MAX_CHARS  100  /* our PrintTag limit */

#define MDC_SIZE_E0000  12  /* size of element 0x0000 in all groups   */
#define MDC_SIZE_E0001  12  /* size of element 0x0001 in group 0x0008 */

/* basic structure of each ACR-NEMA format */
typedef struct MdcAcrTag_t{
  Uint16 group;
  Uint16 element;
  Uint32  length;
  Uint8   *data;
} MDC_ACR_TAG;

typedef struct MdcSeqTag_T{
  Uint16 group;
  Uint16 element;
} MDC_SEQ_TAG;

/* things for the DICOM reader */

#define MDC_VECT_ENERGYWINDOW  0   /* 0x0054:0x0010 */
#define MDC_VECT_DETECTOR      1   /* 0x0054:0x0020 */
#define MDC_VECT_PHASE         2   /* 0x0054:0x0030 */
#define MDC_VECT_ROTATION      3   /* 0x0054:0x0050 */
#define MDC_VECT_RRINTERVAL    4   /* 0x0054:0x0060 */
#define MDC_VECT_TIMESLOT      5   /* 0x0054:0x0070 */
#define MDC_VECT_SLICE         6   /* 0x0054:0x0080 */
#define MDC_VECT_ANGULARVIEW   7   /* 0x0054:0x0090 */
#define MDC_VECT_TIMESLICE     8   /* 0x0054:0x0100 */

#define MDC_VECT_TOTAL         9   /* last + 1 */

typedef struct MdcDicomStuff_t {

  MDC_MODALITY modality;
  Int8 INVERT;
  Int16 sign, type;
  /* window / rescale */
  float si_slope;
  float si_intercept;
  /* vectors */
  Int8   VectDO[MDC_VECT_TOTAL];
  Uint16 VectNR[MDC_VECT_TOTAL];
  Uint32 acqnr, dynnr;
  /* detector */
  Int16 motion;
  /* gated stuff */
  float timeslottime, frametime, framestart, frameduration, nrframes;
  float window_low, window_high, scan_arc;
  float intervals_acquired, intervals_rejected;
  Int16 heart_rate;
  /* mosaic images */
  Int8   MOSAIC;
  Int8   mosaic_interlaced;
  Uint32 mosaic_width;
  Uint32 mosaic_height;
  Uint32 mosaic_number;
  /* transfer syntax */
  Int8   ENCAPSULATED_UNCOMPRESSED;

}MDC_DICOM_STUFF_T;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckACR(FILEINFO *fi);
void MdcSwapTag(MDC_ACR_TAG *tag);
int MdcFindAcrInfo(FILEINFO *fi, off_t filesize, off_t *BeginAddress);
int MdcGetAcrInfo(FILEINFO *fi, off_t filesize, off_t offset);
char *MdcHackACR(FILEINFO *fi);
const char *MdcReadACR(FILEINFO *fi);
void MdcPrintTag(FILEINFO *fi, MDC_ACR_TAG *tag, char *fmt, ...);
int MdcGetStrVM(char *dest,char *src, Uint32 nr);
void MdcDicomInitStuff(MDC_DICOM_STUFF_T *dicom);
void MdcDicomCheckVect(MDC_DICOM_STUFF_T *dicom, MDC_ACR_TAG *tag, int VECTOR);
Uint32 MdcDicomNrOfVect(MDC_DICOM_STUFF_T *dicom, Uint16 nr, int VECTOR);
int MdcDicomDoAcqData(FILEINFO *fi, MDC_DICOM_STUFF_T *dicom);
int MdcDicomSOPClass(char *sopclass);
int MdcGetHHMMSS(char *time, Int16 *hour, Int16 *minute, Int16 *second);
char *MdcDoTag(MDC_SEQ_TAG *seq, MDC_ACR_TAG *tag, FILEINFO *fi, Uint32 index);
void MdcPutGroupLength(FILE *fp,Uint16 group, off_t gbegin);
int MdcPutTag(FILE *fp,Uint16 group,Uint16 elem,Uint32 length,Uint8 *data);
int MdcPutGroup(FILEINFO *fi, Uint16 group, Uint32 img);
const char *MdcWriteACR(FILEINFO *fi);

#endif

