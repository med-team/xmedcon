/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xfilesel.c                                                      *
 *                                                                           *
 * UTIL C-source: Medical Image Conversion Utility                           *
 *                                                                           *
 * purpose      : file selection routines                                    *
 *                                                                           *
 * project      : (X)MedCon by Erik Nolf                                     *
 *                                                                           *
 * Note         : basic code extracted from Gtk+ tutorial                    *
 *                                                                           *
 * Functions    : XMdcFileSelOpenOk()                 - Open file Ok         *
 *                XMdcFileSelOpen()                   - Open file selection  *
 *                XMdcFileSelSaveSetDefaultName()     - Set Default name     *
 *                XMdcFileSelSaveCreateFormatMenu()   - Create Format menu   *
 *                XMdcFileSelSaveCreateBtnAlias()     - Create Alias button  *
 *                XMdcFileSelSaveCreateBtnDefault()   - Create Default button*
 *                XMdcFileSelSaveCallbackFormatMenu() - Get output format    *
 *                XMdcFileSelSaveCallbackAlias()      - Get alias   filename *
 *                XMdcFileSelSaveCallbackDefault()    - Get default filename *
 *                XMdcFileSelSaveCancel()             - Save file Cancel     *
 *                XMdcFileSelSaveOk()                 - Save file Ok         *
 *                XMdcFileSelSave()                   - Save file selection  *
 *                XMdcLutSelOpenOk()                  - LUT Open Ok          *
 *                XMdcLutSelOpen()                    - LUT open file        *
 *                XMdcRawPredefSelSaveOk()            - Raw Predef Save Ok   *
 *                XMdcRawPredefSelSave()              - Raw Predef Save      *
 *                XMdcRawPredefSelOpenOk()            - Raw Predef Load Ok   *
 *                XMdcRawPredefSelOpen()              - Raw Predef Load      *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "m-depend.h"

#include <stdio.h>
#ifdef LIBMDC_HAVE_STRING_H
#include <string.h>
#endif
#ifdef LIBMDC_HAVE_STRINGS_H
#ifndef _WIN32
#include <strings.h>
#endif
#endif

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

static gint format_to_menu[MDC_MAX_FRMTS], menu_to_format[MDC_MAX_FRMTS];
static guint OPEN_TYPE=XMDC_NORMAL;
static GtkWidget *ofilew=NULL, *sfilew=NULL, *olutw=NULL, *formatmenu=NULL;
static GtkWidget *opredefw=NULL, *spredefw=NULL;
static GtkWidget *default_name=NULL;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

/* Get the selected filename and display */
void XMdcFileSelOpenOk(GtkWidget *fs)
{
    GtkFileChooser *chooser = GTK_FILE_CHOOSER(fs);
    char *fname;

    fname = gtk_file_chooser_get_filename(chooser);

    if (OPEN_TYPE == XMDC_RAW) {

      XMdcRawReadInteractive(fname);

    }else if(OPEN_TYPE == XMDC_PREDEF) {

      XMdcRawReadPredef(fname);

    }else{

      XMdcMainWidgetsInsensitive();

      XMdcDisplayFile(fname);

      XMdcMainWidgetsResensitive();

   }

   g_free(fname);

}

void XMdcFileSelOpen(GtkWidget *widget, guint otype)
{
    gint response;

    OPEN_TYPE = otype;

    if (ofilew == NULL) {

      /* Create a new file selection widget */
      ofilew = gtk_file_chooser_dialog_new ("Open File",
                                             NULL,
                                             GTK_FILE_CHOOSER_ACTION_OPEN,
                                             "Open File", GTK_RESPONSE_ACCEPT,
                                             "Cancel", GTK_RESPONSE_CANCEL,
                                             NULL);
    }

    if (OPEN_TYPE == XMDC_RAW) {
      gtk_window_set_title(GTK_WINDOW(ofilew),"Open RAW File (interactive)");
    }else if (OPEN_TYPE == XMDC_PREDEF) {
      gtk_window_set_title(GTK_WINDOW(ofilew),"Open RAW File (predefined)");
    }else{
      gtk_window_set_title(GTK_WINDOW(ofilew),"Open File");
    }

    switch (XMDC_FILE_TYPE) {
      case XMDC_RAW    : XMdcDisplayWarn("RAW file was not saved");
          break;
      case XMDC_PREDEF : XMdcDisplayWarn("RAW file was not saved");
          break;
      case XMDC_EXTRACT: XMdcDisplayWarn("Extracted images were not saved");
          break;
      case XMDC_RESLICE: XMdcDisplayWarn("Resliced images not saved");
          break;
      case XMDC_TRANSF : XMdcDisplayWarn("Transformed images not saved");
          break;
      case XMDC_EDITFI : XMdcDisplayWarn("Changed FileInfo not saved");
          break;
    }

    response = gtk_dialog_run(GTK_DIALOG(ofilew)); 

    gtk_widget_hide(ofilew);

    if (response == GTK_RESPONSE_ACCEPT) {

      XMdcFileSelOpenOk(ofilew);

    }

}

void XMdcFileSelSaveSetDefaultName(GtkWidget *fs)
{

  MdcDefaultName(my.fi,XMDC_WRITE_FRMT,my.fi->ofname,my.fi->ifname);

  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(fs),my.fi->ofname);

}

GtkWidget *XMdcFileSelSaveCreateFormatMenu(GtkWidget *fs, guint format)
{
    gint item=0, selected=0;
    Uint8 LOOK=MDC_YES;

    formatmenu = gtk_combo_box_text_new();

    memset(format_to_menu,0,sizeof(format_to_menu));
    memset(menu_to_format,0,sizeof(menu_to_format));

/* format Raw Binary */
    if (LOOK && (format == MDC_FRMT_RAW)) {
      XMDC_WRITE_FRMT = MDC_FRMT_RAW; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_RAW]=item;
    menu_to_format[item]=MDC_FRMT_RAW;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_RAW]);
    item++;
/* format Raw Ascii */
    if (LOOK && (format == MDC_FRMT_ASCII)) {
     XMDC_WRITE_FRMT  = MDC_FRMT_ASCII; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_ASCII]=item;
    menu_to_format[item]=MDC_FRMT_ASCII;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_ASCII]);
    item++;
#if MDC_INCLUDE_ACR
    if (LOOK && (format == MDC_FRMT_ACR)) {
      XMDC_WRITE_FRMT = MDC_FRMT_ACR; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_ACR]=item;
    menu_to_format[item]=MDC_FRMT_ACR;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_ACR]);
    item++;
#endif
#if MDC_INCLUDE_ANLZ
    if (LOOK && (format == MDC_FRMT_ANLZ)) {
      XMDC_WRITE_FRMT = MDC_FRMT_ANLZ; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_ANLZ]=item;
    menu_to_format[item]=MDC_FRMT_ANLZ;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_ANLZ]);
    item++;
#endif
#if MDC_INCLUDE_CONC
    if (LOOK && (format == MDC_FRMT_CONC)) {
      XMDC_WRITE_FRMT = MDC_FRMT_CONC; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_CONC]=item;
    menu_to_format[item]=MDC_FRMT_CONC;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_CONC]);
    item++;
#endif
#if MDC_INCLUDE_DICM
    if (LOOK && (format == MDC_FRMT_DICM)) {
      XMDC_WRITE_FRMT = MDC_FRMT_DICM; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_DICM]=item;
    menu_to_format[item]=MDC_FRMT_DICM;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_DICM]);
    item++;
#endif
#if MDC_INCLUDE_ECAT
    if (LOOK && (format == MDC_FRMT_ECAT6)) {
      XMDC_WRITE_FRMT = MDC_FRMT_ECAT6; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_ECAT6]=item;
    menu_to_format[item]=MDC_FRMT_ECAT6;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_ECAT6]);
    item++;
  #if MDC_INCLUDE_TPC
    if (LOOK && (format == MDC_FRMT_ECAT7)) {
      XMDC_WRITE_FRMT = MDC_FRMT_ECAT7; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_ECAT7]=item;
    menu_to_format[item]=MDC_FRMT_ECAT7;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_ECAT7]);
    item++;
  #endif
#endif
#if MDC_INCLUDE_GIF
    if (LOOK && (format == MDC_FRMT_GIF)) {
      XMDC_WRITE_FRMT = MDC_FRMT_GIF; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_GIF]=item;
    menu_to_format[item]=MDC_FRMT_GIF;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_GIF]);
    item++;
#endif
#if MDC_INCLUDE_INTF
    if (LOOK && (format == MDC_FRMT_INTF)) {
      XMDC_WRITE_FRMT = MDC_FRMT_INTF; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_INTF]=item;
    menu_to_format[item]=MDC_FRMT_INTF;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_INTF]);
    item++;
#endif
#if MDC_INCLUDE_INW
    if (LOOK && (format == MDC_FRMT_INW)) {
      XMDC_WRITE_FRMT = MDC_FRMT_INW; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_INW]=item;
    menu_to_format[item]=MDC_FRMT_INW;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_INW]);
    item++;
#endif
#if MDC_INCLUDE_NIFTI
    if (LOOK && (format == MDC_FRMT_NIFTI)) {
      XMDC_WRITE_FRMT = MDC_FRMT_NIFTI; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_NIFTI]=item;
    menu_to_format[item]=MDC_FRMT_NIFTI;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_NIFTI]);
    item++;
#endif
#if MDC_INCLUDE_PNG
    if (LOOK && (format == MDC_FRMT_PNG)) {
      XMDC_WRITE_FRMT = MDC_FRMT_PNG; LOOK = MDC_NO; selected = item;
    }
    format_to_menu[MDC_FRMT_PNG]=item;
    menu_to_format[item]=MDC_FRMT_PNG;
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(formatmenu), FrmtString[MDC_FRMT_PNG]);
    item++;
#endif

    if (LOOK) XMdcDisplayFatalErr(MDC_BAD_CODE,"Unexpected format to save");

    gtk_combo_box_set_active(GTK_COMBO_BOX(formatmenu), selected);
    g_signal_connect(formatmenu, "changed"
		               , G_CALLBACK(XMdcFileSelSaveCallbackFormatMenu)
			       , NULL);
    gtk_widget_show(formatmenu);

    return(formatmenu);

}

GtkWidget *XMdcFileSelSaveCreateBtnAlias(GtkWidget *fs)
{
  GtkWidget *button;

  button = gtk_button_new_with_label("Alias Name");
  g_signal_connect_swapped(button, "clicked",
     G_CALLBACK(XMdcFileSelSaveCallbackAlias), fs);
  gtk_widget_show(button);

  return(button);

}

GtkWidget *XMdcFileSelSaveCreateBtnDefault(GtkWidget *fs)
{
  GtkWidget *button;

  button = gtk_button_new_with_label("Default Name");
  g_signal_connect_swapped(button, "clicked",
     G_CALLBACK(XMdcFileSelSaveCallbackDefault), fs);
  gtk_widget_show(button);

  return(button);

}

void XMdcFileSelSaveCallbackFormatMenu(GtkWidget *widget, gpointer data)
{
   gint index;

   index = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
   XMDC_WRITE_FRMT = menu_to_format[index];

   XMdcFileSelSaveSetDefaultName(sfilew);
}

void XMdcFileSelSaveCallbackAlias(GtkWidget *fs, char *filename)
{
  Int8 prev = MDC_ALIAS_NAME;

  MDC_ALIAS_NAME = MDC_YES;

  MdcDefaultName(my.fi,XMDC_WRITE_FRMT,my.fi->ofname,my.fi->ifname);

  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(fs),my.fi->ofname);

  MDC_ALIAS_NAME = prev;
}

void XMdcFileSelSaveCallbackDefault(GtkWidget *fs, char *filename)
{
  MdcDefaultName(my.fi,XMDC_WRITE_FRMT,my.fi->ofname,my.fi->ifname);
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(fs),my.fi->ofname);
}

void XMdcFileSelSaveCancel(GtkWidget *fs)
{
   write_counter-=1;
}

void XMdcFileSelSaveOk(GtkWidget *fs)
{

  strcpy(my.fi->opath,gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fs)));

   MdcSplitPath(my.fi->opath,my.fi->odir,my.fi->ofname);
   if (my.fi->ofname[0] == '\0') {
     XMdcDisplayErr("No file specified");
     write_counter-=1;
     return;
   }
   MdcMergePath(my.fi->opath,my.fi->odir,my.fi->ofname);

   if (XMdcWriteFile(XMDC_WRITE_FRMT)) {
     if (XMDC_FILE_TYPE >= XMDC_UNSAVED) XMDC_FILE_TYPE = XMDC_SAVED;
   }

   XMdcProgressBar(MDC_PROGRESS_END,0.,NULL);

}

void XMdcFileSelSave(GtkWidget *widget, guint format)
{
   GtkWidget *options;
   GtkWidget *menu;
   GtkWidget *btnalias;
   GtkWidget *btndefault;

   gint index, response;

   if (XMdcNoFileOpened()) return;

   MdcPrefix((signed)write_counter++);

   if (sfilew == NULL) {

     /* first call "Save": no format set before */
     /* so we use default value this first time */
     if (format == MDC_MAX_FRMTS) format = XMDC_DEFAULT_FRMT;

     /* Create a new file selection widget */
     sfilew = gtk_file_chooser_dialog_new("Save File",
                                           NULL,
                                           GTK_FILE_CHOOSER_ACTION_SAVE,
                                           "Save File", GTK_RESPONSE_ACCEPT,
                                           "Cancel", GTK_RESPONSE_CANCEL,
                                           NULL);

     options = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);

     menu = XMdcFileSelSaveCreateFormatMenu(sfilew,format);
     gtk_box_pack_start(GTK_BOX(options), menu, TRUE, TRUE, 0);

     btnalias = XMdcFileSelSaveCreateBtnAlias(sfilew);
     gtk_box_pack_start(GTK_BOX(options), btnalias, TRUE, TRUE, 0);

     btndefault = XMdcFileSelSaveCreateBtnDefault(sfilew);
     gtk_box_pack_start(GTK_BOX(options), btndefault, TRUE, TRUE, 0);
     default_name = btnalias; /* keep widget available */

     gtk_widget_show(options);

     gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(sfilew), options);

   }else{

     if (format == MDC_MAX_FRMTS) {

       /* Save: use previous selected format */
       index = gtk_combo_box_get_active(GTK_COMBO_BOX(formatmenu));
       gtk_combo_box_set_active(GTK_COMBO_BOX(formatmenu), index);

     }else{

       /* Save As: activate given format */
       index = format_to_menu[format];
       gtk_combo_box_set_active(GTK_COMBO_BOX(formatmenu), index);

     }

  }

  if (MDC_ALIAS_NAME == MDC_YES) gtk_widget_hide(default_name);
  else gtk_widget_show(default_name);

  XMdcFileSelSaveSetDefaultName(sfilew);

  response = gtk_dialog_run(GTK_DIALOG(sfilew));

  gtk_widget_hide(sfilew);

  if (response == GTK_RESPONSE_ACCEPT) {
    XMdcFileSelSaveOk(sfilew);
  }else{
    XMdcFileSelSaveCancel(sfilew);
  }
}


void XMdcLutSelOpenOk(GtkWidget *fs)
{
   char *fname;

   fname = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fs));

   XMdcLoadLUT(fname);

   g_free(fname);

}

void XMdcLutSelOpen(void)
{
   GtkFileFilter *filter;
   gint response;

   if (olutw == NULL) {

     /* Create a new file selection widget */
     olutw = gtk_file_chooser_dialog_new("Select LUT File",
                                          NULL,
                                          GTK_FILE_CHOOSER_ACTION_OPEN,
                                          "Open LUT", GTK_RESPONSE_ACCEPT,
                                          "Cancel", GTK_RESPONSE_CANCEL,
                                          NULL);

     filter = gtk_file_filter_new();
     gtk_file_filter_set_name(filter, "*.lut");
     gtk_file_filter_add_pattern(filter, "*.lut");
     gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(olutw), filter);

   }

   /* Lets set the filename, as if this were a save dialog, and we are giving
    *       a default filename */
   if (XMEDCONLUT != NULL) {
     strncpy(xmdcstr,XMEDCONLUT,MDC_1KB_OFFSET); xmdcstr[MDC_1KB_OFFSET]='\0';

     if (xmdcstr[strlen(xmdcstr)-1] != MDC_PATH_DELIM_CHR)
       strcat(xmdcstr,MDC_PATH_DELIM_STR);

     gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(olutw),xmdcstr);
   }else{
     /* installation dir */
     gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(olutw),XMDCLUT);
   }

   response = gtk_dialog_run(GTK_DIALOG(olutw));

   gtk_widget_hide(olutw);

   if (response == GTK_RESPONSE_ACCEPT) {

     XMdcLutSelOpenOk(olutw);

   }

}

void XMdcRawPredefSelSaveOk(GtkWidget *fs)
{
  char *fname;
  char *msg;

  fname = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fs));

  if ((msg = MdcWritePredef(fname)) != NULL) {
    XMdcDisplayWarn("%s",msg);
  }else{
    XMdcDisplayMesg("File successfully written");
  }

  g_free(fname);

}

void XMdcRawPredefSelSave(GtkWidget *widget, gpointer data)
{
  gint response;

  if (spredefw == NULL) {

     /* Create a new file selection widget */
     spredefw = gtk_file_chooser_dialog_new ("Save Raw Predef File",
                                              NULL,
                                              GTK_FILE_CHOOSER_ACTION_SAVE,
                                              "Save Predef", GTK_RESPONSE_ACCEPT,
                                              "Cancel", GTK_RESPONSE_CANCEL,
                                              NULL);

  }

  if (XMEDCONRPI != NULL) {
    strncpy(xmdcstr,XMEDCONRPI,MDC_1KB_OFFSET); xmdcstr[MDC_1KB_OFFSET]='\0';

    if (xmdcstr[strlen(xmdcstr)-1] != MDC_PATH_DELIM_CHR)
      strcat(xmdcstr,MDC_PATH_DELIM_STR);

    gtk_file_chooser_set_current_folder_uri(GTK_FILE_CHOOSER(spredefw),xmdcstr);

  }

  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(spredefw),"predef.rpi");

  response = gtk_dialog_run(GTK_DIALOG(spredefw));

  gtk_widget_hide(spredefw);

  if (response == GTK_RESPONSE_ACCEPT) {

    XMdcRawPredefSelSaveOk(spredefw);

  }

}

void XMdcRawPredefSelOpenOk(GtkWidget *fs)
{
  char *fname;
  char *msg;

  fname = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fs));

  if (MdcCheckPredef(fname) == MDC_NO) {
    g_free(fname);
    XMdcDisplayWarn("Invalid raw predef input file");
    return;
  }

  msg = MdcReadPredef(fname);
  if (msg != NULL) XMdcDisplayWarn(msg);

  g_free(fname);

}

void XMdcRawPredefSelOpen(GtkWidget *widget, gpointer data)
{

  GtkFileFilter *filter;
  gint response;

  if (opredefw == NULL) {

    /* Create a new file selection widget */
    opredefw = gtk_file_chooser_dialog_new ("Load Raw Predef File",
                                             NULL,
                                             GTK_FILE_CHOOSER_ACTION_OPEN,
                                             "Load Predef", GTK_RESPONSE_ACCEPT,
                                             "Cancel", GTK_RESPONSE_CANCEL,
                                             NULL);

    filter = gtk_file_filter_new();
    gtk_file_filter_set_name(filter, "*.rpi");
    gtk_file_filter_add_pattern(filter, "*.rpi");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(opredefw), filter);
  }

  if (XMEDCONRPI != NULL) {
    strncpy(xmdcstr,XMEDCONRPI,MDC_1KB_OFFSET); xmdcstr[MDC_1KB_OFFSET]='\0';

    if (xmdcstr[strlen(xmdcstr)-1] != MDC_PATH_DELIM_CHR)
      strcat(xmdcstr,MDC_PATH_DELIM_STR);

    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(opredefw),xmdcstr);

  }

  response = gtk_dialog_run(GTK_DIALOG(opredefw));

  gtk_widget_hide(opredefw);

  if (response == GTK_RESPONSE_ACCEPT) {

    XMdcRawPredefSelOpenOk(opredefw);

  }

}

