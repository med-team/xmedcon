/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xmnuftry.c                                                    *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : manual menu creation factory                             *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcAttachMainMenu()   - Attach main menu to window      *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "xmedcon.h"

/****************************************************************************
                              D E F I N E S
****************************************************************************/

/* hack, need those for value by reference in callback functions, because
 * we can't use #define values; opted to use real #defines for clarity */

/* FileSelOpen */
guint xmdc_file_normal = XMDC_NORMAL;
guint xmdc_file_raw    = XMDC_RAW;
guint xmdc_file_predef = XMDC_PREDEF;

/* FileSelSave */
guint xmdc_frmt_raw   = MDC_FRMT_RAW;
guint xmdc_frmt_ascii = MDC_FRMT_ASCII;
guint xmdc_frmt_acr   = MDC_FRMT_ACR;
guint xmdc_frmt_anlz  = MDC_FRMT_ANLZ;
guint xmdc_frmt_conc  = MDC_FRMT_CONC;
guint xmdc_frmt_dicm  = MDC_FRMT_DICM;
guint xmdc_frmt_ecat6 = MDC_FRMT_ECAT6;
guint xmdc_frmt_ecat7 = MDC_FRMT_ECAT7;
guint xmdc_frmt_gif   = MDC_FRMT_GIF;
guint xmdc_frmt_intf  = MDC_FRMT_INTF;
guint xmdc_frmt_inw   = MDC_FRMT_INW;
guint xmdc_frmt_nifti = MDC_FRMT_NIFTI;
guint xmdc_frmt_png   = MDC_FRMT_PNG;

guint xmdc_max_frmts  = MDC_MAX_FRMTS;

/* Extract */
guint xmdc_extract_normal = MDC_INPUT_NORM_STYLE;
guint xmdc_extract_ecat   = MDC_INPUT_ECAT_STYLE;

/* Reslice */
guint xmdc_reslice_transaxial = MDC_TRANSAXIAL;
guint xmdc_reslice_coronal    = MDC_CORONAL;
guint xmdc_reslice_sagittal   = MDC_SAGITTAL;

/* Transform */
guint xmdc_flip_horizontal    = MDC_TRANSF_HORIZONTAL;
guint xmdc_flip_vertical      = MDC_TRANSF_VERTICAL;
guint xmdc_sort_reverse       = MDC_TRANSF_REVERSE;
guint xmdc_cine_apply         = MDC_TRANSF_CINE_APPLY;
guint xmdc_cine_undo          = MDC_TRANSF_CINE_UNDO;
guint xmdc_matrix_square      = MDC_TRANSF_SQR1;
guint xmdc_matrix_square_pwr2 = MDC_TRANSF_SQR2;

/* Place */
guint xmdc_colmap_place_left  = MDC_LEFT;
guint xmdc_colmap_place_right = MDC_RIGHT;

/* Menubar */
guint xmdc_menubar_ltr = GTK_PACK_DIRECTION_LTR; 
guint xmdc_menubar_rtl = GTK_PACK_DIRECTION_RTL; 
guint xmdc_menubar_ttb = GTK_PACK_DIRECTION_TTB; 
guint xmdc_menubar_btt = GTK_PACK_DIRECTION_BTT; 

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcMenuCbFileOpen(GtkWidget *widget, gpointer data)
{
  guint *otype = (guint *)data;

  XMdcFileSelOpen(widget, *otype);
}

void XMdcMenuCbFileSave(GtkWidget *widget, gpointer data)
{
  guint *format = (guint *)data;

  XMdcFileSelSave(widget, *format);
}

void XMdcMenuCbExtract(GtkWidget *widget, gpointer data)
{
  guint *style = (guint *)data;

  XMdcGetImages(widget, *style);
}

void XMdcMenuCbReslice(GtkWidget *widget, gpointer data)
{
  guint *projection = (guint *)data;

  XMdcResliceImages(widget, *projection);
}

void XMdcMenuCbTransform(GtkWidget *widget, gpointer data)
{
  guint *transformation = (guint *)data;

  XMdcTransformImages(widget, *transformation);
}

void XMdcMenuCbPlace(GtkWidget *widget, gpointer data)
{
  guint *place = (guint *)data;

  XMDC_CMAP_PLACE = *place;

  XMdcApplyMapPlace(XMDC_CMAP_PLACE);
}

void XMdcMenuCbMenubar(GtkWidget *widget, gpointer data)
{
  guint *menubar_pack = (guint *)data;

  g_object_set(my.menubar, "pack-direction", *menubar_pack, NULL);

}

void XMdcAttachMainMenu(GtkWidget *window)
{
  GtkWidget *menu_bar;
  GtkWidget *file_item,
            *file_menu,
              *open_item,
              *open_raw_item,
              *open_raw_menu,
                *interactive_item,
                *predefined_item,
              *separator1,
              *raw_predef_load_item,
              *raw_predef_save_item,
              *separator2,
              *info_show_item,
              *info_edit_item,
              *separator3,
              *save_item,
              *save_as_item,
              *save_as_menu,
                *save_as_raw_bin_item,
                *save_as_raw_ascii_item,
#if MDC_INCLUDE_ACR
                *save_as_acr_item,
#endif
#if MDC_INCLUDE_ANLZ
                *save_as_anlz_item,
#endif
#if MDC_INCLUDE_CONC
                *save_as_conc_item,
#endif
#if MDC_INCLUDE_DICM
                *save_as_dicm_item,
#endif
#if MDC_INCLUDE_ECAT
                *save_as_ecat6_item,
 #if MDC_INCLUDE_TPC
                *save_as_ecat7_item,
 #endif
#endif
#if MDC_INCLUDE_GIF
                *save_as_gif_item,
#endif
#if MDC_INCLUDE_INTF
                *save_as_intf_item,
#endif
#if MDC_INCLUDE_INW
                *save_as_inw_item,
#endif
#if MDC_INCLUDE_NIFTI
                *save_as_nifti_item,
#endif
#if MDC_INCLUDE_PNG
                *save_as_png_item,
#endif
              *separator4,
              *close_item,
              *quit_item,
            *images_item,
            *images_menu,
              *view_item,
              *extract_item,
              *extract_menu,
                *extract_normal_item,
                *extract_ecat_item,
              *reslice_item,
              *reslice_menu,
                *reslice_tra_item,
                *reslice_cor_item,
                *reslice_sag_item,
              *flip_item,
              *flip_menu,
                *flip_horizontal_item,
                *flip_vertical_item,
              *sort_item,
              *sort_menu,
                *reverse_item,
                *cine_item,
                *cine_menu,
                  *cine_apply_item,
                  *cine_undo_item,
              *matrix_item,
              *matrix_menu,
                *square_item,
                *square_pwr2_item,
            *options_item,
            *options_menu,
              *medcon_item,
              *render_item,
              *labels_item,
              *pages_item,
              *resize_item,
              *colormap_item,
              *colormap_menu,
                *colors_item,
                *place_item,
                *place_menu,
                  *place_left_item,
                  *place_right_item,
              *menubar_item,
              *menubar_menu,
                *menubar_ltr_item,
                *menubar_rtl_item,
                *menubar_ttb_item,
                *menubar_btt_item,
            *help_item,
            *help_menu,
              *online_info_item,
              *console_logs_item,
              *about_item;

  GtkWidget *vbox;

  /* vbox to put menu in */
  vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add(GTK_CONTAINER(window), vbox);
  gtk_widget_show(vbox);

  /* menu_bar to hold menus */
  menu_bar = gtk_menu_bar_new();

  /* keep menu_bar available, set default orientation */
  my.menubar = menu_bar;
  gtk_menu_bar_set_pack_direction(GTK_MENU_BAR(my.menubar), XMDC_MENUBAR_PACK);

  /*** create menu items for menu_bar ***/
  file_item     = gtk_menu_item_new_with_label("File");
  images_item   = gtk_menu_item_new_with_label("Images");
  options_item  = gtk_menu_item_new_with_label("Options");
  help_item     = gtk_menu_item_new_with_label("Help");

  /* add menu items to menu_bar, left to right order */
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), file_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), images_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), options_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), help_item);

  /*** create File menu ***/
  file_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(file_item), file_menu);

  open_item      = gtk_menu_item_new_with_label("Open");
  open_raw_item  = gtk_menu_item_new_with_label("Open Raw");
  separator1     = gtk_separator_menu_item_new();
  raw_predef_load_item = gtk_menu_item_new_with_label("Raw Predef Load");
  raw_predef_save_item = gtk_menu_item_new_with_label("Raw Predef Save");
  separator2 = gtk_separator_menu_item_new();
  info_show_item = gtk_menu_item_new_with_label("Info Show");
  info_edit_item = gtk_menu_item_new_with_label("Info Edit");
  separator3 = gtk_separator_menu_item_new();
  save_item      = gtk_menu_item_new_with_label("Save");
  save_as_item   = gtk_menu_item_new_with_label("Save As");
  separator4 = gtk_separator_menu_item_new();
  close_item     = gtk_menu_item_new_with_label("Close");
  quit_item      = gtk_menu_item_new_with_label("Quit");

  /* attach file sub items */
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), open_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), open_raw_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), separator1);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), raw_predef_load_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), raw_predef_save_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), separator2);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), info_show_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), info_edit_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), separator3);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), save_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), save_as_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), separator4);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), close_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), quit_item);

  /* attach file sub callbacks */
  g_signal_connect(open_item, "activate",
        G_CALLBACK(XMdcFileSelOpen), (gpointer) &xmdc_file_normal);

  g_signal_connect(raw_predef_load_item, "activate",
        G_CALLBACK(XMdcRawPredefSelOpen), NULL);

  g_signal_connect(raw_predef_save_item, "activate",
        G_CALLBACK(XMdcRawPredefSelSave), NULL);

  g_signal_connect(info_show_item, "activate",
        G_CALLBACK(XMdcShowFileInfo), NULL);

  g_signal_connect(info_edit_item, "activate",
        G_CALLBACK(XMdcEditFileInfo), NULL);

  g_signal_connect(save_item, "activate",
        G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_max_frmts);

  g_signal_connect(close_item, "activate",
        G_CALLBACK(XMdcCloseFile), NULL);

  g_signal_connect(quit_item, "activate",
        G_CALLBACK(XMdcMedconQuit), NULL);

  /* open raw sub menus */
  open_raw_menu = gtk_menu_new();
  
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(open_raw_item), open_raw_menu);

  interactive_item = gtk_menu_item_new_with_label("Interactive");
  predefined_item  = gtk_menu_item_new_with_label("Predefined");

  gtk_menu_shell_append(GTK_MENU_SHELL(open_raw_menu), interactive_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(open_raw_menu), predefined_item);

  /* attach raw sub callbacks */
  g_signal_connect(interactive_item, "activate",
        G_CALLBACK(XMdcMenuCbFileOpen), (gpointer) &xmdc_file_raw);

  g_signal_connect(predefined_item, "activate",
        G_CALLBACK(XMdcMenuCbFileOpen), (gpointer) &xmdc_file_predef);

  /* save as sub menus */
  save_as_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(save_as_item), save_as_menu);

  save_as_raw_bin_item   = gtk_menu_item_new_with_label("Raw Binary");
  save_as_raw_ascii_item = gtk_menu_item_new_with_label("Raw Ascii");
#if MDC_INCLUDE_ACR
  save_as_acr_item       = gtk_menu_item_new_with_label("Acr/Nema");
#endif
#if MDC_INCLUDE_ANLZ
  save_as_anlz_item      = gtk_menu_item_new_with_label("Analyze");
#endif
#if MDC_INCLUDE_CONC
  save_as_conc_item      = gtk_menu_item_new_with_label("Concorde");
#endif
#if MDC_INCLUDE_DICM
  save_as_dicm_item      = gtk_menu_item_new_with_label("DICOM");
#endif
#if MDC_INCLUDE_ECAT
  save_as_ecat6_item     = gtk_menu_item_new_with_label("Ecat 6");
 #if MDC_INCLUDE_TPC
  save_as_ecat7_item     = gtk_menu_item_new_with_label("Ecat 7");
 #endif
#endif
#if MDC_INCLUDE_GIF
  save_as_gif_item       = gtk_menu_item_new_with_label("Gif89a");
#endif
#if MDC_INCLUDE_INTF
  save_as_intf_item      = gtk_menu_item_new_with_label("InterFile");
#endif
#if MDC_INCLUDE_INW
  save_as_inw_item       = gtk_menu_item_new_with_label("INW (RUG)");
#endif
#if MDC_INCLUDE_NIFTI
  save_as_nifti_item     = gtk_menu_item_new_with_label("NIfTI");
#endif
#if MDC_INCLUDE_PNG
  save_as_png_item       = gtk_menu_item_new_with_label("PNG");
#endif

  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_raw_bin_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_raw_ascii_item);
#if MDC_INCLUDE_ACR
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_acr_item);
#endif
#if MDC_INCLUDE_ANLZ
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_anlz_item);
#endif
#if MDC_INCLUDE_CONC
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_conc_item);
#endif
#if MDC_INCLUDE_DICM
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_dicm_item);
#endif
#if MDC_INCLUDE_ECAT
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_ecat6_item);
 #if MDC_INCLUDE_TPC
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_ecat7_item);
 #endif
#endif
#if MDC_INCLUDE_GIF
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_gif_item);
#endif
#if MDC_INCLUDE_INTF
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_intf_item);
#endif
#if MDC_INCLUDE_INW
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_inw_item);
#endif
#if MDC_INCLUDE_NIFTI
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_nifti_item);
#endif
#if MDC_INCLUDE_PNG
  gtk_menu_shell_append(GTK_MENU_SHELL(save_as_menu), save_as_png_item);
#endif

  /* attach save as callbacks */

  g_signal_connect(save_as_raw_bin_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_raw);

  g_signal_connect(save_as_raw_ascii_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_ascii);
#if MDC_INCLUDE_ACR
  g_signal_connect(save_as_acr_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_acr);
#endif
#if MDC_INCLUDE_ANLZ
  g_signal_connect(save_as_anlz_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_anlz);
#endif
#if MDC_INCLUDE_CONC
  g_signal_connect(save_as_conc_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_conc);
#endif
#if MDC_INCLUDE_DICM
  g_signal_connect(save_as_dicm_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_dicm);
#endif
#if MDC_INCLUDE_ECAT
  g_signal_connect(save_as_ecat6_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_ecat6);
 #if MDC_INCLUDE_TPC
  g_signal_connect(save_as_ecat7_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_ecat7);
 #endif
#endif
#if MDC_INCLUDE_GIF
  g_signal_connect(save_as_gif_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_gif);
#endif
#if MDC_INCLUDE_INTF
  g_signal_connect(save_as_intf_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_intf);
#endif
#if MDC_INCLUDE_INW
  g_signal_connect(save_as_inw_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_inw);
#endif
#if MDC_INCLUDE_NIFTI
  g_signal_connect(save_as_nifti_item, "activate",
         G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_nifti);
#endif
#if MDC_INCLUDE_PNG
  g_signal_connect(save_as_png_item, "activate",
        G_CALLBACK(XMdcMenuCbFileSave), (gpointer) &xmdc_frmt_png);
#endif

  /*** create Images menu ***/
  images_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(images_item), images_menu);

  view_item     = gtk_menu_item_new_with_label("View");
  extract_item  = gtk_menu_item_new_with_label("Extract");
  reslice_item  = gtk_menu_item_new_with_label("Reslice");
  flip_item     = gtk_menu_item_new_with_label("Flip");
  sort_item     = gtk_menu_item_new_with_label("Sort");
  matrix_item   = gtk_menu_item_new_with_label("Matrix");

  gtk_menu_shell_append(GTK_MENU_SHELL(images_menu), view_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(images_menu), extract_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(images_menu), reslice_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(images_menu), flip_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(images_menu), sort_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(images_menu), matrix_item);

  /* attach images callbacks */
  g_signal_connect(view_item, "activate",
        G_CALLBACK(XMdcImagesView), NULL);

  /* extract sub menu */
  extract_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(extract_item), extract_menu);

  extract_normal_item = gtk_menu_item_new_with_label("Normal");
  extract_ecat_item   = gtk_menu_item_new_with_label("Ecat");

  gtk_menu_shell_append(GTK_MENU_SHELL(extract_menu), extract_normal_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(extract_menu), extract_ecat_item);

  /* attach extract callbacks */
  g_signal_connect(extract_normal_item, "activate",
      G_CALLBACK(XMdcMenuCbExtract), &xmdc_extract_normal);

  g_signal_connect(extract_ecat_item, "activate",
      G_CALLBACK(XMdcMenuCbExtract), &xmdc_extract_ecat);

  /* reslice sub menu */
  reslice_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(reslice_item), reslice_menu);

  reslice_tra_item = gtk_menu_item_new_with_label("Transaxial (XY)");
  reslice_cor_item = gtk_menu_item_new_with_label("Coronal    (XZ)");
  reslice_sag_item = gtk_menu_item_new_with_label("Sagittal   (YZ)");

  gtk_menu_shell_append(GTK_MENU_SHELL(reslice_menu), reslice_tra_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(reslice_menu), reslice_cor_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(reslice_menu), reslice_sag_item);

  /* attach reslice callbacks */
  g_signal_connect(reslice_tra_item, "activate",
        G_CALLBACK(XMdcMenuCbReslice), (gpointer) &xmdc_reslice_transaxial);

  g_signal_connect(reslice_cor_item, "activate",
        G_CALLBACK(XMdcMenuCbReslice), (gpointer) &xmdc_reslice_coronal);

  g_signal_connect(reslice_sag_item, "activate",
        G_CALLBACK(XMdcMenuCbReslice), (gpointer) &xmdc_reslice_sagittal);

  /* flip sub menu */
  flip_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(flip_item), flip_menu);

  flip_horizontal_item = gtk_menu_item_new_with_label("Horizontal");
  flip_vertical_item   = gtk_menu_item_new_with_label("Vertical");

  gtk_menu_shell_append(GTK_MENU_SHELL(flip_menu), flip_horizontal_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(flip_menu), flip_vertical_item);

  /* attach flip callbacks */

  g_signal_connect(flip_horizontal_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_flip_horizontal);

  g_signal_connect(flip_vertical_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_flip_vertical);

  /* sort sub menu */
  sort_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(sort_item), sort_menu);

  reverse_item = gtk_menu_item_new_with_label("Reverse");
  cine_item    = gtk_menu_item_new_with_label("Cine");

  gtk_menu_shell_append(GTK_MENU_SHELL(sort_menu), reverse_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(sort_menu), cine_item);

  /* attach sort callback */
  g_signal_connect(reverse_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_sort_reverse);

  /* cine sub menu */
  cine_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(cine_item), cine_menu);

  cine_apply_item = gtk_menu_item_new_with_label("Apply");
  cine_undo_item  = gtk_menu_item_new_with_label("Undo");

  gtk_menu_shell_append(GTK_MENU_SHELL(cine_menu), cine_apply_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(cine_menu), cine_undo_item);

  /* attach cine callbacks */
  g_signal_connect(cine_apply_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_cine_apply);

  g_signal_connect(cine_undo_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_cine_undo);

  /* matrix sub menu */
  matrix_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(matrix_item), matrix_menu);

  square_item      = gtk_menu_item_new_with_label("Square");
  square_pwr2_item = gtk_menu_item_new_with_label("Square Pwr2");

  gtk_menu_shell_append(GTK_MENU_SHELL(matrix_menu), square_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(matrix_menu), square_pwr2_item);

  /* attach matrix callbacks */
  g_signal_connect(square_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_matrix_square);

  g_signal_connect(square_pwr2_item, "activate",
        G_CALLBACK(XMdcMenuCbTransform), (gpointer) &xmdc_matrix_square_pwr2);

  /*** create Options menu ***/
  options_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(options_item), options_menu);

  medcon_item    = gtk_menu_item_new_with_label("MedCon");
  render_item    = gtk_menu_item_new_with_label("Render");
  labels_item    = gtk_menu_item_new_with_label("Labels");
  pages_item     = gtk_menu_item_new_with_label("Pages");
  resize_item    = gtk_menu_item_new_with_label("Resize");
  colormap_item  = gtk_menu_item_new_with_label("Colormap");
  menubar_item   = gtk_menu_item_new_with_label("Menubar");

  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), medcon_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), render_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), labels_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), pages_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), resize_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), colormap_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(options_menu), menubar_item);

  /* attach options callbacks */
  g_signal_connect(medcon_item, "activate",
        G_CALLBACK(XMdcOptionsMedconSel), NULL);

  g_signal_connect(render_item, "activate",
        G_CALLBACK(XMdcOptionsRenderSel), NULL);

  g_signal_connect(labels_item, "activate",
        G_CALLBACK(XMdcOptionsLabelSel), NULL);

  g_signal_connect(pages_item, "activate",
        G_CALLBACK(XMdcOptionsPagesSel), NULL);

  g_signal_connect(resize_item, "activate",
        G_CALLBACK(XMdcOptionsResizeSel), NULL);

  /* colormap sub menu */
  colormap_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(colormap_item), colormap_menu);

  colors_item = gtk_menu_item_new_with_label("Colors");
  place_item  = gtk_menu_item_new_with_label("Place");

  gtk_menu_shell_append(GTK_MENU_SHELL(colormap_menu), colors_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(colormap_menu), place_item);

  /* attach colormap callbacks */
  g_signal_connect(colors_item, "activate",
        G_CALLBACK(XMdcOptionsColorMapSel), NULL);

  /* place sub menu */
  place_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(place_item), place_menu);

  place_left_item  = gtk_menu_item_new_with_label("Left");
  place_right_item = gtk_menu_item_new_with_label("Right");

  gtk_menu_shell_append(GTK_MENU_SHELL(place_menu), place_left_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(place_menu), place_right_item);

  /* attach place callbacks */
  g_signal_connect(place_left_item, "activate",
        G_CALLBACK(XMdcMenuCbPlace), &xmdc_colmap_place_left);

  g_signal_connect(place_right_item, "activate",
        G_CALLBACK(XMdcMenuCbPlace), &xmdc_colmap_place_right);

  /* menubar sub menu */
  menubar_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menubar_item), menubar_menu);

  menubar_ltr_item = gtk_menu_item_new_with_label("Left-to-Right");
  menubar_rtl_item = gtk_menu_item_new_with_label("Right-to-Left");
  menubar_ttb_item = gtk_menu_item_new_with_label("Top-to-Bottom");
  menubar_btt_item = gtk_menu_item_new_with_label("Bottom-to-Top");

  gtk_menu_shell_append(GTK_MENU_SHELL(menubar_menu), menubar_ltr_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menubar_menu), menubar_rtl_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menubar_menu), menubar_ttb_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menubar_menu), menubar_btt_item);

  /* attach menubar callbacks */
  g_signal_connect(menubar_ltr_item, "activate",
      G_CALLBACK(XMdcMenuCbMenubar), &xmdc_menubar_ltr);

  g_signal_connect(menubar_rtl_item, "activate",
      G_CALLBACK(XMdcMenuCbMenubar), &xmdc_menubar_rtl);

  g_signal_connect(menubar_ttb_item, "activate",
      G_CALLBACK(XMdcMenuCbMenubar), &xmdc_menubar_ttb);

  g_signal_connect(menubar_btt_item, "activate",
      G_CALLBACK(XMdcMenuCbMenubar), &xmdc_menubar_btt);

  /*** create Help menu ***/
  help_menu = gtk_menu_new();

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(help_item), help_menu);

  online_info_item  = gtk_menu_item_new_with_label("Online Info");
  console_logs_item = gtk_menu_item_new_with_label("Console Logs");
  about_item        = gtk_menu_item_new_with_label("About");

  gtk_menu_shell_append(GTK_MENU_SHELL(help_menu), online_info_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(help_menu), console_logs_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(help_menu), about_item);

  /* attach help callbacks */
  g_signal_connect(online_info_item, "activate",
        G_CALLBACK(XMdcHelp), NULL);

  g_signal_connect(console_logs_item, "activate",
        G_CALLBACK(XMdcShowLogConsole), NULL);

  g_signal_connect(about_item, "activate",
        G_CALLBACK(XMdcAbout), NULL);

  /* finally show & attach menu */
  gtk_widget_show_all(menu_bar);
  gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE, 2);

}

