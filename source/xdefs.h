/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xdefs.h                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xdefs.c header file                                      *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XDEFS_H__
#define __XDEFS_H__

/****************************************************************************
                              D E F I N E S
****************************************************************************/

/* file types */                 /* unsaved status > 128*/
#define XMDC_RAW     255
#define XMDC_PREDEF  254
#define XMDC_EXTRACT 253
#define XMDC_RESLICE 252
#define XMDC_TRANSF  251
#define XMDC_EDITFI  250

#define XMDC_UNSAVED 128
                                 /* saved   status < 128 */
#define XMDC_SAVED     2
#define XMDC_NORMAL    1

#define XMDC_FREE_BORDER 125
#define XMDC_COLORMAP_WIDTH  25
#define XMDC_COLORMAP_HEIGHT 48

#define XMDC_LABEL_RED    1
#define XMDC_LABEL_GREEN  2
#define XMDC_LABEL_BLUE   3
#define XMDC_LABEL_YELLOW 4

#define XMDC_LABEL_STYLE_ABS   1
#define XMDC_LABEL_STYLE_PAGE  2
#define XMDC_LABEL_STYLE_ECAT  3

#define XMDC_ZOOM_FACTOR       1
#define XMDC_ZOOM_IN          XMDC_ZOOM_FACTOR   /* incr */
#define XMDC_ZOOM_OUT       -(XMDC_ZOOM_FACTOR)  /* decr */
#define XMDC_ZOOM_NONE         0

#define XMDC_RESIZE_FIFTH     -5   /* 1:5 */
#define XMDC_RESIZE_FOURTH    -4   /* 1:4 */
#define XMDC_RESIZE_THIRD     -3   /* 1:3 */
#define XMDC_RESIZE_HALF      -2   /* 1:2 */
#define XMDC_RESIZE_ORIGINAL   1   /* 1:1 */
#define XMDC_RESIZE_DOUBLE     2   /* 2:1 */
#define XMDC_RESIZE_TRIPLE     3   /* 3:1 */
#define XMDC_RESIZE_QUAD       4   /* 4:1 */
#define XMDC_RESIZE_QUINT      5   /* 5:1 */

#define XMDC_PAGES_FRAME_BY_FRAME 0                   /* frame by frame */
#define XMDC_PAGES_SLICE_BY_SLICE 1                   /* slice by slice */
#define XMDC_PAGES_SCREEN_FULL    2                   /* screen full    */

#define XMDC_DEFAULT_FRMT  MDC_FRMT_RAW  /* default save format (enabled!?) */

#define XMDC_MAX_LOADABLE_LUTS  999      /* maximum external LUTs available */

typedef struct OptionsMedConStruct_t {
        GtkWidget *PixPositives;
        GtkWidget *PixNegatives;
        GtkWidget *PixNoQuant;
        GtkWidget *PixQuantify;
        GtkWidget *PixCalibrate;
        GtkWidget *PixTypeNONE;
        GtkWidget *PixTypeBIT8_U;
        GtkWidget *PixTypeBIT16_S;
        GtkWidget *BitsUsed12;
        GtkWidget *FileTypeLITTLE;
        GtkWidget *FileTypeBIG;
        GtkWidget *FlipHoriz;
        GtkWidget *FlipVert;
        GtkWidget *SortReverse;
        GtkWidget *SortCine;
        GtkWidget *SortCineApply;
        GtkWidget *SortCineUndo;
        GtkWidget *MakeSqrNo;
        GtkWidget *MakeSqr1;
        GtkWidget *MakeSqr2;
        GtkWidget *NormOverFrames;
        GtkWidget *NormOverAll;
        GtkWidget *FallbackNONE;
        GtkWidget *FallbackANLZ;
        GtkWidget *FallbackCONC;
        GtkWidget *FallbackECAT;
        GtkWidget *FallbackDICM;
        GtkWidget *SplitNone;
        GtkWidget *SplitFrames;
        GtkWidget *SplitSlices;
        GtkWidget *ColorModeIndexed;
        GtkWidget *ColorMakeGray;
        GtkWidget *ColorDither;
        GtkWidget *PadAround;
        GtkWidget *PadTopLeft;
        GtkWidget *PadBottomRight;
        GtkWidget *NameAlias;
        GtkWidget *NameNoPrefix;
        GtkWidget *DicmMosaicEnabled;
        GtkWidget *DicmMosaicForced;
        GtkWidget *DicmMosaicWidth;
        GtkWidget *DicmMosaicHeight;
        GtkWidget *DicmMosaicNumber;
        GtkWidget *DicmMosaicDoInterl;
        GtkWidget *DicmMosaicFixVoxel;
        GtkWidget *DicmTrueGap;
        GtkWidget *DicmContrast;
        GtkWidget *DicmWriteImplicit;
        GtkWidget *DicmWriteNoMeta;
        GtkWidget *AnlzSPM;
        GtkWidget *IntfSkip1;
        GtkWidget *IntfNoPath;
        GtkWidget *IntfSingleFile;
        GtkWidget *EcatSortAnatom;
        GtkWidget *EcatSortByFrame;
}OptionsMedConStruct;

typedef struct MyMainStruct_t {
    GtkWidget *mainwindow;
    GtkWidget *menubar;
    GtkWidget *viewwindow;
    GtkWidget *viewbox;
    GtkWidget *pagemenu;
    GtkWidget *imgsbox;
    GtkWidget *imgsgrid;
    GtkWidget *cmapbox;
    GtkWidget **image;
    GtkWidget *cmap;
    GdkPixbuf **im, *imcmap;
    GdkInterpType interp;
    gint cmap_w, cmap_h;
    int screen_height, screen_width;

    FILEINFO *fi;
    Uint32 curpage, prevpage;
    Uint32 number_of_pages, images_per_page;
    Uint32 images_horizontal, images_vertical;
    Uint32 startimage, real_images_on_page;
    Uint32 *pagenumber, *imagenumber, *realnumber;

    Int8 RESIZE;
    float scale_width, scale_height;

}MyMainStruct;


typedef struct ColormapSelectionStruct_t {
   gint      Nr;
   gint      CurMap;
   GtkWidget *Gray;
   GtkWidget *Inverted;
   GtkWidget *Rainbow;
   GtkWidget *Combined;
   GtkWidget *Hotmetal;
   GtkWidget *Loaded;
}ColormapSelectionStruct;

typedef struct MapPlaceSelectionStruct_t {
   GtkWidget *Right;
   GtkWidget *Left;
}MapPlaceSelectionStruct;

typedef struct LabelSelectionStruct_t {
   gint CurState;
   gint CurColor;
   gint CurStyle;
   GtkWidget *On;
   GtkWidget *Off;
   GtkWidget *Red;
   GtkWidget *Blue;
   GtkWidget *Green;
   GtkWidget *Yellow;
   GtkWidget *NrAbsolute;
   GtkWidget *NrInPage;
   GtkWidget *NrEcat;
}LabelSelectionStruct;


typedef struct RenderSelectionStruct_t {
   GdkInterpType Interp;
   GtkWidget *InterpNearest;
   GtkWidget *InterpTiles;
   GtkWidget *InterpBilinear;
   GtkWidget *InterpHyper;
}RenderSelectionStruct;

typedef struct ExtractSelectionStruct_t {
   GtkWidget *NormStyle;
   GtkWidget *EcatStyle;
   GtkWidget *InputPlanes;
   GtkWidget *InputFrames;
   GtkWidget *InputGates;
   GtkWidget *InputBeds;
   MdcExtractInputStruct *input;
}ExtractSelectionStruct;

typedef struct RawReadSelectionStruct_t {
   GtkWidget *HdrInfoWindow;
   GtkWidget *NrImages;
   GtkWidget *GenOffset;
   GtkWidget *ImgOffset;
   GtkWidget *AbsOffset;
   GtkWidget *IhdrRep, *PixSwap, *ImgSame;
   GtkWidget *ImgWidth, *ImgHeight;
   GtkWidget *typeBIT1,    *typeASCII;
   GtkWidget *typeBIT8_S,  *typeBIT8_U;
   GtkWidget *typeBIT16_S, *typeBIT16_U;
   GtkWidget *typeBIT32_S, *typeBIT32_U;
   GtkWidget *typeBIT64_S, *typeBIT64_U;
   GtkWidget *typeFLT32,   *typeFLT64;
   GtkWidget *typeCOLRGB;
   Uint32 ImgCounter;
}RawReadSelectionStruct;

typedef struct ResizeSelectionStruct_t {
   Int8 CurType;
   GtkWidget *Original;
   GtkWidget *Fifth;
   GtkWidget *Fourth;
   GtkWidget *Third;
   GtkWidget *Half;
   GtkWidget *Double;
   GtkWidget *Triple;
   GtkWidget *Quad;
   GtkWidget *Quint;
}ResizeSelectionStruct;

typedef struct PagesSelectionStruct_t{
   Int8 CurType;
   GtkWidget *FrameByFrame;
   GtkWidget *SliceBySlice;
   GtkWidget *ScreenFull;
}PagesSelectionStruct;

typedef struct SliderValueStruct_t {
  GtkAdjustment *adj;
  GtkWidget  *range;
  int        *value;
}SliderValueStruct;

typedef struct ColorModifier_t{
  guint gamma, brightness, contrast;
  Uint8 vgbc[256];
}ColorModifier;

typedef struct ColGbcCorrectStruct_t {
  GtkWidget *area;
  GdkPixbuf *image;
  GdkPixbuf *brightness_icon;
  GdkPixbuf *contrast_icon;
  GdkPixbuf *gamma_icon;
  ColorModifier mod;
  Uint32 i, nr, w, h, rw, rh;
  Int16 t;
  Uint8 *img8, vgbc[256];
}ColGbcCorrectStruct;

typedef struct EditFileInfoStruct_t {
  int CurModality;
  GtkWidget *PatSliceOrient[MDC_MAX_ORIENT];
  GtkWidget *PixelSize;
  GtkWidget *SliceWidth;
  GtkWidget *SliceSpacing;
  GtkWidget *FrameDuration;
  GtkWidget *NrDimPlanes;
  GtkWidget *NrDimFrames;
  GtkWidget *NrDimGates;
  GtkWidget *NrDimBeds;
  GtkWidget *NrDimWindows;
  GtkWidget *Reconstructed;
  GtkWidget *Planar;
  GtkWidget *ModalityNM;
  GtkWidget *ModalityPT;
  GtkWidget *ModalityCT;
  GtkWidget *ModalityMR;
  GtkWidget *ModalityCurrent;
  GtkWidget *AcquisitionType[MDC_MAX_ACQUISITIONS];
}EditFileInfoStruct;

extern Uint8 XMDC_FILE_OPEN;
extern Uint8 XMDC_FILE_TYPE;

extern Uint8 XMDC_IMAGE_BORDER;
extern Uint8 XMDC_MENUBAR_PACK;
extern Uint8 XMDC_CMAP_PLACE;

extern Uint8 XMDC_DOBAR;

extern GdkCursor *handcursor;
extern GdkCursor *zoomcursor;

extern MyMainStruct            my;
extern OptionsMedConStruct     sOptionsMedCon;
extern ColormapSelectionStruct sColormapSelection;
extern MapPlaceSelectionStruct sMapPlaceSelection;
extern LabelSelectionStruct    sLabelSelection;
extern RenderSelectionStruct   sRenderSelection;
extern ExtractSelectionStruct  sExtractSelection;
extern RawReadSelectionStruct  sRawReadSelection;
extern ResizeSelectionStruct   sResizeSelection;
extern PagesSelectionStruct    sPagesSelection;
extern ColGbcCorrectStruct     sGbc;
extern EditFileInfoStruct      sEditFI;

extern char labelindex[50];
extern char labeltimes[50];
extern Uint32 write_counter;

extern char xmdcstr[MDC_2KB_OFFSET];
extern char *XMEDCONLUT;
extern char *XMEDCONRPI;

#endif

