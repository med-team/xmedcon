/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xerror.h                                                      *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : xerror.c header file                                     *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __XERROR_H__
#define __XERROR_H__

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcFatalErrorKill(GtkWidget *button, int *code);
void *XMdcDisplayDialog(int code, char *windowtitle, char *info);
void *XMdcDisplayWarn(char *fmt, ...);
void *XMdcDisplayMesg(char *fmt, ...);
void *XMdcDisplayErr(char *fmt, ...);
void XMdcDisplayFatalErr(int code, char *fmt, ...);
void XMdcLogHandler(const gchar *domain, GLogLevelFlags level,
                    const gchar *message, gpointer user_data);
void XMdcCreateLogConsole(void);
void XMdcShowLogConsole(void);
void XMdcClearLogConsole(void);

#endif

