/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: xhelp.c                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : help routines (open URL in browser)                      *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * Functions    : XMdcHelp()                    - Open help in browser     *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

/****************************************************************************
                              H E A D E R S
****************************************************************************/

#include "xmedcon.h"

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

void XMdcHelp(GtkWidget *widget, gpointer data)
{

  GError *error = NULL;
  GtkWidget *toplevel;
  GtkWindow *window;

  /* widget core dumps on Wayland / CentOS Stream 8 */
  /* toplevel = gtk_widget_get_toplevel(widget);    */
  toplevel = gtk_widget_get_toplevel(my.mainwindow);

  if (!gtk_widget_is_toplevel(toplevel)) {
    XMdcDisplayMesg("Online documentation available at\n%s", XMDCWEB);    
    return;
  }

  window = GTK_WINDOW(toplevel);

  if (!gtk_show_uri_on_window(window, XMDCWEB,
                              gtk_get_current_event_time(), &error)) {

    XMdcDisplayMesg("Online documentation available at\n%s",XMDCWEB);
    MdcDebugPrint("XMdcHelp() - %s\n", error->message);
    g_error_free(error);
    return;
  }

}

