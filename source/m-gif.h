/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * filename: m-gif.h                                                       *
 *                                                                         *
 * UTIL C-source: Medical Image Conversion Utility                         *
 *                                                                         *
 * purpose      : m-gif.c header file                                      *
 *                                                                         *
 * project      : (X)MedCon by Erik Nolf                                   *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 */

/*
   Copyright (C) 1997-2025 by Erik Nolf

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any later
   version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
   Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   59 Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef __M_GIF_H__
#define __M_GIF_H__


/****************************************************************************
                              D E F I N E S
****************************************************************************/

#define MDC_GIF_SIG     "GIF"
#define MDC_GIF89_SIG   "GIF89a"


#define GIF_DELAY  165

typedef struct MdcGifHeader_t {
        char sig[6];
        Uint16 screenwidth,screenheight;
        Uint8  flags,background,aspect;

} MDC_GIFHEADER;

#define MDC_GIF_GH_SIZE 13

typedef struct MdcGifImageBlock_t {
        Uint16 left,top,width,height;
        Uint8  flags;
} MDC_GIFIMAGEBLOCK;

#define MDC_GIF_IBLK_SIZE  9

typedef struct MdcGifControlBlock_t {
        Uint8 blocksize;
        Uint8 flags;
        Uint16 delay;
        Uint8 transparent_colour;
        Uint8 terminator;
} MDC_GIFCONTROLBLOCK;

#define MDC_GIF_CBLK_SIZE 6

typedef struct MdcGifPlainText_t {
        Uint8 blocksize;
        Uint16 left,top;
        Uint16 gridwidth,gridheight;
        Uint8 cellwidth,cellheight;
        Uint8 forecolour,backcolour;
} MDC_GIFPLAINTEXT;

#define MDC_GIF_TBLK_SIZE 13

typedef struct MdcGifApplication_t {
        Uint8 blocksize;
        char applstring[8];
        char authentication[3];
} MDC_GIFAPPLICATION;

#define MDC_GIF_ABLK_SIZE 12


typedef struct MdcGifOpt_t {
        Uint8 loop, transp;         /* 1 = YES  or 0 = NO */
        Uint8 bground_color;        /* 0 ... 255          */
        Uint8 transp_color;         /* 0 ... 255          */
        Uint16 delay;               /* delay 1/100ths sec */
} MDC_GIFOPT;

/****************************************************************************
                            F U N C T I O N S
****************************************************************************/

int MdcCheckGIF(FILEINFO *fi);
char *MdcReadGIF(FILEINFO *fi);
void MdcDoExtension(FILEINFO *fi);
int MdcReadGifHeader(FILE *fp, MDC_GIFHEADER *gh);
int MdcReadGifImageBlk(FILE *fp, MDC_GIFIMAGEBLOCK *ib);
int MdcReadGifControlBlk(FILE *fp, MDC_GIFCONTROLBLOCK *cb);
int MdcReadGifPlainTextBlk(FILE *fp, MDC_GIFPLAINTEXT *pt);
int MdcReadGifApplicationBlk(FILE *fp, MDC_GIFAPPLICATION *ap);
char *MdcUnpackImage(FILEINFO *fi, Uint32 nr);
void MdcPutGifLine(IMG_DATA *ri, Uint8 *p, Int16 n);
char *MdcWriteGIF(FILEINFO *fi);
void MdcGetGifOpt(FILEINFO *fi, MDC_GIFOPT *opt);
int MdcWriteGifHeader(FILEINFO *fi, MDC_GIFOPT *opt);
int MdcWriteControlBlock(FILEINFO *fi, MDC_GIFOPT *opt, Uint32 n);
int MdcWriteImageBlock(FILEINFO *fi, Uint32 n);
int MdcWriteImage(Uint8 *buffer, FILEINFO *fi, Uint32 n);
int MdcWriteCommentBlock(FILEINFO *fi, const char *comment);
int MdcWriteLoopBlock(FILEINFO *fi, const char *applstr, const char *auth);
int MdcWriteApplicationBlock(FILEINFO *fi, const char *applstr, const char *auth);
void MdcInitTable(Int16 min_code_size);
void MdcFlush(FILE *fp, Int16 n);
void MdcWriteCode(FILE *fp, Int16 code);

#endif

