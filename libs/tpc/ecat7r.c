/******************************************************************************

  Copyright (c) 2003-2010 Turku PET Centre

  Library file: ecat7r.c
  Description:  Functions for reading ECAT 7.x format.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details:
  http://www.gnu.org/copyleft/lesser.html

  You should have received a copy of the GNU Lesser General Public License
  along with this library/program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

  Turku PET Centre, Turku, Finland, http://www.turkupetcentre.fi
  
  Modification history:
  2003-07-24 Vesa Oikonen
    First created.
  2003-09-08 VO
    Added support for 3D sinograms, ecat7ReadScanMatrix().
  2004-05-23 VO
    Comments changed into Doxygen format.
  2004-06-21 VO
    ecat7ReadScanMatrix():
    Before: reads datablocks based on matrix list.
    After: if block number based on bin nr is smaller, then read only those
    Reason: simulated file with erroneous matrix list.
  2004-09-20 VO
    Doxygen style comments are corrected.
  2004-11-10 VO
    Calculation of trueblockNr simplified in ecat7ReadScanMatrix().
  2006-02-07 Jarkko Johansson
    Comments added in ecat7ReadScanMatrix().
  2007-03-21 VO  
    ecat7ReadImageheader(): fill_cti[] and fill_user[] are read correctly.
  2007-03-27 VO
    Added ecat7ReadPolarmapMatrix().
  2010-08-19 VO
    Main header field patient_birth_date can be in two different int formats,
    either YYYYMMDD or as seconds from start of year 1970. In latter case
    the number can be negative, which is not identified correctly by all C
    library versions. Therefore those are converted to YYYYMMDD format.


******************************************************************************/
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
/*****************************************************************************/
#include "ecat7.h"
/*****************************************************************************/

/*****************************************************************************/
int ecat7pxlbytes(short int data_type) {
  int byteNr=0;
  switch(data_type) {
    case ECAT7_BYTE: byteNr=1; break;
    case ECAT7_VAXI2:
    case ECAT7_SUNI2: byteNr=2; break;
    case ECAT7_VAXI4:
    case ECAT7_VAXR4:
    case ECAT7_IEEER4:
    case ECAT7_SUNI4: byteNr=4; break;
  }
  return(byteNr);
}
/*****************************************************************************/

/*****************************************************************************/
