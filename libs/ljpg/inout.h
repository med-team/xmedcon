/*
 * inout.h --
 *
 */
/*
 *
 */
#ifndef _INOUT
#define _INOUT

#include "jpeg.h"

/*
 * Size of the input and output buffer
 */
#define JPEG_BUF_SIZE   4096 

/*
 * The following variables keep track of the input and output
 * buffer for the JPEG data.
 */
extern char   outputBuffer[JPEG_BUF_SIZE];      /* output buffer              */
extern int    numOutputBytes;                   /* bytes in the output buffer */
extern Uchar *inputBuffer;                      /* Input buffer for JPEG data */
extern int    inputBufferOffset;                /* Offset of current byte     */

#endif /* _INOUT */
